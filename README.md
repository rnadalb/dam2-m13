## 📘 DAM2-M13 - Projecte de DAM2

Benvingut/da al repositori del **Mòdul M13** del Cicle Formatiu de Grau Superior en **Desenvolupament d'Aplicacions Multiplataforma (DAM2)** de l'[Institut Eugeni d'Ors](https://www.ies-eugeni.cat) a [Vilafranca del Penedès](https://goo.gl/maps/CX6KV3ZGNsR2). 

***Professorat***
* [David Amoròs](https://www.linkedin.com/in/david-amor%C3%B3s-56011360/)
* Rafael del Moral
* [Rubén Nadal](https://www.linkedin.com/in/rnadalb/)

### 📌 Continguts 
* [Informació i exemples](src/python) complementaris de suport del curs de certificació [PCAP™ – Certified Associate Python Programmer](https://www.netacad.com/courses/python-essentials-1?courseLang=en-US)
* [Introducció a FastAPI](src/fastapi)
* [Ús de git](src/git)
* [Introducció a l'anàlisi de dades](src/data%20analysis)
* [Exemple de client MQTT amb python](src/mqtt) 


#### Recursos en línia (updating)

* Moodle del mòdul: [DAM2-M13-PRO](http://www.ies-eugeni.cat/course/view.php?id=817)
* [Presentació M13 at Genially](https://view.genially.com/613f667ca5a8670dfd629b58/presentation-presentacio-m13-pro)

---

### 🚀 Instal·lació i execució

#### 📥 1. Clonar el repositori
```bash
git clone https://gitlab.com/rnadalb/dam2-m13.git
cd dam2-m13
```
#### 📦 2. Instal·lar dependències, si escau
```bash
pip install -r requirements.txt
```

#### ▶️ 3. Executar un exemple bàsic
```bash
python src/main.py
```

### Recursos útils

#### 🔄 Control de versions i desenvolupament
- **Git**: [Documentació oficial](https://git-scm.com/doc)
- **GitLab**: [Plataforma de repositoris](https://about.gitlab.com/)

#### 👨‍💻 Llibreries per a anàlisi de dades
- **Pandas**: [Manipulació i anàlisi de dades](https://pandas.pydata.org/docs/)
- **NumPy**: [Llibreria de computació numèrica](https://numpy.org/doc/)
- **Matplotlib**: [Gràfics en Python](https://matplotlib.org/stable/contents.html)
- **Seaborn**: [Visualització estadística](https://seaborn.pydata.org/)

#### 🛠️ Entorns d'execució i desplegament
- **Docker**: [Documentació i guies](https://docs.docker.com/)

#### 💡 Llenguatge de programació i paquets
- **Python**: [Documentació oficial](https://docs.python.org/3/)
- **PyPI (Python Package Index)**: [Repositori de paquets Python](https://pypi.org/)

*Always updating ...*

