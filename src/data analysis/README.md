
## Anàlisi de dades
L'anàlisi de dades és el procés de recopilar, netejar, transformar i modelar dades amb l'objectiu d'extreure informació útil, arribar a conclusions i donar suport a la presa de decisions. Aquest procés pot incloure l'identificació de patrons i tendències, la realització d'estadístiques descriptives, i la inferència de relacions causals o prediccions sobre esdeveniments futurs.

Una de les eines que facilita aquesta tasca és [Pandas](https://pandas.pydata.org/).

### Què aporta Pandas a l'anàlisi de dades?

Pandas és una biblioteca de Python especialment dissenyada per a l'anàlisi de dades que ofereix potents estructures de dades ([Series](https://pandas.pydata.org/docs/reference/api/pandas.Series.html) i [DataFrames](https://pandas.pydata.org/docs/reference/api/pandas.DataFrame.html)) i eines per manipular i analitzar eficientment grans volums de dades. Algunes de les principals aportacions de Pandas a l'anàlisi de dades inclouen:

- **Facilitat de manipulació de dades:** Pandas simplifica la importació, neteja i manipulació de dades gràcies a les seves funcions integrades per a la gestió de dades mancants, la transformació de formats, l'agregació i la combinació de conjunts de dades.
- **Anàlisi exploratòria de dades (EDA):** Pandas facilita l'EDA amb funcions per a càlcul d'estadístiques descriptives, correlacions, comptes de valors únics, etc., que ajuden a entendre millor les dades.
- **Visualització de dades:** encara que Pandas no és una biblioteca de visualització per se, s'integra bé amb biblioteques com [Matplotlib](https://matplotlib.org/) i [Seaborn](https://seaborn.pydata.org/), permetent crear visualitzacions directament des de DataFrames.
- **Flexibilitat:** l'ús de Pandas no està limitat a un tipus específic de dades o domini, fent-lo útil en una àmplia gamma d'aplicacions d'anàlisi de dades, des de finances fins a la bioinformàtica.

#### Un exemple ...

Imaginem que volem analitzar un conjunt de dades sobre les vendes mensuals d'una botiga per identificar tendències i patrons. Podem utilitzar Pandas per a:

1. **Càrrega de dades:** importar les dades de vendes des d'un fitxer CSV.
2. **Neteja de dades:** tractar valors mancants o incorrectes.
3. **Anàlisi exploratòria:** calcular estadístiques descriptives com la mitjana de vendes mensuals, la desviació estàndard, i identificar els mesos amb millor i pitjor rendiment.
4. **Visualització:** generar gràfics de línia per visualitzar les tendències de vendes al llarg del temps per exemple amb la llibreria Matplotlib.

#### Avantatges i inconvenients

**Avantatges:**
- **Flexibilitat i facilitat d'ús:** permet manipular dades de manera eficient amb poc codi.
- **Àmpli suport:** té una comunitat gran i activa, recursos educatius abundants, i s'integra bé amb altres biblioteques.

**Inconvenients:**
- **Rendiment amb dades molt grans:** per a conjunts de dades extremadament grans, Pandas pot ser menys eficient que altres solucions especialitzades com Dask o Apache Spark.
- **Corba d'aprenentatge:** encara que és fàcil d'usar, dominar totes les funcionalitats de Pandas pot requerir temps.

#### Tendències actuals

- **Optimització del rendiment:** Es treballa constantment en la millora del rendiment de Pandas per a manejar grans volums de dades més eficientment.
- **Interoperabilitat:** Millora de la integració amb altres ecosistemes de dades i anàlisi, com ara Apache Arrow per a l'intercanvi de dades més ràpid entre sistemes.
- **Machine Learning:** Integració més estreta amb biblioteques de Machine Learning, facilitant l'ús conjunt de Pandas per a la preparació de dades i biblioteques com Scikit-learn per a l'anàlisi predictiva.


Ara anem per feina ... 

* [05-Introducció a Pandas](https://colab.research.google.com/drive/1bx9dQy21K6_quApfLMgVNawx3gBOi7PZ?usp=sharing)
* [06-EDA per dades metereològiques](https://colab.research.google.com/drive/1sdHvgldLKubeoQbsY9JQxp9757qaS-JN?usp=sharing)