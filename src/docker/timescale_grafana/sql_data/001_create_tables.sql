create table if not exists public.tbl_device
(
    device_id serial constraint tbl_device_pk primary key,
    dev_eui   varchar(16) default '0000000000000000'::character varying not null,
    alias     varchar(64),
    online    boolean     default false,
    latitude  numeric(10, 8),
    longitude numeric(10, 8)
);

create table if not exists public.tbl_meteo
(
    time        timestamp with time zone not null,
    temperature double precision,
    humidity    double precision,
    pressure    double precision,
    device_id   integer constraint tbl_meteo_tbl_device_device_id_fk references public.tbl_device
);