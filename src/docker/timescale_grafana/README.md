# Exemples i configuracions amb docker per diferents serveis

* [timescale_grafana](./timescale_grafana)
  * Orquestració de dos serveis:
    * [TimescaleDB](https://www.timescale.com): és una base de dades dissenyada per a emmagatzemar i processar dades en forma de sèries de temps (segments de valors de paràmetres a intervals de temps donats, el registre forma el temps i un conjunt de valors corresponents a aquest temps). 
     Aquesta forma d'emmagatzematge és òptima per a aplicacions com ara sistemes de monitoratge, plataformes de negociació, sistemes per a recopilar mètriques i estats de sensors.
    * [Grafana](https://www.grafana.com): és una eina per a visualitzar dades de sèrie temporals.  A partir d'una sèrie de dades recol·lectades obtindrem un panorama gràfic de la situació d'una empresa o organització.
