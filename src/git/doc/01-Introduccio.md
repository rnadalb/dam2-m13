# 1. Introducció

## 1.1. Objectius generals

Aquest material didàctic té com a objectiu principal ajudar l'alumnat a familiaritzar-se amb l'ús de Git com a eina de control de versions i gestió de codi. Amb aquests coneixements, els estudiants podran gestionar projectes de programació de manera eficaç, treballar col·laborativament amb altres desenvolupadors i resoldre problemes comuns relacionats amb la gestió de codi.

### Objectius específics:
- Aprendre a crear i gestionar repositoris de Git, tant locals com remots.
- Entendre i aplicar conceptes bàsics i avançats de Git, com commits, branques i fusions.
- Desenvolupar habilitats per col·laborar en projectes amb altres desenvolupadors utilitzant Git.
- Familiaritzar-se amb les millors pràctiques de programació per mantenir un codi clar, estructurat i segur.

## 1.2. Importància de la gestió de codi en el desenvolupament de programari

El control de versions és una pràctica essencial per al desenvolupament de programari professional. Git permet gestionar de manera eficient els canvis realitzats al codi font, facilitant així el treball col·laboratiu i la resolució de conflictes. Això és especialment important en projectes on participen diversos desenvolupadors, ja que ajuda a evitar problemes com la pèrdua de canvis o l'overwriting de codi.

### Beneficis de l'ús de Git:
- **Històric de canvis**: Permet mantenir un registre detallat de totes les modificacions, facilitant la revisió i la recuperació de versions anteriors.
- **Treball col·laboratiu**: Facilita la col·laboració entre desenvolupadors mitjançant l'ús de branques i repositoris remots.
- **Resolució de conflictes**: Proporciona eines per identificar i resoldre conflictes de codi de manera eficient.
- **Seguretat i integritat**: Garanteix que les versions del codi estiguin protegides i siguin coherents.

### Exemple pràctic:
Imagina que estàs treballant en un projecte amb un equip de cinc persones. Cada membre pot treballar en una funcionalitat diferent en la seva pròpia branca, i després integrar els canvis en la branca principal (normalment anomenada `main` o `master`). Aquest enfocament ajuda a mantenir el codi ordenat i evita problemes de compatibilitat que poden sorgir si es treballa directament sobre una sola branca compartida.