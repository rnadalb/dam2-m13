# 5. Col·laboració amb Git

## 5.1. Configuració de repositoris remots

Un repositori remot és una còpia d'un repositori que es troba en un servidor, com GitLab, i permet col·laborar amb altres desenvolupadors. Per configurar un repositori remot en un projecte local, segueix aquests passos:

### Afegir un repositori remot:
Si has creat un projecte localment i vols connectar-lo a un repositori remot a GitLab, utilitza el següent comandament:
```bash
git remote add origin git@gitlab.com:<usuari>/<nom-repositori>.git
```
Verificar els repositoris remots configurats:
```bash
git remote -v
```
Aquesta ordre mostra la llista de repositoris remots associats al projecte.

## 5.2. clone, push, pull i fetch: Treballar amb repositoris remots
* `git clone`: es fa servir per copiar un repositori remot al teu ordinador:
```bash
git clone git@gitlab.com:<usuari>/<nom-repositori>.git
```
Això crearà una carpeta amb el contingut del repositori i configurarà origin com el repositori remot per defecte.

* `git push`: Després de realitzar canvis i fer commits al teu repositori local, utilitza git push per pujar-los al repositori remot:
```bash
git push origin main
```
* `git pull`: Aquest comandament actualitza el teu repositori local amb els canvis del repositori remot:
```bash
git pull origin main
```

* `git pull`: és una combinació de git fetch i git merge, que descarrega i integra els canvis.

* `git fetch`: descarrega els canvis del repositori remot sense fusionar-los automàticament amb el teu codi local:

```bash
git fetch origin
```

Després de fer git fetch, pots revisar els canvis i decidir si els vols fusionar.

## 5.3. Gestió de permisos i col·laboració amb equips

Quan treballes en un projecte col·laboratiu a GitLab, la gestió de permisos és fonamental per controlar qui pot accedir al repositori i quin tipus d’accions poden realitzar.

Tipus de rols a GitLab:

* **Owner** (_propietari_): Té accés complet al repositori i pot gestionar membres i configuracions.
* **Maintainer** (_mantenidor_): Pot realitzar canvis importants, gestionar branques i fusionar sol·licituds de canvis.
* **Developer** (_desenvolupador_): Pot contribuir al codi amb permisos per crear branques i fer commits.
* **Reporter** (_informador_): Pot veure el projecte i els seus problemes, però no pot fer commits ni canvis al codi.
* **Guest** (_convidat_): Té un accés limitat per visualitzar el projecte.

Com afegir membres al teu projecte:

1.	Ves al repositori a GitLab.
2.	A la secció de “Members”, afegeix l’usuari amb el rol adequat.
3.	Configura els permisos i el temps d’accés si cal.

## 5.4. Fluxos de treball col·laboratius: Git Flow i altres patrons

Un flux de treball col·laboratiu defineix com els desenvolupadors treballen conjuntament en un projecte. Un dels fluxos més populars és Git Flow, que utilitza branques de manera estratègica per gestionar el desenvolupament i les versions.

Conceptes bàsics de Git Flow:

* **_Branca main_**: Conté el codi de producció estable.
* **_Branca develop_**: Conté l’últim codi de desenvolupament en procés.
* **_Feature branches_**: Utilitzades per desenvolupar noves funcionalitats (feature/nova-funcionalitat).
* **_Release branches_**: Creats per preparar una nova versió per al llançament.
* **_Hotfix branches_**: Per corregir errors crítics en la versió de producció.

Exemple de flux de treball:

1. Crea una branca de funcionalitat des de develop:
```bash
git checkout -b feature/nova-funcionalitat develop
```

2.	Un cop acabada la funcionalitat, fusiona la branca a develop:
```bash
git checkout develop
git merge feature/nova-funcionalitat
```

3. Posa la branca de desenvolupament al repositori remot:
```bash
git push origin develop
```

Aquest flux ajuda a mantenir el codi organitzat i permet una col·laboració més eficient.