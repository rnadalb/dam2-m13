# 2. Conceptes fonamentals

## 2.1. Què és Git i per què utilitzar-lo?

**Git** és un sistema de control de versions distribuït creat per Linus Torvalds el 2005 per ajudar els desenvolupadors a gestionar el codi font de projectes de programari. A diferència d'altres sistemes de control de versions centralitzats, Git permet que cada desenvolupador tingui una còpia completa de l'historial del projecte al seu ordinador, cosa que facilita el treball en equip i la col·laboració independent de la connexió a internet.

### Per què utilitzar Git?
- **Control complet del codi**: Permet gestionar totes les versions d'un projecte, des de la creació fins a la publicació.
- **Treball fora de línia**: Pots treballar al teu repositori local sense necessitat de tenir accés constant a la xarxa.
- **Alt rendiment**: Realitzar operacions com commits, canvis de branca i fusions és ràpid i eficient.
- **Amplia comunitat**: Git és àmpliament utilitzat i compta amb una gran quantitat de documentació, tutorials i suport comunitari.

## 2.2. Sistema de control de versions: conceptes bàsics

Un sistema de control de versions és una eina que permet gestionar els canvis en els fitxers al llarg del temps. Això és especialment útil per a projectes de programació on diferents versions d'un codi han de ser revisades, millorades i, de vegades, recuperades.

### Funcionalitats principals de Git:
- **Seguiment de canvis**: Registra cada modificació realitzada en el projecte.
- **Treball col·laboratiu**: Facilita que diversos desenvolupadors treballin sobre el mateix projecte alhora.
- **Històric de versions**: Guarda una llista de tots els canvis realitzats, permetent la recuperació d'una versió anterior si cal.

## 2.3. Repositoris: Locals i remots

### Què és un repositori?
Un **repositori** és un directori que conté el projecte, incloent-hi tots els fitxers i l'historial de canvis. A Git, hi ha dos tipus principals de repositoris:

- **Repositori local**: Una còpia del repositori que es troba a l'ordinador del desenvolupador.
- **Repositori remot**: Una còpia del repositori que es troba en un servidor, com GitLab.com, que facilita el treball col·laboratiu.

### Exemple pràctic:
Quan inicies un nou projecte amb Git, primer es crea un repositori local mitjançant el següent comandament:
```bash
git init
```
Per col·laborar amb altres desenvolupadors, pots afegir un repositori remot amb:
```bash
git remote add origin https://gitlab.com/<username>/<nom-repositori>.git
```

En cas que utilitzis ssh (recomanat)
```bash
git remote add origin git@gitlab.com:<username>/<nom-repositori>.git
```

On:
* **\<username>**: és el nom del teu usuari
* **\<nom-repositori>**: és el nom del repositori on vols treballar.

## 2.4. Introducció a GitLab i altres plataformes

[GitLab](https://gitlab.com) és una plataforma popular que permet l’emmagatzematge i la gestió de repositoris remots. Ofereix una àmplia gamma de funcionalitats per al desenvolupament col·laboratiu, incloent la integració i el desplegament continu (CI/CD). GitLab és conegut per la seva flexibilitat i opcions de privacitat en repositoris gratuïts, cosa que el fa ideal per a projectes tant personals com empresarials.

**Característiques clau de GitLab**:

* Repositoris privats i públics: Permet crear repositoris de forma gratuïta amb opcions de privacitat.
* CI/CD integrat: Suporta la integració contínua i el desplegament automàtic, cosa que facilita l’automatització de tasques.
* Gestió de projectes: Ofereix eines com gestió de problemes (issues), wiki i panells de control per millorar la col·laboració.