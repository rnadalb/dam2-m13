# 3. Primeres Passes amb Git

## 3.1. Instal·lació i configuració de Git

Abans de començar a treballar amb Git, cal instal·lar-lo i configurar-lo adequadament al teu ordinador.

### Instal·lació de Git:
- **Windows**: Descarrega l'instal·lador des de [git-scm.com](https://git-scm.com/downloads) i segueix les instruccions del procés d'instal·lació.
- **MacOS**: Pots instal·lar Git utilitzant `Homebrew` amb el següent comandament:
```bash
  brew install git
```
- **Linux**: Instal·la Git mitjançant el gestor de paquets del teu sistema. Per exemple, en distribucions basades en Debian:
```bash
sudo apt-get install git
```


#### Configuració inicial de Git:

Un cop instal·lat, és important configurar les teves dades d’usuari per identificar els teus commits:

```bash
git config --global user.name "El teu Nom"
git config --global user.email "el.teu.email@example.com"
```

També pots configurar l’editor de text que Git utilitzarà per a missatges de commit:
```bash
git config --global core.editor "nom_de_l'editor"
```
## 3.2. Creació d’un repositori local

Un repositori local és la còpia inicial d’un projecte amb control de versions que es troba al teu ordinador. Per crear-ne un, segueix aquests passos:

1. **Crea un directori per al teu projecte**:

```bash
mkdir projecte-nou
cd projecte-nou
```

2. **Inicialitza el repositori de Git**:

```bash
git init --initial-branch=main
```

Aquesta ordre crea un directori ocult anomenat `.git`, que conté tota la informació necessària per al control de versions.
A més a més, inicialitza la branca `main` que serà la principal.

2.1. **Executa les següents ordres**:
```bash
git remote add origin git@gitlab.com:<usuari>/<nom-repositori>.git
git add .
git commit -m "Initial commit"
git push --set-upstream origin main
```

Anem pas per pas:

```bash
git remote add origin git@gitlab.com:<usuari>/<nom-repositori>.git
```
Aquesta instrucció s’utilitza per afegir un enllaç a un repositori remot a GitLab. 
La paraula origin és el nom que assignem per defecte al repositori remot. L’URL que segueix (git@gitlab.com:\<usuari>/\<nom-repositori>.git) 
és l’adreça del repositori a GitLab, que sol estar en format [SSH](https://docs.gitlab.com/ee/user/ssh.html) (també podria ser HTTPS si es prefereix aquesta opció).

```bash
git add .
```
Aquesta instrucció afegeix tots els fitxers nous i els canvis a l’àrea d’staging (o zona d’espera), preparant-los per a 
ser confirmats en el proper commit. El punt (.) significa que s’estan afegint tots els fitxers del directori actual.

```bash
git commit -m "Commit inicial"
```
Amb aquesta instrucció, confirmem els fitxers que hem afegit a l’àrea d’_staging_, creant un nou commit. 
L’opció **-m** s’utilitza per afegir un missatge descriptiu al commit, en aquest cas, "Commit inicial" per indicar que és el primer.

```bash
git push --set-upstream origin main
```
Finalment, aquesta instrucció envia els canvis locals (el commit que hem fet) al repositori remot a GitLab. L’opció `--set-upstream` especifica que la branca main local s’ha de connectar amb la branca main remota al repositori origin. 
Això permet que les futures instruccions `git push` i `git pull` sàpiguen a quin repositori i branca s’han de dirigir per defecte.

## 3.3. Com realitzar commits i seguiment de canvis

Un commit és un punt en l’historial del projecte que guarda un estat específic del teu codi. Segueix aquests passos per fer un commit:

1. **Afegeix els fitxers al “staging area”**:
```bash
git add nom-del-fitxer.txt
```

Per afegir tots els fitxers modificats:
```bash
git add .
```

2. **Realitza un commit**:

```bash
git commit -m "Descripció del canvi realitzat"
```


Com veure l’estat del repositori:

Per veure quins fitxers han estat modificats i quins estan preparats per al commit, utilitza:
```bash
git status
```

### Exemple pràctic:

1. Crea i inicialitza un nou repositori	
2. Modifica un fitxer existent o crea’n un de nou.
3. Afegeix el fitxer al “staging area”:
```bash
git add nou-arxiu.txt
```

3. **Realitza un commit amb un missatge descriptiu**:

```bash
git commit -m "Afegeixo nou-arxiu.txt amb les primeres línies de codi"
```

## 3.4. Com afegir fitxers i utilitzar add, commit i status

* `git add`: Aquesta ordre mou els canvis dels fitxers al “staging area”, preparant-los per al següent commit.
* `git commit`: El commit emmagatzema l’estat del projecte al repositori local amb un missatge que descriu els canvis.
* `git status`: Aquesta ordre mostra l’estat actual del repositori, incloent-hi els fitxers nous, modificats i els que estan en la “staging area”.