# 4. Treball amb Branques

## 4.1. Què són les branques i per què són importants?

Una **branca** és una línia de desenvolupament independent en un projecte de Git. Les branques permeten que diversos desenvolupadors treballin en diferents funcionalitats o correccions de manera simultània sense interferir en el treball dels altres.

### Importància de les branques:
- **Desenvolupament paral·lel**: Permeten treballar en múltiples funcionalitats alhora.
- **Seguretat**: Pots fer canvis sense afectar la branca principal (`main` o `master`) fins que estiguis preparat per fusionar-los.
- **Organització**: Ajuda a mantenir un codi més net i estructurat, evitant el caos en projectes col·laboratius.

## 4.2. Crear, canviar i suprimir branques

### Crear una branca nova:
* **Per crear una branca nova anomenada `nova-funcionalitat`, utilitza**:
```bash
git branch nova-funcionalitat
```

* **Per treballar en una branca específica, has de canviar a ella amb**:
```bash
git checkout nova-funcionalitat
```
**Nota**: Amb versions recents de Git, pots crear i canviar directament a una nova branca amb:
```bash
git switch -c nova-funcionalitat
```

* **Suprimir una branca**:
Un cop la branca ja no sigui necessària, pots suprimir-la amb:
```bash
git branch -d nova-funcionalitat
```

* **Per forçar la seva eliminació si la branca no ha estat fusionada**:
```bash
git branch -D nova-funcionalitat
```

## 4.3. Fusió de branques i resolució de conflictes

* **Fusió de branques**:

Per combinar els canvis d’una branca a la branca principal (main), primer assegura’t de trobar-te a la branca main:

```bash
git switch main
git merge nova-funcionalitat
``````
* **Resolució de conflictes**:

Quan els canvis de dues branques se superposen, Git generarà un conflicte de fusió. En aquests casos, hauràs de:

**1.**	Obrir els fitxers conflictius i revisar les seccions marcades amb <<<<<<<, ======= i >>>>>>>.
  * \<<<<<<<: Marca l’inici de la secció que prové de la branca actual en la qual et trobes.
  * =======: Separa les dues versions del codi que entren en conflicte.
  * \>>>>>>>: Marca el final de la secció en conflicte i l’inici de la versió de la branca que s’està fusionant.

**2.**	Editar els fitxers per mantenir els canvis desitjats.

**3.**	Afegir els fitxers resolts a l’“staging area”:

```bash
git add fitxer-conflictiu.txt
```

**4.** Finalitzar la fusió amb un commit:
```bash
git commit -m "Resolc conflictes i fusiono nova-funcionalitat a main"
```


### Exemple pràctic:

1. **Crea una nova branca i fes-hi alguns canvis**:
```bash
git checkout -b correccio-bug
# Modifica el fitxer "script.js" (per exemple. modifica un d'existent)
git add script.js
git commit -m "Corregit un bug a script.js"
```

2. **Torna a la branca main i fusiona**:

```bash
git switch main
git merge correccio-bug
```

#### Exemple de conflicte:

Suposa que tens un fitxer _exemple.txt_ amb el següent contingut després d’intentar una fusió:

```text
Aquest és un exemple de text.
<<<<<<< main
Aquest text prové de la branca principal.
=======
Aquest text prové de la branca que estàs fusionant.
>>>>>>> nova-funcionalitat
```

Com resoldre el conflicte:

1.	Llegeix el codi entre els marcadors per entendre les diferències.
2.	Edita el fitxer per observar la versió desitjada o combinar les dues versions de manera coherent.
3.	Un cop resolt el conflicte, elimina els marcadors (<<<<<<<, =======, >>>>>>>) i guarda el fitxer.
4.	Afegeix el fitxer resolt a l’“staging area” amb:
```bash
git add exemple.txt
```
5.	Finalitza la fusió amb un commit:
```bash
git commit -m "Resolc conflictes al fitxer exemple.txt"
```

## 4.4. Bones pràctiques per treballar amb branques

* Utilitza noms descriptius per a les branques: Com ara _feature-login, bugfix-crash-in-home_, etc.
* Fes commits petits i freqüents: Això facilita la revisió i la resolució de conflictes.
* Elimina les branques obsoletes: Un cop una branca ha estat fusionada, suprimeix-la per mantenir el repositori organitzat.
* Treballa amb branques locals i remotes: Puja les branques al repositori remot per compartir el treball amb l’equip:

```bash
git push origin nova-funcionalitat
```