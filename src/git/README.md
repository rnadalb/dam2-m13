# Git i la gestió de codi

## 1. [Introducció](doc/01-Introduccio.md)
- 1.1. Objectius generals del material
- 1.2. Importància de la gestió de codi en el desenvolupament de programari

## 2. [Conceptes fonamentals de Git](doc/02-conceptes_fonamentals.md)
- 2.1. Què és Git i per què utilitzar-lo?
- 2.2. Sistema de control de versions: Conceptes bàsics
- 2.3. Repositoris: Locals i remots
- 2.4. Introducció a GitLab i altres plataformes

## 3. [Primeres passes amb Git](doc/03-primeres_passes.md)
- 3.1. Instal·lació i configuració de Git
- 3.2. Creació d’un repositori local
- 3.3. Com realitzar commits i seguiment de canvis
- 3.4. Com afegir fitxers i utilitzar l’`add`, `commit` i `status`

## 4. [Treball amb branques (branch)](doc/04-treball_branques.md)
- 4.1. Què són les branques i per què són importants?
- 4.2. Crear, canviar i suprimir branques
- 4.3. Fusió de branques i resolució de conflictes
- 4.4. Bones pràctiques per treballar amb branques

## 5. [Col·laboració amb Git](doc/05-collaboracio_amb_git.md)
- 5.1. Configuració de repositoris remots
- 5.2. `clone`, `push`, `pull` i `fetch`: Treballar amb repositoris remots
- 5.3. Gestió de permisos i col·laboració amb equips
- 5.4. Fluxos de treball col·laboratius: Git Flow i altres patrons