
# MQTT en Python

## Introducció a MQTT

MQTT, acrònim de *Message Queuing Telemetry Transport*, és un protocol de missatgeria lleuger dissenyat per a la comunicació *machine-to-machine* (M2M) i l'Internet de les Coses (IoT). Es basa en un model de publicació/subscripció, on els dispositius poden publicar missatges en "tòpics" específics i subscriure's a aquests tòpics per rebre missatges rellevants. Aquest enfocament permet una comunicació eficient i escalable entre dispositius connectats.

### Característiques clau de MQTT

- **Lleugeresa**: MQTT està dissenyat per ser senzill i consumir pocs recursos, cosa que el fa ideal per a dispositius amb capacitats de processament i memòria limitades.

- **Fiabilitat**: Ofereix diferents nivells de Qualitat de Servei (QoS) per assegurar la lliurament de missatges segons les necessitats de l'aplicació.

- **Escalabilitat**: El model de publicació/subscripció permet la comunicació entre múltiples dispositius sense necessitat de connexions directes entre ells.

### Usos comuns de MQTT

MQTT s'utilitza àmpliament en aplicacions IoT i M2M, incloent:

- **Automatització domèstica**: Control de dispositius intel·ligents com llums, termòstats i sistemes de seguretat.

- **Monitoratge industrial**: Supervisió de sensors i equips en entorns industrials per a la gestió eficient de processos.

- **Automoció**: Comunicació entre vehicles i sistemes d'infraestructura per a aplicacions com la telemàtica i la conducció autònoma.

- **Salut**: Monitoratge remot de pacients mitjançant dispositius mèdics connectats.

## Descripció de l'aplicació

Aquesta aplicació en Python permet als usuaris connectar-se a un servidor MQTT, subscriure's a un tòpic específic i enviar missatges a aquest tòpic. Proporciona una interfície de línia de comandes que facilita la interacció amb el servidor MQTT i la gestió de missatges.

### Funcionalitats principals

- **Connexió a un servidor MQTT**: L'usuari pot introduir l'adreça del servidor, el nom d'usuari i la contrasenya per establir una connexió segura.

- **Subscriure's a un tòpic**: Permet subscriure's a un tòpic específic i mostrar els missatges rebuts en format tabular.

- **Enviar missatges**: Facilita l'enviament de missatges al tòpic seleccionat.

- **Selecció del format de la taula**: L'usuari pot triar el format en què es mostren els missatges rebuts, utilitzant la biblioteca `tabulate`.

## Requisits

- **Python 3.x**: Assegureu-vos de tenir instal·lada una versió recent de Python 3.

- **Llibreries Python**: Les següents llibreries són necessàries ([requirements.txt](requirements.txt)):

  - `paho-mqtt`: Per a la comunicació MQTT.

  - `tabulate`: Per formatar taules de dades.

**Recordeu instal·lar les dependències**:

   ```bash
   pip install -r requirements.txt
   ```

## Ús

1. **Executar l'aplicació**:

   ```bash
   python -m mqtt_app
   ```

2. **Introduir les dades de connexió**:

   Quan se us demani, introduïu l'adreça del servidor MQTT, el nom d'usuari, la contrasenya i el tòpic al qual voleu subscriure-us o enviar missatges.

3. **Seleccionar el format de la taula**:

   Trieu el format en què voleu que es mostrin els missatges rebuts. Les opcions disponibles inclouen `plain`, `grid`, `pipe`, `simple`, `fancy_grid`, `github`, `html`, `latex`, `latex_raw`, `latex_booktabs` i `tsv`.

4. **Interacció amb el menú**:

   Després de la configuració inicial, se us presentarà un menú amb les següents opcions:

   - **Subscriure's al tòpic**: Per començar a rebre i mostrar els missatges del tòpic seleccionat.

   - **Enviar dades al tòpic**: Per enviar un missatge al tòpic.

   - **Sortir**: Per desconnectar-se i tancar l'aplicació.

   *Nota*: Quan esteu subscrits a un tòpic, podeu prémer `Ctrl+C` per tornar al menú principal.

## Exemple de sortida

Després d'executar l'aplicació amb `python __main__.py`, la interacció amb l'usuari podria ser similar a la següent:

```
--- Configuració del client MQTT ---
Introdueix l'adreça del servidor MQTT: broker.emqx.io
Introdueix el nom d'usuari: usuari_exemple
Introdueix la contrasenya: ********
Introdueix el tòpic al qual vols subscriure't o enviar dades: sensors/temperatura

--- Selecciona el format de la taula ---
1. plain
2. grid
3. pipe
4. simple
5. fancy_grid
6. github
7. html
8. latex
9. latex_raw
10. latex_booktabs
11. tsv
Selecciona una opció de format (1-11): 2

Client MQTT connectat correctament.

--- Menú ---
1. Subscriure's al tòpic
2. Enviar dades al tòpic
3. Sortir
Selecciona una opció: 1

Subscriure's al tòpic: sensors/temperatura
+---------------------+-----------+
| Data i Hora         | Missatge  |
+---------------------+-----------+
| 2025-01-27 21:40:19 | 22.5°C    |
| 2025-01-27 21:41:10 | 22.7°C    |
| 2025-01-27 21:42:05 | 22.6°C    |
+---------------------+-----------+
```
