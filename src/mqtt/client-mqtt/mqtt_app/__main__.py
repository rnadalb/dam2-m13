from mqtt_app.mqtt_client import MQTTClient


def menu():
    """Mostra un menú per introduir les dades del servidor MQTT i seleccionar accions."""
    print("--- Configuració del client MQTT ---")
    server = input("Introdueix l'adreça del servidor MQTT: ")
    username = input("Introdueix el nom d'usuari: ")
    password = input("Introdueix la contrasenya: ")
    topic = input("Introdueix el tòpic al qual vols subscriure't o enviar dades: ")

    print("\n--- Selecciona el format de la taula ---")
    print("1. plain")
    print("2. grid")
    print("3. pipe")
    print("4. simple")
    print("5. fancy_grid")
    print("6. github")
    print("7. html")
    print("8. latex")
    print("9. latex_raw")
    print("10. latex_booktabs")
    print("11. tsv")

    format_options = {
        "1": "plain",
        "2": "grid",
        "3": "pipe",
        "4": "simple",
        "5": "fancy_grid",
        "6": "github",
        "7": "html",
        "8": "latex",
        "9": "latex_raw",
        "10": "latex_booktabs",
        "11": "tsv"
    }

    format_choice = input("Selecciona una opció de format (1-11): ")
    table_format = format_options.get(format_choice, "plain")

    return server, username, password, topic, table_format


def main():
    server, username, password, topic, table_format = menu()

    mqtt_client = MQTTClient(server, username, password, topic, table_format)
    mqtt_client.connect()

    while True:
        print("\n--- Menú ---")
        print("1. Subscriure's al tòpic")
        print("2. Enviar dades al tòpic")
        print("3. Sortir")

        try:
            option = int(input("Selecciona una opció: "))

            if option == 1:
                mqtt_client.subscribe()
            elif option == 2:
                message = input("Introdueix el missatge a enviar: ")
                mqtt_client.publish(message)
            elif option == 3:
                print("Sortint...")
                mqtt_client.client.loop_stop()
                mqtt_client.client.disconnect()
                print("Desconnectat.")
                break
            else:
                print("Opció no vàlida. Torna-ho a intentar.")

        except ValueError:
            print("Si us plau, introdueix un número vàlid.")
        except KeyboardInterrupt:
            print("\nSortint...")
            mqtt_client.client.loop_stop()
            mqtt_client.client.disconnect()
            print("Desconnectat.")
            break


if __name__ == "__main__":
    main()
