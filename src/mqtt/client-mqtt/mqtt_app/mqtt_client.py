import paho.mqtt.client as mqtt
import signal
import sys
from datetime import datetime
from tabulate import tabulate


class MQTTClient:
    """
    Defineix un client MQTT per gestionar la subscripció a un tòpic i l'enviament de dades.

    Atributs:
        server (str): Adreça del servidor MQTT.
        port (int): Port del servidor MQTT (per defecte 1883).
        username (str): Nom d'usuari per l'accés segur.
        password (str): Contrasenya per l'accés segur.
        topic (str): Tòpic MQTT al qual subscriure's o enviar dades.
        client (mqtt.Client): Instància del client MQTT.
        messages (list): Llista per emmagatzemar els missatges rebuts.
        table_format (str): Format de la taula per mostrar els missatges.

    Mètodes:
        connect(): Connecta al servidor MQTT.
        subscribe(): Subscriu el client al tòpic definit i mostra els missatges en format tabular.
        publish(message): Envia dades al tòpic definit.
        on_message(client, userdata, message): Gestiona els missatges rebuts del tòpic.
        signal_handler(sig, frame): Gestiona la sortida segura quan es prem Ctrl+C.
    """

    def __init__(self, server, username, password, topic, table_format, port=1883):
        """Inicialitza el client MQTT amb les dades de connexió."""
        self.server = server
        self.port = port
        self.username = username
        self.password = password
        self.topic = topic
        self.client = mqtt.Client()
        self.client.username_pw_set(self.username, self.password)
        self.client.on_message = self.on_message
        self.messages = []
        self.table_format = table_format

    def connect(self):
        """Connecta al servidor MQTT utilitzant les dades proporcionades."""
        try:
            self.client.connect(self.server, self.port, 60)
            print("Client MQTT connectat correctament.")
        except Exception as e:
            print(f"Error connectant al servidor MQTT: {e}")

    def subscribe(self):
        """Subscriu el client al tòpic definit i mostra els missatges en format tabular."""
        try:
            self.client.subscribe(self.topic)
            print(f"Subscriure's al tòpic: {self.topic}")
            print("---- Tòpic -- Prem Ctrl+C per sortir --")
            print("{:<20} | {}".format("Data i Hora", "Missatge"))
            print("-" * 50)
            self.client.loop_start()
            signal.signal(signal.SIGINT, self.signal_handler)
            while True:
                pass
        except Exception as e:
            print(f"Error subscrivint-se al tòpic '{self.topic}': {e}")

    def publish(self, message):
        """Envia dades al tòpic definit."""
        try:
            self.client.publish(self.topic, message)
            print(f"Missatge enviat al tòpic '{self.topic}': {message}")
        except Exception as e:
            print(f"Error enviant dades al tòpic '{self.topic}': {e}")

    def on_message(self, client, userdata, message):
        """Gestiona els missatges rebuts del tòpic."""
        timestamp = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
        msg = message.payload.decode()
        self.messages.append([timestamp, msg])
        print(tabulate(self.messages, headers=["Data i Hora", "Missatge"], tablefmt=self.table_format))

    def signal_handler(self, sig, frame):
        """Gestiona la sortida segura quan es prem Ctrl+C."""
        print("\nDesconnectant del client MQTT...")
        self.client.loop_stop()
        self.client.disconnect()
        print("Desconnectat.")
        sys.exit(0)
