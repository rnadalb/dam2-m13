class Jugador:
    def __init__(self, nickname):
        self.nickname = nickname
        self.intents_encertats = 0

    def incrementar_intents_encertats(self):
        self.intents_encertats += 1

    def __str__(self):
        return f"Nickname: {self.nickname}, Intents Encertats: {self.intents_encertats}"
