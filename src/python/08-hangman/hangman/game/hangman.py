import datetime
import random

from hangman.game.jugador import Jugador

# Adreça del servidor web que conté les paraules
_DEFAULT_MAX_INTENTS = 6
_DEFAULT_ALFABET = "abcdefghijklmnopqrstuwxyzàéèíóòúç"


class HangmanGame:
    def __init__(self, alfabet=_DEFAULT_ALFABET, llista_paraules=None, max_intents=_DEFAULT_MAX_INTENTS):
        self.paraules = llista_paraules
        self.alfabet = alfabet
        self.max_intents = max_intents
        self.titol = """
          _   _    _    _   _  ____ __  __    _    _   _ 
         | | | |  / \  | \ | |/ ___|  \/  |  / \  | \ | |
         | |_| | / _ \ |  \| | |  _| |\/| | / _ \ |  \| |
         |  _  |/ ___ \| |\  | |_| | |  | |/ ___ \| |\  |
         |_| |_/_/   \_\_| \_|\____|_|  |_/_/   \_\_| \_|

        """

        self.jugador = None
        self.paraula = None
        self.lletres_encertades = set()
        self.intents = 1

    def mostrar_progres(self):
        progres = ""
        for lletra in self.paraula:
            if lletra in self.lletres_encertades:
                progres += lletra
            else:
                progres += "_"
        return progres + f" [{self.intents}/{self.max_intents}]"

    def jugar(self):
        # Reiniciem els comptadors ...
        self.intents = 1
        self.jugador = Jugador(input("Introdueix el teu nickname: "))
        self.paraula = random.choice(self.paraules)  # Escollim 1 paraula aleatòria
        self.lletres_encertades.clear()
        torns = 1
        encertat = False

        while not encertat and self.intents <= self.max_intents:
            torns += 1
            progres = self.mostrar_progres()
            print(f"Paraula a endevinar: {progres}")
            lletra = input("Introdueix una lletra: ").lower()

            if len(lletra) != 1 or lletra not in self.alfabet:
                print("Introdueix una sola lletra vàlida.")

            if lletra in self.lletres_encertades:
                print("Ja has intentat aquesta lletra.")

            if lletra in self.paraula:
                self.lletres_encertades.add(lletra)
                print("Encertat!")
            else:
                print("Lletra incorrecta.")
                self.intents += 1  # Comptem els intents fallats

            if len(self.lletres_encertades) == len(set(self.paraula)):
                print(f"Has guanyat! La paraula era '{self.paraula}'.")
                data = datetime.datetime.now().strftime("%d-%m-%Y")
                self.registrar_puntuacio(data, torns - 1)
                encertat = True

            if self.intents > self.max_intents:
                print(f"Has superat el límit d'intents. La paraula era '{self.paraula}'.")
                encertat = True

    def registrar_puntuacio(self, data, torns):
        with open("hall_of_fame.txt", "a") as f:
            f.write(f"{data}\t{self.jugador.nickname}\t{self.paraula}\t{torns - self.intents}\n")

    @staticmethod
    def visualitzar_hall_of_fame():
        print("Hall of Fame:")
        print("Data\tNickname\tParaula Endevinada\nTorns Necessaris")
        print("-------------------------------------------------------------")
        with open("hall_of_fame.txt", "r") as f:
            for linia in f:
                data, nickname, paraula, torns = linia.strip().split("\t")
                print(f"{data}\t{nickname}\t{paraula}\t{torns}")

    def run(self):
        salta = True
        while salta:
            print("Benvingut a Hangman!")
            print(self.titol)
            print("1. Jugar Hangman")
            print("2. Visualitzar Hall of Fame")
            print("3. Sortir")
            opcio = input("Selecciona una opció: ")

            if opcio == "1":
                self.jugar()
            elif opcio == "2":
                self.visualitzar_hall_of_fame()
            elif opcio == "3":
                salta = False
            else:
                print("Opció no vàlida. Torna a intentar-ho.")
