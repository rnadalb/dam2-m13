#!/usr/bin/env python3
from hangman.game.hangman import HangmanGame
from hangman.utils import utils

if __name__ == "__main__":
    # Des del web
    # paraules = utils.obte_llista_paraules()
    # print(paraules)
    # Des d'una base de dades -> from_db=True
    paraules = utils.obte_llista_paraules(from_db=True, limit=100)
    print(paraules)
    joc_penjat = HangmanGame(llista_paraules=paraules, max_intents=4)
    joc_penjat.run()

