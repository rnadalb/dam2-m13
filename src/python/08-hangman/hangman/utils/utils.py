import psycopg2
import requests

from hangman.db.database_config import DatabaseConfig
from hangman.db.database_connection import DatabaseConnection

_DEFAULT_URL = "https://gitlab.com/rnadalb/dam2-m13/-/raw/main/src/python/08-hangman/paraules.txt"
_MIN = 100
_PARAULES = ["camió", "exercici", "hangman", "bicicleta", "barcelona", "llibre", "terra", "muntanya", "mar", "estudi",
             "pintura", "ballar", "menjar", "cervesa", "copa", "familia", "amistat", "alegria", "color", "guitarra", ]


def obte_llista_paraules(from_db: bool = False, limit: int = _MIN, wordlist_url: str = _DEFAULT_URL) -> []:
    if from_db:
        return __paraules_bd(limit)
    elif len(wordlist_url) > 0:
        return __paraules_web(wordlist_url)


def __paraules_web(wordlist_url=_DEFAULT_URL):
    # Descarrega la llista de paraules des del servidor web
    response = requests.get(wordlist_url)
    if response.status_code == requests.codes.ok:
        wordlist = response.text.splitlines()
        return wordlist
    else:
        # Si no pot descarregar-se, utilitzem les paraules per defecte
        return _PARAULES


def __paraules_bd(quantitat=_MIN):
    paraules = []
    config = DatabaseConfig()
    with DatabaseConnection(config) as conn:
        try:
            paraules = conn.execute_query(query="SELECT paraula FROM tbl_paraules ORDER BY RANDOM() LIMIT %s;",
                                          params=(quantitat,))
            paraules = [paraula[0] for paraula in paraules]
        except psycopg2.Error as e:
            print(f"Error al obtenir la llista de paraules: {e}")
            paraules = _PARAULES.copy()
    return paraules
