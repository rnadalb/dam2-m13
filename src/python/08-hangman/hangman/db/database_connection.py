# Classe que gestiona la connexió amb la base de dades
import psycopg2

from hangman.db.database_config import DatabaseConfig


class DatabaseConnection:
    def __init__(self, config: DatabaseConfig):
        """
        Constructor de la classe
        :param config: el contingut del fitxer de configuració de la base de dades
        """
        self.config = config
        self.connection = None

    def connect(self):
        """
        Estableix una connexió amb la base de dades
        :return: la connexió establerta
        """
        try:
            self.connection = psycopg2.connect(
                host=self.config.host,
                database=self.config.database,
                user=self.config.user,
                password=self.config.password,
                port=self.config.port,
                sslmode=self.config.sslmode
            )
        except Exception as error:
            print(f"Error connectant a la base de dades: {error}")
            raise

    def execute_query(self, query: str, params: tuple = None):
        """
        Executa una consulta a la base de dades i retorna les files i els noms de les columnes
        :param query: consulta SQL a executar
        :param params: paràmetres de la consulta
        :return: files de la consulta (taula completa o selecció)
        """
        try:
            with self.connection.cursor() as cursor:
                cursor.execute(query, params)
                rows = cursor.fetchall()
                return rows
        except Exception as error:
            print(f"Error executant la consulta: {error}")
            raise

    def close(self):
        """
        Tanca la connexió amb la base de dades
        """
        if self.connection:
            self.connection.close()

    # Mètode per gestionar l'entrada al context i establir la connexió
    def __enter__(self):
        self.connect()
        return self  # Retorna l'objecte DatabaseConnection (ho espera el context)

    # Mètode per gestionar la sortida del context i tancar la connexió
    def __exit__(self, exc_type, exc_value, traceback):
        if self.connection:
            self.connection.close()
