from dataclasses import dataclass

from gestio_esdeveniments.esdeveniments.esdeveniment import Esdeveniment
from gestio_esdeveniments.persones.estudiant import Estudiant
from gestio_esdeveniments.persones.jubilat import Jubilat
from gestio_esdeveniments.persones.persona import Persona


@dataclass
class Concert(Esdeveniment):
    """Classe que representa un concert."""

    artista: str

    def aplicar_descompte(self, persona: Persona):
        """Aplica el descompte segons la classe de la persona per als concerts."""
        if isinstance(persona, Estudiant):
            return 0.2
        elif isinstance(persona, Jubilat):
            return 0.5
        else:
            return 0.0

    def __str__(self):
        return super().__str__() + f" : {self.artista}"
