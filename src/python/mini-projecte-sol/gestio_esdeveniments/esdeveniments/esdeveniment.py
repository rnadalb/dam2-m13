from dataclasses import dataclass

from gestio_esdeveniments.persones.persona import Persona


@dataclass
class Esdeveniment:
    """Classe que representa un esdeveniment."""
    nom: str
    data: str
    preu: float
    ubicacio: str
    participants: list

    def mostrar_info(self):
        """Mostra la informació de l'esdeveniment."""
        print(f"Nom: {self.nom}")
        print(f"Data: {self.data}")
        print(f"Preu: {self.preu}")
        print(f"Ubicació: {self.ubicacio}")

    def inscriu_persona(self, persona: Persona):
        """Inscriu una persona a l'esdeveniment."""
        self.participants.append(persona)

    def aplicar_descompte(self, persona: Persona):
        """Aplica el descompte segons la classe de la persona."""
        return 0.0

    def mostrar_participants(self):
        """Mostra els participants d'un esdeveniment de manera tabulada."""
        print(f"Participants a {self.nom}:")
        print("{:<20} {:<10} {:<15}".format("Nom", "Edat", "Email"))
        for persona in self.participants:
            print("{:<20} {:<10} {:<15}".format(persona.nom, persona.edat, persona.email))
        print("\n")

    def __str__(self):
        return f"{self.data} --> {self.nom}: {self.preu} €"
