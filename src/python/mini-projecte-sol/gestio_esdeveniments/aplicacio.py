from gestio_esdeveniments.esdeveniments.concert import Concert
from gestio_esdeveniments.esdeveniments.conferencia import Conferencia
from gestio_esdeveniments.gestor.gestor_esdeveniments import GestorEsdeveniments
from gestio_esdeveniments.persones.estudiant import Estudiant
from gestio_esdeveniments.persones.jubilat import Jubilat
from gestio_esdeveniments.persones.treballador import Treballador


def inicialitza_gestor():
    gestor = GestorEsdeveniments()
    concert1 = Concert("Concert Punk", "15/10/2023", 20.0, "Sidecar", [], "SCUMBAG MILLONAIRE")
    concert2 = Concert("Concert Hip Hop", "20/11/2023", 30.0, "Razzmatazz", [], "JUANCHO MARQUÉS")
    conferencia1 = Conferencia("Software Crafters", "25/11/2023", 150.0, "Escola d'Arts i Disseny de Barcelona",
                               [], "SOFTWARE CRAFTERS")
    gestor.crear_esdeveniment(concert1)
    gestor.crear_esdeveniment(concert2)
    gestor.crear_esdeveniment(conferencia1)
    return gestor


def obte_opcio():
    print("Què vols fer?")
    print("1. Mostrar esdeveniments disponibles")
    print("2. Inscriure's a un esdeveniment")
    print("3. Mostrar les persones inscrites per esdeveniment")
    print("4. Sortir")
    opcio = input("Selecciona una opció: ")
    return opcio


def main():
    gestor = inicialitza_gestor()

    print("Benvingut al Sistema de Gestió d'Esdeveniments!")

    while True:
        opcio = obte_opcio()

        if opcio == "1":
            gestor.mostrar_esdeveniments()
        elif opcio == "2":
            gestor.mostrar_esdeveniments()
            esdeveniment_idx = int(input("Selecciona un esdeveniment pel seu índex: "))

            if 0 <= esdeveniment_idx < len(gestor.esdeveniments):
                nom = input("Introdueix el teu nom: ")
                edat = int(input("Introdueix la teva edat: "))
                email = input("Introdueix el teu correu electrònic: ")
                classe = input("Introdueix si ets estudiant o treballador: ")

                if classe.lower() == "estudiant":
                    estudis = input("Ets estudiant, pots indicar-nos que estudies? ")
                    persona = Estudiant(nom, edat, email, estudis)
                elif classe.lower() == "treballador":
                    ocupacio = input("Ets treballador/a, pots indicar-nos la teva ocupació? ")
                    persona = Treballador(nom, edat, email, ocupacio)
                else:
                    persona = Jubilat(nom, edat, email)

                gestor.inscriure_persona(esdeveniment_idx, persona)
            else:
                print("Índex d'esdeveniment invàlid.")
        elif opcio == "3":
            for esdeveniment in gestor.esdeveniments:
                esdeveniment.mostrar_participants()
        elif opcio == "4":
            print("Gràcies per utilitzar el Sistema de Gestió d'Esdeveniments.")
            break
        else:
            print("Opció invàlida. Si us plau, selecciona una opció vàlida.")


if __name__ == "__main__":
    main()
