from dataclasses import dataclass

from gestio_esdeveniments.persones.persona import Persona


@dataclass
class Estudiant(Persona):
    """Classe que representa un estudiant"""
    estudis: str
