from dataclasses import dataclass

from gestio_esdeveniments.persones.persona import Persona


@dataclass
class Treballador(Persona):
    """Classe que descriu un treballador del Futur."""
    ocupacio: str
