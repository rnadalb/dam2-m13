from dataclasses import dataclass

from gestio_esdeveniments.persones.persona import Persona


@dataclass
class Jubilat(Persona):
    """Classe que representa una persona jubilada."""
    pass
