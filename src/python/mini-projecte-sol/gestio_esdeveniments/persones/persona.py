from dataclasses import dataclass


@dataclass
class Persona:
    """Classe base que representa una persona."""
    nom: str
    edat: int
    email: str

    def __str__(self):
        """Retorna la informació de la persona."""
        return f"Nom: {self.nom}\nEdat: ({self.edat}|)\nEmail: {self.email}"
