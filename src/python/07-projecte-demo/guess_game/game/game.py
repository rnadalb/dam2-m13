import random

from guess_game.db.database import Database
from guess_game.db.database_config import DatabaseConfig


class Game:
    def __init__(self, jugador):
        self.jugador = jugador
        self.nombre_secret = self.generar_nombre_aleatori()
        self.db = Database(DatabaseConfig())

    def generar_nombre_aleatori(self):
        return random.randint(1, 100)

    def jugar(self):
        intents = 0
        while True:
            intents += 1
            try:
                nombre = int(input("Endevina el nombre (entre 1 i 100): "))
                if nombre < self.nombre_secret:
                    print("Massa petit. Torna a intentar-ho.")
                elif nombre > self.nombre_secret:
                    print("Massa gran. Torna a intentar-ho.")
                else:
                    print(f"Encertat! Has endevinat el nombre en {intents} intents.")
                    self.jugador.incrementar_encerts()
                    self.db.registrar_encert(self.jugador.nickname, intents)
                    break
            except ValueError:
                print("Si us plau, introdueix un nombre vàlid.")
        self.db.disconnect()

    def recuperar_puntuacions(self):
        return self.db.recuperar_puntuacions()
