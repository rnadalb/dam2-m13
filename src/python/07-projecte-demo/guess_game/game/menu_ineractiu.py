from guess_game.db.database import Database
from guess_game.db.database_config import DatabaseConfig


class MenuInteractiu:
    def mostrar_menu(self):
        print("\nBenvingut a Guess Game!")
        print("1. Jugar")
        print("2. Veure Puntuacions")
        print("3. Sortir")
        return input("Selecciona una opció: ")

    def veure_puntuacions(self):
        db = Database(DatabaseConfig())
        puntuacions = db.recuperar_puntuacions()
        print("\nHall of Fame:")
        for puntuacio in puntuacions:
            print(f"{puntuacio['data']} - {puntuacio['nickname']} - {puntuacio['intents']} intents")
