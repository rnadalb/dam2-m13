import sys

from guess_game.db.database import Database
from guess_game.db.database_config import DatabaseConfig
from guess_game.game.game import Game
from guess_game.game.jugador import Jugador
from guess_game.game.menu_ineractiu import MenuInteractiu


def main():
    menu = MenuInteractiu()
    while True:
        opcio = menu.mostrar_menu()
        if opcio == "1":
            nickname = input("Introdueix el teu nickname: ")
            jugador = Jugador(nickname)
            game = Game(jugador)
            game.jugar()
        elif opcio == "2":
            menu.veure_puntuacions()
        elif opcio == "3":
            print("Sortint del joc...")
            break


def create_hall_of_fame():
    db = Database(DatabaseConfig())
    db.crear_taula()
    db.disconnect()


if __name__ == '__main__':
    create_hall_of_fame()
    main()
    # print("Aquesta aplicació no es pot executar")

