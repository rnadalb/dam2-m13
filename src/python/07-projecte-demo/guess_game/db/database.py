import psycopg2
from psycopg2 import sql
from datetime import datetime

from guess_game.db.database_config import DatabaseConfig


class Database:
    def __init__(self, config: DatabaseConfig):
        self.connection = None
        self.config = config
        self.connect()

    def connect(self):
        """Estableix la connexió amb la base de dades utilitzant la cadena de connexió."""
        try:
            self.connection = psycopg2.connect(
                host=self.config.host,
                database=self.config.database,
                user=self.config.user,
                password=self.config.password,
                port=self.config.port
            )
            print("Connexió amb la base de dades establerta amb èxit.")
        except Exception as e:
            print(f"Error connectant a la base de dades: {e}")
            raise

    def disconnect(self):
        """Tanca la connexió amb la base de dades."""
        if self.connection:
            self.connection.close()
            print("Connexió amb la base de dades tancada.")

    def registrar_encert(self, nickname, intents):
        """Enregistra un encert del jugador a la taula 'hall_of_fame'."""
        try:
            with self.connection.cursor() as cursor:
                query = sql.SQL("INSERT INTO hall_of_fame (nickname, intents, data) VALUES (%s, %s, %s)")
                cursor.execute(query, (nickname, intents, datetime.now()))
                self.connection.commit()
                print(f"Encert enregistrat per {nickname} amb {intents} intents.")
        except Exception as e:
            print(f"Error en registrar l'encert: {e}")
            self.connection.rollback()

    def recuperar_puntuacions(self):
        """Recupera les puntuacions de la taula 'hall_of_fame' i les ordena per nombre d'intents."""
        try:
            with self.connection.cursor() as cursor:
                query = sql.SQL("SELECT nickname, intents, data FROM hall_of_fame ORDER BY intents ASC")
                cursor.execute(query)
                result = cursor.fetchall()
                print("Puntuacions recuperades.")
                return [{'nickname': row[0], 'intents': row[1], 'data': row[2]} for row in result]
        except Exception as e:
            print(f"Error en recuperar les puntuacions: {e}")
            return []

    def crear_taula(self):
        """Crea la taula 'hall_of_fame' si no existeix."""
        try:
            with self.connection.cursor() as cursor:
                query = """
                CREATE TABLE IF NOT EXISTS hall_of_fame (
                    id SERIAL PRIMARY KEY,
                    nickname VARCHAR(50),
                    intents INT,
                    data TIMESTAMP
                );
                """
                cursor.execute(query)
                self.connection.commit()
                print("Taula 'hall_of_fame' creada (o ja existeix).")
        except Exception as e:
            print(f"Error en crear la taula: {e}")
            self.connection.rollback()
