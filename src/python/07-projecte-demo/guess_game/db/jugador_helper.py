# Classe que gestiona operacions amb la taula tbl_ggame
from typing import Any
from guess_game.db.database_connection import DatabaseConnection
from guess_game.game.jugador import Jugador


class JugadorHelper:
    def __init__(self, db_connection: DatabaseConnection):
        """
        Inicialitza la classe JugadorHelper amb una connexió a la base de dades.

        :param db_connection: Connexió a la base de dades.
        """
        self.db_connection = db_connection

    def get_all_players(self) -> Any:
        """
        Obté tots els jugadors de la taula tbl_usuari.

        :return: Resultat de la consulta SQL.
        """
        query = "SELECT * FROM tbl_ggame ORDER BY id DESC;"
        return self.db_connection.execute_query(query)

    def insert_score(self, jugador: Jugador) -> Any:
        """
        Insereix un nou usuari a la taula tbl_usuari.

        :param jugador: jugador que es vol inserir.
        :return: Resultat de la consulta SQL.
        """
        query = "INSERT INTO tbl_ggame (nickname, score) VALUES (%s, %s);"
        return self.db_connection.execute_query(query, (jugador.nickname, jugador.encerts))
