import os

from dotenv import load_dotenv


class DatabaseConfig:
    def __init__(self):
        load_dotenv()  # Carrega les variables d'entorn des del fitxer .env
        self.host = os.getenv("POSTGRES_HOST")
        self.database = os.getenv("POSTGRES_DBNAME")
        self.user = os.getenv("POSTGRES_USER")
        self.password = os.getenv("POSTGRES_PASSWORD")
        self.port = os.getenv("POSTGRES_PORT")
