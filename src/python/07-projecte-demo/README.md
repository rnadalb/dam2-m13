# Mini Projecte: Guess Game - Joc d'Endevinar el Nombre 🎮

Benvinguts al fascinant món dels jocs d'ordinador! En aquest mini projecte, crearem una aplicació divertida anomenada "Guess Game" on els jugadors hauran de posar a prova les seves habilitats d'endevinar nombres aleatoris. 🤔💡

## Tasques a desenvolupar 🚀
Les vostres tasques consistiran en:

1. **Classe Jugador:** 🕵️‍♂️

   a) Crea una classe anomenada **`Jugador`** que tindrà dos atributs: **`nickname`** i **`encerts`**.

   b) Implementa un mètode **`incrementar_encerts`** que augmentarà el comptador d'encerts cada vegada que el jugador encerti un nombre.


2. **Classe Game:** 🤫

   a) Crea una classe anomenada **`Game`** que gestionarà el joc.

   b) Implementa un mètode **`generar_nombre_aleatori`** per generar un nombre aleatori entre 1 i 100.

   c) Implementa un mètode **`jugar`** que permeti als jugadors intentar endevinar el nombre aleatori generat. Utilitza la instància de la classe **`Jugador`** per gestionar les dades del jugador durant el joc.

   d) Implementa un mètode **`registrar_encert`** que enregistri el **nickname**, la **data** i els **encerts** del jugador en una taula de PostgreSQL.


3. **Classe MenuInteractiu:** ⌨️

   a) Crea una classe anomenada **`MenuInteractiu`** que mostrarà el menú principal i les puntuacions.

   b) Implementa un mètode **`mostrar_menu`** que visualitzarà les opcions disponibles (Jugar, Veure Puntuacions, Sortir).

   c) Implementa un mètode **`veure_puntuacions`** que mostrarà les puntuacions del Hall of Fame, extretes de la base de dades, ordenades de major a menor nombre d'encerts.

4. **Base de Dades PostgreSQL:** 🗃️

   a) Configura una base de dades **PostgreSQL** en un contenidor Docker per enregistrar les dades del Hall of Fame.

   b) Crea una taula per a les puntuacions amb els camps **nickname**, **data** i **encerts**.

   c) Implementa la connexió entre l'aplicació i la base de dades per tal de registrar els encerts dels jugadors i recuperar-los posteriorment per mostrar-los.

5. **Dockeritza la teva aplicació amb Docker Compose:** 🐳

   a) Desenvolupa un fitxer **`docker-compose.yml`** per a la teva aplicació. Aquest fitxer ha d'incloure dos serveis principals:
   - **L'aplicació**: que serà l'executable principal.
   - **PostgreSQL**: per gestionar la base de dades del Hall of Fame.

   b) Configura el servei de PostgreSQL dins de **Docker Compose**, definint els ports necessaris, les credencials i els volums per a la persistència de dades.

   c) Configura el servei de l'aplicació perquè es connecti a la base de dades PostgreSQL utilitzant el nom del servei de PostgreSQL definit en el fitxer `docker-compose.yml`.

   d) Utilitza **`docker-compose up`** per executar tant l'aplicació com el contenidor de PostgreSQL.

---

## Exemple de sortida 📋
```text
Benvingut a Guess Game - Joc d'Endevinar el Nombre!

1. Jugar
2. Veure Puntuacions
3. Sortir

Selecciona una opció: 1

Introdueix el teu nickname: Sherlock

Endevina el nombre (entre 1 i 100): 42
Massa gran. Torna a intentar-ho.
Endevina el nombre (entre 1 i 100): 20
Massa petit. Torna a intentar-ho.
Endevina el nombre (entre 1 i 100): 30
Encertat! Has endevinat el nombre en 3 intents.

1. Jugar
2. Veure Puntuacions
3. Sortir

Selecciona una opció: 2

Hall of Fame:
03/10/2023 Sherlock ha trigat en encertar: 3
...

1. Jugar
2. Veure Puntuacions
3. Sortir

Selecciona una opció: 3
```
### Principis del Codi d'Honor

*L'ús de la IA ha de ser una eina d'aprenentatge i millora personal, no una forma de trampa que minvi el teu progrés, comprensió dels conceptes i capacitat d'assolir reptes més complexos*

1. **Autenticitat en l'aprenentatge**: Utilitza l'intel·ligència artificial per entendre els problemes i desenvolupar les teves habilitats, no per evadir els reptes d'aprenentatge.

2. **Col·laboració ètica**: Col·labora amb altres estudiants de manera ètica i transparent. Ajuda'ls a comprendre i superar els obstacles, però no els donis solucions completes si això compromet la seva pròpia comprensió.

3. **Reconeixement dels recursos**: Si utilitzes codi, solucions o materials d'altres fonts, assegura't de reconèixer i citar adequadament aquests recursos. La honestitat intel·lectual és fonamental.

4. **Responsabilitat personal**: La responsabilitat pel teu aprenentatge i èxit recau en tu mateix. Utilitza les eines d'intel·ligència artificial com a suport, no com a substitut de l'esforç.