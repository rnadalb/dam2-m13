# Mini-projecte 
🚀 El Món de les Inscripcions d'Esdeveniments amb contenidors 🎉


## Descripció

Recordes el mini-projecte del [món de les inscripcions](../09-gestor-esdeveniments/)?

### Quin repte ens proposem?

Utilitzar [Docker](https://www.docker.com/) per executar el nostre programari i facilitar el desplegament.

### Sobre docker ...
Aquí trobaràs el [document](/resources/DAM2_M13_Te15_introduccio_docker.pdf) que hem treballat a classe.