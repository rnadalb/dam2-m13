## Mini-projecte 
🚀 El Món de les Inscripcions d'Esdeveniments 🎉


### Descripció

Benvinguts a la nostra aventura, on explorarem el fascinant món de les inscripcions d'esdeveniments! Imagina que estàs gestionant un Centre d'Esdeveniments  i necessitem el teu ajut per desenvolupar un sistema de gestió que sigui eficient !

La missió consisteix a crear un sistema que permeti als habitants de diferents galàxies inscriure's a esdeveniments èpics, com concerts i conferències. ***Wow !*** Però aquí està la gràcia: hem d'aplicar descomptes d'acord a la classe de les persones que volen assistir a aquests esdeveniments!

### Requisits del Projecte

1. **Classes a estudiar:**:

    - `Estudiant`: Una classe que representa un estudiant amb uns estudis que està cursant.

    - `Treballador`: Una classe que descriu un treballador o treballadora amb una ocupació.

    - `Jubilat`: Aquesta és la classe dels savis  que ara gaudeixen d'una tranquil·litat jubilatòria plena de facilitats.

    - `Esdeveniment`: Una classe que detalla els esdeveniments  amb propietats com nom, data, preu i ubicació. Serà el punt de partida per a la nostra aventura. A més a més podrà mostrar de manera tabular les persones inscrites a cada esdeveniment.

2. **Descomptes èpics:** *Like a Boss !!!* Desenvolupa un sistema de descomptes espectacular!

    - Estudiants obtenen un descompte del **20%** (no poden usar la brúixola per a calcular-ho).
    - Treballadors **no tenen descompte**..
    - Jubilats gaudeixen d'un increïble **50%** de descompte (l'edat té els seus avantatges!).

3. **Aplicació de descomptes:** Assegura't de que els descomptes s'apliquin correctament quan les persones es registren per a un esdeveniment.

4. **Navegant pels esdeveniments:** Crea una aplicació que permeti als usuaris:

    - Veure una llista d'esdeveniments disponibles per a l'inscripció.
    - Inscriure's a esdeveniments, introduint les seves dades d'identitat i la seva classe (estudiant, treballador o jubilat).
    - Mostrar les persones inscrites a cada esdeveniment de manera tabular.
    - Sortir de l¡aplicació quan ho desitgin.

## Diagrama de Classes
![diagrama de classes](/resources/images/04-POO_diagrama_classes_projecte.png)