class GestorEsdeveniments:
    """Classe que gestiona els esdeveniments còsmics."""

    def __init__(self):
        """Inicialitza el gestor d'esdeveniments amb una llista buida d'esdeveniments."""
        self.esdeveniments = []

    def crear_esdeveniment(self, esdeveniment):
        """Crea un esdeveniment i l'afegeix a la llista d'esdeveniments."""
        self.esdeveniments.append(esdeveniment)

    def inscriure_persona(self, esdeveniment_idx, persona):
        """Inscriu una persona a un esdeveniment específic."""
        if 0 <= esdeveniment_idx < len(self.esdeveniments):
            esdeveniment = self.esdeveniments[esdeveniment_idx]
            esdeveniment.inscriure_persona(persona)
            descompte = esdeveniment.aplicar_descompte(persona)

            preu_final = esdeveniment.preu - (esdeveniment.preu * descompte)
            print(f"Inscripció exitosa a {esdeveniment.nom}. Preu amb descompte: {preu_final} euros.")
        else:
            print("Índex d'esdeveniment invàlid.")

    def mostrar_esdeveniments(self):
        """Mostra la llista d'esdeveniments disponibles."""
        for idx, esdeveniment in enumerate(self.esdeveniments):
            print(f"{idx}. {esdeveniment}")
