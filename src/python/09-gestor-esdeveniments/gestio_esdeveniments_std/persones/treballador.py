from gestio_esdeveniments_std.persones.persona import Persona


class Treballador(Persona):
    """Classe que descriu un treballador del Futur."""

    def __init__(self, nom, edat, email, ocupacio):
        """Inicialitza un treballador amb una ocupació emocionant."""
        super().__init__(nom, edat, email)
        self.ocupacio = ocupacio
