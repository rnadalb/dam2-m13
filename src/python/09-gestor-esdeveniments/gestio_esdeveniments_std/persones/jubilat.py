from gestio_esdeveniments_std.persones.persona import Persona


class Jubilat(Persona):
    """Classe que representa una persona jubilada."""

    def __init__(self, nom, edat, email):
        """Inicialitza una persona jubilada."""
        super().__init__(nom, edat, email)
