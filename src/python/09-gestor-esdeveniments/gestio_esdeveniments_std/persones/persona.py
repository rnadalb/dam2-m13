class Persona:
    """Classe base que representa una persona."""

    def __init__(self, nom, edat, email):
        """Inicialitza una persona amb nom, edat i email."""
        self.nom = nom
        self.edat = edat
        self.email = email

    def __str__(self):
        """ Retorna la informació de la persona."""
        return f"Nom: {self.nom}\nEdat: ({self.edat}|)\nEmail: {self.email}"
