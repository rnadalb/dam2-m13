from gestio_esdeveniments_std.persones.persona import Persona


class Estudiant(Persona):
    """Classe que representa un estudiant"""

    def __init__(self, nom, edat, email, estudis):
        """Inicialitza un estudiant amb una classe específica."""
        super().__init__(nom, edat, email)
        self.estudis = estudis
