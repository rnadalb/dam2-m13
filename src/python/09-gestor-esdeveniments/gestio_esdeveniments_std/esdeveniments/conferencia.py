from gestio_esdeveniments_std.esdeveniments.esdeveniment import Esdeveniment
from gestio_esdeveniments_std.persones.estudiant import Estudiant
from gestio_esdeveniments_std.persones.jubilat import Jubilat


class Conferencia(Esdeveniment):
    """Classe que representa una conferència d'alt coneixement."""

    def __init__(self, nom, data, preu, ubicacio, orador):
        """Inicialitza una conferència amb un orador destacat."""
        super().__init__(nom, data, preu, ubicacio)
        self.orador = orador

    def aplicar_descompte(self, persona):
        """Aplica el descompte segons la classe de la persona per a les conferències."""
        if isinstance(persona, Estudiant):
            return 0.2
        elif isinstance(persona, Jubilat):
            return 0.5
        else:
            return 0.0

    def __str__(self):
        return super().__str__() + f" : {self.orador}"
