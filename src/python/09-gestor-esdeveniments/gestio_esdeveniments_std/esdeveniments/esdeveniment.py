class Esdeveniment:
    """Classe que representa un esdeveniment."""

    def __init__(self, nom, data, preu, ubicacio):
        """Inicialitza un esdeveniment amb nom, data i ubicació."""
        self.nom = nom
        self.data = data
        self.ubicacio = ubicacio
        self.preu = preu
        self.participants = []

    def mostrar_info(self):
        """Mostra la informació de l'esdeveniment."""
        print(f"Nom: {self.nom}")
        print(f"Data: {self.data}")
        print(f"Preu: {self.preu}")
        print(f"Ubicació: {self.ubicacio}")

    def inscriure_persona(self, persona):
        """Inscriu una persona a l'esdeveniment."""
        self.participants.append(persona)

    def aplicar_descompte(self, persona):
        """Aplica el descompte segons la classe de la persona."""
        return 0.0

    def mostrar_participants(self):
        """Mostra els participants d'un esdeveniment de manera tabulada."""
        print(f"Participants a {self.nom}:")
        print("{:<20} {:<10} {:<15}".format("Nom", "Edat", "Email"))
        for persona in self.participants:
            print("{:<20} {:<10} {:<15}".format(persona.nom, persona.edat, persona.email))
        print("\n")

    def __str__(self):
        return f"{self.data} --> {self.nom}: {self.preu} €"
