from gestio_esdeveniments_std.esdeveniments.esdeveniment import Esdeveniment
from gestio_esdeveniments_std.persones.estudiant import Estudiant
from gestio_esdeveniments_std.persones.jubilat import Jubilat


class Concert(Esdeveniment):
    """Classe que representa un concert."""

    def __init__(self, nom, data, preu, ubicacio, artista):
        """Inicialitza un concert amb un artista."""
        super().__init__(nom, data, preu, ubicacio)
        self.artista = artista

    def aplicar_descompte(self, persona):
        """Aplica el descompte segons la classe de la persona per als concerts."""
        if isinstance(persona, Estudiant):
            return 0.2
        elif isinstance(persona, Jubilat):
            return 0.5
        else:
            return 0.0

    def __str__(self):
        return super().__str__() + f" : {self.artista}"
