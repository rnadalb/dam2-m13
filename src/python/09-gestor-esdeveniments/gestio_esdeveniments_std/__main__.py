from gestio_esdeveniments_std.esdeveniments.concert import Concert
from gestio_esdeveniments_std.esdeveniments.conferencia import Conferencia
from gestio_esdeveniments_std.gestor.gestor_esdeveniments import GestorEsdeveniments
from gestio_esdeveniments_std.persones.estudiant import Estudiant
from gestio_esdeveniments_std.persones.jubilat import Jubilat
from gestio_esdeveniments_std.persones.treballador import Treballador


def main():
    print("Benvingut al Sistema de Gestió d'Esdeveniments!")

    gestor = GestorEsdeveniments()

    concert1 = Concert("Concert Punk", "15/10/2023", 20, "Sidecar", "SCUMBAG MILLONAIRE")
    concert2 = Concert("Concert Hip Hop", "20/11/2023", 30, "Razzmatazz", "JUANCHO MARQUÉS")
    conferencia1 = Conferencia("Software Crafters", "25/11/2023", 150, "Escola d'Arts i Disseny de Barcelona",
                               "Software Crafters")

    gestor.crear_esdeveniment(concert1)
    gestor.crear_esdeveniment(concert2)
    gestor.crear_esdeveniment(conferencia1)

    while True:
        print("Què vols fer?")
        print("1. Mostrar Esdeveniments Disponibles")
        print("2. Inscriure's a un Esdeveniment")
        print("3. Mostrar Participants d'Esdeveniment")
        print("4. Sortir")

        opcio = input("Selecciona una opció: ")

        if opcio == "1":
            gestor.mostrar_esdeveniments()
        elif opcio == "2":
            gestor.mostrar_esdeveniments()
            esdeveniment_idx = int(input("Selecciona un esdeveniment pel seu índex: "))

            if 0 <= esdeveniment_idx < len(gestor.esdeveniments):
                nom = input("Introdueix el teu nom: ")
                edat = int(input("Introdueix la teva edat: "))
                email = input("Introdueix el teu correu electrònic: ")
                classe = input("Introdueix si ets estudiant o treballador: ")

                if classe.lower() == "estudiant":
                    estudis = input("Ets estudiant, pots indicar-nos que estudies? ")
                    persona = Estudiant(nom, edat, email, estudis)
                elif classe.lower() == "treballador":
                    ocupacio = input("Ets treballador/a, pots indicar-nos la teva ocupació? ")
                    persona = Treballador(nom, edat, email, ocupacio)
                else:
                    persona = Jubilat(nom, edat, email)

                gestor.inscriure_persona(esdeveniment_idx, persona)
            else:
                print("Índex d'esdeveniment invàlid.")
        elif opcio == "3":
            for esdeveniment in gestor.esdeveniments:
                esdeveniment.mostrar_participants()
        elif opcio == "4":
            print("Gràcies per utilitzar el Sistema de Gestió d'Esdeveniments.")
            break
        else:
            print("Opció invàlida. Si us plau, selecciona una opció vàlida.")


if __name__ == "__main__":
    main()
