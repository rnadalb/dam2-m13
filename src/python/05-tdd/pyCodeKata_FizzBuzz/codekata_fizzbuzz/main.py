from codekata_fizzbuzz.fizz_buzz import fizz_buzz


def run_main():
    for i in range(51):
        print(f'El valor FizzBuzz per {i} és {fizz_buzz(i)}')


if __name__ == '__main__':
    run_main()
