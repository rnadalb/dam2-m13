def es_multiple(valor, mod):
    return (valor % mod) == 0


def fizz_buzz(valor):
    if es_multiple(valor, 3):
        if es_multiple(valor, 5):
            return "FizzBuzz"
        return "Fizz"

    if es_multiple(valor, 5):
        return "Buzz"

    return str(valor)
