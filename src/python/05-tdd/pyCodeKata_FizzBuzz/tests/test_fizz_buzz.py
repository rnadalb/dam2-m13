import unittest

from codekata_fizzbuzz.fizz_buzz import fizz_buzz


class TestFizzBuzz(unittest.TestCase):

    def run_test(self, valor_passat: int, valor_esperat: str) -> bool:
        valor_retornat = fizz_buzz(valor_passat)
        self.assertEqual(valor_retornat, valor_esperat)

    def test_fizzbuzz_retorna_1_si_passo_1(self):
        self.run_test(1, "1")

    def test_fizzbuzz_retorna_2_si_passo_2(self):
        self.run_test(2, "2")

    def test_fizzbuzz_retorna_fizz_si_passo_3(self):
        self.run_test(3, "Fizz")

    def test_fizzbuzz_retorna_buzz_si_passo_5(self):
        self.run_test(5, "Buzz")

    def test_fizzbuzz_retorna_fizz_si_passo_6_o_multiple_de_3(self):
        self.run_test(6, "Fizz")

    def test_fizzbuzz_retorna_buzz_si_passo_10_o_multiple_de_5(self):
        self.run_test(10, "Buzz")

    def test_fizzbuzz_retorna_fizzbuzz_si_passo_15_o_multiple_de_3_i_de_5(self):
        self.run_test(15, "FizzBuzz")
