# Mini-projecte 
🚀 El Món de les Inscripcions d'Esdeveniments amb contenidors 🎉


## Descripció

Recordes el mini-projecte del [món de les inscripcions](../09-gestor-esdeveniments/README.md)?

### Quin repte ens proposem?

* Utilitzar [Docker](https://www.docker.com/) per executar el nostre programari i facilitar el desplegament. Pots utilitzar de base l'exemple [10-docker-esdeveniments](../10-docker-esdeveniments/) que vas desenvolupar anteriorment.
* Per al desplegament crea dos contenidors ([orquestració](https://docs.docker.com/compose/)):
    * **esdeveniments_db**: oferirà el servei de base de dades [postgres](https://hub.docker.com/_/postgres).
        * Al moment d'engegar-se crearà la base de dades, les taules i inserirà valors per defecte. Pots fer-ho amb scripts SQL.
    * **esdeveniments_app**: contindrà l'aplicació que desenvolupes. Els dos contenidors establiran una xarxa anomenada, per exemple, ***esdeveniments_net*** que facilitarà la seva connnexió.


### Sobre docker ...
Aquí trobaràs el [document](/resources/DAM2_M13_Te15_introduccio_docker.pdf) que hem treballat a classe.