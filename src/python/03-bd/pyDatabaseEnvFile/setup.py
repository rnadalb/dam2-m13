from setuptools import setup, find_packages

setup(
    name="app_with_env",
    version="1.0",
    packages=find_packages(),
    install_requires=[
        'psycopg2',
        'python-dotenv',
        'tabulate'
    ],
    entry_points={
        'console_scripts': [
            'my_app = app_with_env.app:main',  # app_with_env és el nom del paquet, app.py conté la funció main
        ],
    },
    author="Rubén Nadal",
    description="App demo per provar la gestió de connexions a bases de dades amb fitxers d'entorn",
    long_description=open('README.md').read(),
    long_description_content_type='text/markdown',
)