# Aplicació de consulta a base de dades PostgreSQL (demo)

## Descripció

Aquesta aplicació està desenvolupada en Python i permet executar consultes a una base de dades PostgreSQL, mostrant els
resultats en format tabular. Utilitza seguretat per gestionar les credencials de la base de dades, i pots instal·lar-la
o generar un executable independent per facilitar-ne l'ús.

## Requisits previs

Abans d'executar l'aplicació, assegura't de tenir instal·lats els següents elements:

- Python 3.x
- PostgreSQL
- `pip` (el gestor de paquets de Python)
- Les següents llibreries Python:
    - `psycopg2`
    - `python-dotenv`
    - `tabulate`

## Instal·lació

### 1. Clona el repositori i importa el projecte:

```shell
git clone git@gitlab.com:rnadalb/dam2-m13.git
cd dam2-m13/src/python/03-bd/pyDatabaseEnvFile
```

### 2. Crea i activa un entorn virtual (o importa el projecte a un IDE):

```shell
python3 -m venv venv
source venv/bin/activate  # En Linux/Mac
```

o a Windows:

```shell
venv\Scripts\activate  # En Windows
```

### 3. Instal·la les dependències:

Executa la comanda següent per instal·lar totes les dependències necessàries:

```bash
pip install -r requirements.txt
```

### 4. Configura les credencials de la base de dades:

Crea un fitxer `.env` a la carpeta arrel del projecte i defineix les següents variables d’entorn amb les credencials de
la teva base de dades:
Per seguretat, no s'ha pujat el fitxer `.env` al repositori. Aquest és un exemple de com ha de ser el seu contingut:

```text
DB_HOST=localhost
DB_NAME=el_nom_de_la_base_de_dades
DB_USER=el_usuari
DB_PASSWORD=la_contrasenya
DB_PORT=5432
```

L’aplicació farà una consulta a la taula tbl_usuari de la teva base de dades PostgreSQL i mostrarà els resultats
ordenats per id_usuari en format tabular.

#### Creació del paquet Python

Si vols empaquetar l’aplicació per poder-la distribuir i instal·lar amb pip, pots utilitzar setuptools. Utilitza el
següent comandament per crear el paquet:

```shell
make build_package
```

Els fitxers de paquet es generaran a la carpeta `dist/`.

#### Creació d’un executable independent

Si vols crear un executable que es pugui distribuir sense necessitat d’instal·lar Python, pots fer servir PyInstaller.
Executa la següent comanda:

```shell
make build_executable
```

Això generarà un fitxer executable dins de la carpeta `dist/`, que podràs compartir amb altres usuaris.

#### Neteja

Pots eliminar els fitxers generats amb la següent comanda:

```shell
make clean
```

Llicència

Aquest projecte està sota la llicència MIT. Consulta el fitxer LICENSE per a més informació.
