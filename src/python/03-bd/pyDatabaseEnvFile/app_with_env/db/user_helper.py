# Classe que gestiona operacions amb la taula tbl_usuari
from typing import Any

from app_with_env.db.database_connection import DatabaseConnection


class UserHelper:
    def __init__(self, db_connection: DatabaseConnection):
        self.db_connection = db_connection

    def get_all_users(self):
        query = "SELECT * FROM tbl_usuari ORDER BY id_usuari DESC;"
        return self.db_connection.execute_query(query)

    def insert_user(self, nom: str, cognom: str, edat: int):
        query = f"INSERT INTO tbl_usuari (nom, cognom, edat) VALUES ('{nom}', '{cognom}', {edat});"
        return self.db_connection.execute_query(query)

    def update_user(self, id_usuari: int, nom: str, cognom: str, edat: int):
        query = f"UPDATE tbl_usuari SET nom = '{nom}', cognom = '{cognom}', edat = {edat} WHERE id_usuari = {id_usuari};"
        return self.db_connection.execute_query(query)