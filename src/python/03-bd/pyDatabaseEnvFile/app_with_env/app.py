import logging

from app_with_env.db.database_config import DatabaseConfig
from app_with_env.db.database_connection import DatabaseConnection
from app_with_env.db.user_helper import UserHelper
from tabulate import tabulate


def main():
    config = DatabaseConfig()
    # Utilitza el context per gestionar automàticament la connexió
    with DatabaseConnection(config) as db_connection:
        try:
            helper = UserHelper(db_connection)  # Passem l'objecte DatabaseConnection
            users, columns = helper.get_all_users()
            print(tabulate(users, headers=columns, tablefmt='psql'))
        except Exception as error:
            print(f"S'ha produït un error durant l'execució: {error}")

if __name__ == '__main__':
    main()