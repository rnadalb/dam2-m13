from app_xorra.bd.DatabaseHelper import DatabaseHelper


def main():
    with DatabaseHelper() as helper:
        helper.execute("SELECT "
                       "  e.*, "
                       "  t.nom "
                       "FROM "
                       "  tbl_esdeveniments as e, "
                       "  tbl_tipus_esdeveniment as t "
                       "WHERE "
                       "  e.id_tipus_esdeveniment = t.id "
                       "ORDER BY "
                       "  id_tipus_esdeveniment;")

        for esdeveniment in helper.fetchall():
            print(esdeveniment)

        input("lock")


if __name__ == "__main__":
    main()
