import os
import sys

import psycopg2
from psycopg2 import OperationalError

from app_xorra.bd.DatabaseException import DBErrorCode, DatabaseException


class DatabaseHelper:
    def __init__(self):
        try:
            # Obtenim les dades de connexió des de les variables d'entorn del SO
            self._host = os.environ['POSTGRES_HOST']
            self._port = os.environ['POSTGRES_PORT']
            self._user = os.environ['POSTGRES_USER']
            self._dbname = os.environ['POSTGRES_DBNAME']
            self._password = os.environ['POSTGRES_PASSWORD']
            self._sslmode = os.environ['POSTGRES_SSLMODE']  # disable, allow, prefer, require, verify-ca, verify-full

            # Construeix la connection string
            conn_string = f"host={self._host} port={self._port} user={self._user} dbname={self._dbname} password={self._password} sslmode={self._sslmode}"

            self._conn = psycopg2.connect(conn_string)
            self._cursor = self._conn.cursor()
        except OperationalError:
            raise DatabaseException(sys.exc_info(), code=DBErrorCode.NOT_CONNECTED,
                                    message="Error. No es possible connectar a la base de dades")

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.close()

    @property
    def connection(self):
        return self._conn

    @property
    def cursor(self):
        return self._cursor

    def commit(self):
        self.connection.commit()

    def close(self, commit: bool = True):
        if commit:
            self.commit()
        self.connection.close()

    def execute(self, sql: str, params=None):
        self.cursor.execute(sql, params or ())

    def fetchall(self):
        return self.cursor.fetchall()

    def fetchone(self):
        return self.cursor.fetchone()
