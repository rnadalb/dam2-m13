import enum


class DBErrorCode(enum.Enum):
    NOT_CONNECTED = 100


class DatabaseException(Exception):
    def __init__(self, exc_info, code=DBErrorCode.NOT_CONNECTED, message='ERROR'):
        self._code = code
        self._message = message
        if exc_info is not None:
            print(f'{self.__class__}: {exc_info[1]}')
        else:
            print.info(f'{self.__class__}: [{self._code}] - {self._message}')
