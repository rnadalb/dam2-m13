from pydantic import BaseModel


class Llibre(BaseModel):
    titol: str
    autor: str
    any_publicacio: int