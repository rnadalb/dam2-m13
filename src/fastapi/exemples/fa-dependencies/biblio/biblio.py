import uuid

from fastapi import FastAPI, Depends, HTTPException, Security
from fastapi.security import HTTPBearer, HTTPAuthorizationCredentials
from starlette import status

from biblio.classes.llibre import Llibre
from biblio.classes.prestec import Prestec

_BOOK_NOT_FOUND = "Llibre no trobat"

app = FastAPI()

fake_db = {
    "llibres": {},
    "prestecs": {}
}

security = HTTPBearer()


def get_current_user(credentials: HTTPAuthorizationCredentials = Security(security)):
    token = credentials.credentials
    if token != "token_secret_valid":
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Token invàlid o absent"
        )
    # Aquí es podria incloure una lògica més complexa per a validar el token
    # Per simplificar, retornem un usuari de test
    return {"usuari_id": "123", "nom": "Usuari Test"}


# Ruta per obtenir informació del perfil de l'usuari
@app.get("/usuari/perfil")
def perfil_usuari(current_user: dict = Depends(get_current_user)):
    return {"nom": current_user["nom"], "usuari_id": current_user["usuari_id"]}


# Ruta per gestionar préstecs de llibres
@app.post("/prestec/")
def realitzar_prestec(llibre_id: int, current_user: dict = Depends(get_current_user)):
    # Lògica per a realitzar un préstec
    return {"usuari_id": current_user["usuari_id"], "llibre_id": llibre_id, "estat": "Prestat"}


# CRUD per a Llibres
@app.get("/llibres/")
def llibres_disponibles(current_user: dict = Depends(get_current_user)):
    # Lògica per a retornar llibres disponibles
    return {"llibres": ["Llibre 1", "Llibre 2", "Llibre 3"]}


@app.get("/llibres/{llibre_id}")
def llegir_llibre(llibre_id: str, current_user: dict = Depends(get_current_user)):
    if llibre_id not in fake_db["llibres"]:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail=_BOOK_NOT_FOUND)
    return fake_db["llibres"][llibre_id]


@app.post("/llibres/", status_code=status.HTTP_201_CREATED)
def crear_llibre(llibre: Llibre, current_user: dict = Depends(get_current_user)):
    llibre_id = str(uuid.uuid4())
    fake_db["llibres"][llibre_id] = llibre.model_dump()
    return {"llibre_id": llibre_id, **llibre.model_dump()}


@app.put("/llibres/{llibre_id}")
def actualitzar_llibre(llibre_id: str, llibre: Llibre, current_user: dict = Depends(get_current_user)):
    if llibre_id not in fake_db["llibres"]:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail=_BOOK_NOT_FOUND)
    fake_db["llibres"][llibre_id] = llibre.model_dump()
    return fake_db["llibres"][llibre_id]


@app.delete("/llibres/{llibre_id}", status_code=status.HTTP_204_NO_CONTENT)
def eliminar_llibre(llibre_id: str, current_user: dict = Depends(get_current_user)):
    if llibre_id not in fake_db["llibres"]:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail=_BOOK_NOT_FOUND)
    del fake_db["llibres"][llibre_id]
    return {"detall": "Llibre eliminat"}


# CRUD per a Préstecs
@app.post("/prestec/", status_code=status.HTTP_201_CREATED)
def realitzar_prestec(prestec: Prestec, current_user: dict = Depends(get_current_user)):
    prestec_id = str(uuid.uuid4())
    fake_db["prestecs"][prestec_id] = prestec.model_dump()
    return {"prestec_id": prestec_id, **prestec.model_dump()}


@app.get("/prestec/{prestec_id}")
def llegir_prestec(prestec_id: str, current_user: dict = Depends(get_current_user)):
    if prestec_id not in fake_db["prestecs"]:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail=_BOOK_NOT_FOUND)
    return fake_db["prestecs"][prestec_id]


@app.delete("/prestec/{prestec_id}", status_code=status.HTTP_204_NO_CONTENT)
def finalitzar_prestec(prestec_id: str, current_user: dict = Depends(get_current_user)):
    if prestec_id not in fake_db["prestecs"]:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail=_BOOK_NOT_FOUND)
    del fake_db["prestecs"][prestec_id]
    return {"detall": "Préstec finalitzat"}
