from datetime import datetime
from typing import List

from fastapi import FastAPI, HTTPException
from pydantic import BaseModel

_NOT_FOUND = "Persona not found"

app = FastAPI()


# Model de dades amb Pydantic
class Persona(BaseModel):
    id: int
    nom: str
    email: str
    edat: int
    data_alta: datetime
    esAdmin: bool = False


# Base de dades fictícia en memòria
db: List[Persona] = []


# CRUD Operations
# Create
@app.post("/persones/", response_model=Persona)
def create_persona(persona: Persona):
    # Comprovar si l'email ja existeix
    if any(p.email == persona.email for p in db):
        raise HTTPException(status_code=400, detail="Email already registered")
    db.append(persona)
    return persona


# Read
@app.get("/persones/", response_model=List[Persona])
def read_persones():
    return db


@app.get("/persones/{persona_id}", response_model=Persona)
def read_persona(persona_id: int):
    persona = next((p for p in db if p.id == persona_id), None)
    if persona is None:
        raise HTTPException(status_code=404, detail=_NOT_FOUND)
    return persona


# Update
@app.put("/persones/{persona_id}", response_model=Persona)
def update_persona(persona_id: int, updated_persona: Persona):
    for index, persona in enumerate(db):
        if persona.id == persona_id:
            db[index] = updated_persona
            return updated_persona
    raise HTTPException(status_code=404, detail=_NOT_FOUND)


# Delete
@app.delete("/persones/{persona_id}", response_model=Persona)
def delete_persona(persona_id: int):
    for index, persona in enumerate(db):
        if persona.id == persona_id:
            removed_persona = db.pop(index)
            return removed_persona
    raise HTTPException(status_code=404, detail=_NOT_FOUND)
