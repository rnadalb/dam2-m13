import os
import urllib
from datetime import datetime

import sqlalchemy
from sqlalchemy import create_engine, Column, Integer, String, Float, Boolean, DateTime, Date
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker

_SPECIAL_CHARACTERS = ('@', '#', '$', '%', '&', '.')


def parse_password(password: str) -> str:
    if any(c in password for c in _SPECIAL_CHARACTERS):
        return urllib.parse.quote_plus(password)
    return password


# dialect+driver://username:password@host:port/database
SQLALCHEMY_DATABASE_URL = f"postgresql+psycopg2://{os.environ.get('POSTGRES_USER')}:{parse_password(os.environ.get('POSTGRES_PASSWORD'))}@{os.environ.get('POSTGRES_HOST')}:{os.environ.get('POSTGRES_PORT')}/{os.environ.get('POSTGRES_DBNAME')}"

engine = create_engine(SQLALCHEMY_DATABASE_URL)
SessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=engine)

Base = declarative_base()


class Persona(Base):
    __tablename__ = "tbl_persona_demo"

    codi = Column(Integer, primary_key=True, index=True)
    nom = Column(String, index=True)
    edat = Column(Integer)
    data_naixement = Column(Date)
    es_admin = Column(Boolean, default=False)
    sou = Column(Float)


# Crea les taules en la base de dades
Base.metadata.create_all(bind=engine)
