from typing import List

from fastapi import FastAPI, Depends, HTTPException
from sqlalchemy.orm import Session
from starlette import status

from orm_app.classes.models import PersonaResponse, PersonaCreate
from orm_app.classes.persona import Persona, SessionLocal

_PERSON_NOT_FOUND = "Persona no trobada"

app = FastAPI()


def get_db():
    db = SessionLocal()
    try:
        yield db
    finally:
        db.close()


@app.post("/persones/", response_model=PersonaResponse, status_code=status.HTTP_201_CREATED)
def crear_persona(persona: PersonaCreate, db: Session = Depends(get_db)):
    db_persona = Persona(**persona.model_dump())
    db.add(db_persona)
    db.commit()
    db.refresh(db_persona)
    return db_persona


@app.get("/persones/{codi}", response_model=PersonaResponse, status_code=status.HTTP_200_OK)
def llegir_persona(codi: int, db: Session = Depends(get_db)):
    db_persona = db.query(Persona).filter(Persona.codi == codi).first()
    if db_persona is None:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail=_PERSON_NOT_FOUND)
    return db_persona


@app.get("/persones/", response_model=List[PersonaResponse], status_code=status.HTTP_200_OK)
def llegir_persones(db: Session = Depends(get_db)):
    persones = db.query(Persona).all()
    if persones is None:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail=_PERSON_NOT_FOUND)
    return persones


@app.put("/persones/{codi}", response_model=PersonaResponse, status_code=status.HTTP_201_CREATED)
def actualitzar_persona(codi: int, persona: PersonaCreate, db: Session = Depends(get_db)):
    db_persona = db.query(Persona).filter(Persona.codi == codi).first()
    if db_persona is None:
        raise HTTPException(status_code=404, detail=_PERSON_NOT_FOUND)
    for key, value in persona.model_dump().items():
        setattr(db_persona, key, value)
    db.commit()
    db.refresh(db_persona)
    return db_persona


@app.delete("/persones/{codi}", status_code=status.HTTP_204_NO_CONTENT)
def eliminar_persona(codi: int, db: Session = Depends(get_db)):
    db_persona = db.query(Persona).filter(Persona.codi == codi).first()
    if db_persona is None:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail=_PERSON_NOT_FOUND)
    db.delete(db_persona)
    db.commit()
    return {"detall": "Persona eliminada"}
