# Estructura d'una aplicació complexa

---
L'aplicació consta de quatre paquets que ofereixen funcionalitat relacionada amb els serveis: **enrutadors**, **serveis**, **esquemes** i **models**. Per introduir un nou servei, és necessari afegir un nou mòdul dins de cadascun d'aquests paquets. L'estructura proposada està dissenyada de manera semblant al patró d'arquitectura de tres nivells.

```text
── app
│   ├── backend               # Funcionalitat i configuració del backend 
│   │   ├── config.py           # Configuració de l'app
│   │   ├── database.py         # Gestió de la BBDD (sessions, connexió, ...)
│   ├── models                # Models de l'SQLAlchemy 
│   │   ├── persona.py          # Model Persona
│   │   └── tipus_persona.py    # Model TipusPersona
│   ├── routers               # Rutes/Paths de l'API
│   │   ├── persona.py          # Paths persona
│   │   └── tipus_persona.py    # Paths tipus_persona
│   ├── schemas               # Models Pydantic
│   │   ├── persona.py          # Tip: acabem amb Schema per identifdicar-los millor
│   │   └── tipus_persona.py
│   ├── services               # Lògica de negoci
│   │   ├── persona.py           # CRUD persona 
│   │   └── tipus_persona.py     # CRUD tipus_persona
│   ├── main.py                # Aplicació principal (runner)
│   └── version.py             # Versió de l'applicació
├── requirements.txt           # Dependències (paquets)
```

En aquesta estructura, el paquet **routers** serveix com a capa d'interacció de la interfície d'usuari (UI).

El paquet de **models** proporciona mapeigs SQLAlchemy que estableixen la relació entre objectes de la base de dades i classes Python, mentre que el paquet **schemas** representa models de dades serialitzats (models Pydantic) que s'utilitzen en tota l'aplicació i com a objectes de resposta.

El paquet de **backend** proporciona un gestor de sessions de base de dades i una classe de configuració de l'aplicació. En escenaris on l'aplicació interactua no només amb una base de dades sinó també amb altres backends, com ara APIs addicionals, els clients respectius es poden col·locar dins del paquet de backend.

El paquet **services** proporciona les diferents accions i/o funcionalitats que seran necessàries perquè la capa d'interacció (**routers**) funcioni correctament.  