from fastapi import APIRouter, Depends, HTTPException
from sqlalchemy.orm import Session
from starlette import status

from app.backend import database
from app.schemas.tipus_persona import TipusPersonaSchema, TipusPersonaCreateSchema
from app.services import tipus_persona as crud

router = APIRouter(prefix="/tipus_persones", tags=["tipus_persones"])


def get_db():
    db = database.SessionLocal()
    try:
        yield db
    finally:
        db.close()


# Rutes per a la taula TipusPersona
# Rutes per a la taula TipusPersona
@router.post("/", response_model=TipusPersonaSchema, status_code=status.HTTP_201_CREATED)
def create_tipus_persona(tipus_persona: TipusPersonaCreateSchema, db: Session = Depends(get_db)):
    return crud.create_tipus_persona(db=db, tipus_persona=tipus_persona)


@router.get("/{tipus_persona_id}", response_model=TipusPersonaSchema)
def read_tipus_persona(tipus_persona_id: int, db: Session = Depends(get_db)):
    db_tipus_persona = crud.get_tipus_persona(db, tipus_persona_id=tipus_persona_id)
    if db_tipus_persona is None:
        raise HTTPException(status_code=404, detail="Tipus de persona no trobat")
    return db_tipus_persona


@router.get("/", response_model=list[TipusPersonaSchema])
def read_tipus_persones(skip: int = 0, limit: int = 100, db: Session = Depends(get_db)):
    tipus_persones = crud.get_tipus_persones(db, skip=skip, limit=limit)
    return tipus_persones


@router.delete("/{tipus_persona_id}", status_code=status.HTTP_204_NO_CONTENT)
def delete_tipus_persona(tipus_persona_id: int, db: Session = Depends(get_db)):
    crud.delete_tipus_persona(db=db, tipus_persona_id=tipus_persona_id)
    return {"detail": "Tipus de persona eliminat"}
