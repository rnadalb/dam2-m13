from fastapi import Depends, APIRouter, HTTPException
from sqlalchemy.orm import Session
from starlette import status

from app.backend import database
from app.schemas.persona import PersonaSchema, PersonaCreateSchema
from app.services import persona as crud

router = APIRouter(prefix="/persones", tags=["persones"])


def get_db():
    db = database.SessionLocal()
    try:
        yield db
    finally:
        db.close()


# Rutes per a la taula Persona
@router.post("/",
             response_model=PersonaSchema,
             status_code=status.HTTP_201_CREATED)
def create_persona(persona: PersonaCreateSchema, db: Session = Depends(get_db)):
    return crud.create_persona(db=db, persona=persona)


@router.get("/{persona_id}", response_model=PersonaSchema)
def read_persona(persona_id: int, db: Session = Depends(get_db)):
    db_persona = crud.get_persona(db, persona_id=persona_id)
    if db_persona is None:
        raise HTTPException(status_code=404, detail="Persona no trobada")
    return db_persona


@router.get("/", response_model=list[PersonaSchema])
def read_persones(skip: int = 0, limit: int = 100, db: Session = Depends(get_db)):
    persones = crud.get_persones(db, skip=skip, limit=limit)
    return persones


@router.put("/{persona_id}", response_model=PersonaSchema)
def update_persona(persona_id: int, persona: PersonaCreateSchema, db: Session = Depends(get_db)):
    return crud.update_persona(db=db, persona_id=persona_id, persona=persona)


@router.delete("/{persona_id}", status_code=status.HTTP_204_NO_CONTENT)
def delete_persona(persona_id: int, db: Session = Depends(get_db)):
    crud.delete_persona(db=db, persona_id=persona_id)
    return {"detail": "Persona eliminada"}
