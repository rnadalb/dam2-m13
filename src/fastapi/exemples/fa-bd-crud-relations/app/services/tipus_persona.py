from sqlalchemy.orm import Session

from app.models.tipus_persona import TipusPersona
from app.schemas.tipus_persona import TipusPersonaCreateSchema, TipusPersonaSchema


# Operacions CRUD per a TipusPersona
def get_tipus_persona(db: Session, tipus_persona_id: int):
    return db.query(TipusPersona).filter(TipusPersona.id == tipus_persona_id).first()


def get_tipus_persones(db: Session, skip: int = 0, limit: int = 100):
    return db.query(TipusPersona).offset(skip).limit(limit).all()


def create_tipus_persona(db: Session, tipus_persona: TipusPersonaCreateSchema):
    db_tipus_persona = TipusPersona(**tipus_persona.model_dump())
    db.add(db_tipus_persona)
    db.commit()
    db.refresh(db_tipus_persona)
    return db_tipus_persona


def delete_tipus_persona(db: Session, tipus_persona_id: int):
    db_tipus_persona = get_tipus_persona(db, tipus_persona_id=tipus_persona_id)
    if db_tipus_persona:
        db.delete(db_tipus_persona)
        db.commit()
    return db_tipus_persona


def update_tipus_persona(db: Session, tipus_persona_id: int, tipus_persona: TipusPersonaCreateSchema):
    db_tipus_persona = get_tipus_persona(db, tipus_persona_id=tipus_persona_id)
    if db_tipus_persona:
        for key, value in tipus_persona.model_dump().items():
            setattr(db_tipus_persona, key, value)
        db.commit()
        db.refresh(db_tipus_persona)
    return db_tipus_persona
