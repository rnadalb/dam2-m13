# Operacions CRUD per a Persona
from sqlalchemy.orm import Session

from app.models.persona import Persona
from app.schemas.persona import PersonaSchema, PersonaCreateSchema


def get_persona(db: Session, persona_id: int):
    return db.query(Persona).filter(Persona.id == persona_id).first()


def get_persones(db: Session, skip: int = 0, limit: int = 100):
    return db.query(Persona).offset(skip).limit(limit).all()


def create_persona(db: Session, persona: PersonaCreateSchema):
    db_persona = Persona(**persona.model_dump())
    db.add(db_persona)
    db.commit()
    db.refresh(db_persona)
    return db_persona


def update_persona(db: Session, persona_id: int, persona: PersonaCreateSchema):
    db_persona = get_persona(db, persona_id=persona_id)
    if db_persona:
        for key, value in persona.model_dump().items():
            setattr(db_persona, key, value)
        db.commit()
        db.refresh(db_persona)
    return db_persona


def delete_persona(db: Session, persona_id: int):
    db_persona = get_persona(db, persona_id=persona_id)
    if db_persona:
        db.delete(db_persona)
        db.commit()
    return db_persona
