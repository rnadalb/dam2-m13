from fastapi import FastAPI

from app.routers.persona import router as persona_router
from app.routers.tipus_persona import router as tipus_persona_router
from app.version import __version__

app = FastAPI(
    title="Relational App",
    description="An app with a relational database",
    version=__version__,
    swagger_ui_parameters={"defaultModelsExpandDepth": -1},
)

app.include_router(persona_router)
app.include_router(tipus_persona_router)
