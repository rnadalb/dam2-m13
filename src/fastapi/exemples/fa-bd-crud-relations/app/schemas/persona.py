from typing import Optional

from pydantic import BaseModel

from app.schemas.tipus_persona import TipusPersonaSchema


# Esquemes per a Persona
class PersonaBaseSchema(BaseModel):
    email: str
    nom: Optional[str] = None
    edat: Optional[int] = None
    id_tipus_persona: int


class PersonaCreateSchema(PersonaBaseSchema):
    pass


class PersonaSchema(PersonaBaseSchema):
    id: int
    tipus_persona: Optional[TipusPersonaSchema] = None

    class Config:
        from_attributes = True


# Esquema per a la resposta que inclou relacions
class PersonaAmbTipusPersonaSchema(PersonaSchema):
    tipus_persona: TipusPersonaSchema
