from pydantic import BaseModel


# Esquemes per a TipusPersona
class TipusPersonaBaseSchema(BaseModel):
    descripcio: str


class TipusPersonaCreateSchema(TipusPersonaBaseSchema):
    pass


class TipusPersonaSchema(TipusPersonaBaseSchema):
    id: int

    class Config:
        from_attributes = True
