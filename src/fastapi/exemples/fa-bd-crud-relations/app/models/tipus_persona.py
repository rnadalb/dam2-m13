from sqlalchemy import Column, Integer, String
from sqlalchemy.orm import relationship

from app.backend.database import Base


class TipusPersona(Base):
    __tablename__ = 'tbl_tipus_persona'

    id = Column(Integer, primary_key=True, index=True)
    descripcio = Column(String)

    persones = relationship("Persona", back_populates="tipus_persona")
