from sqlalchemy import Column, Integer, String, ForeignKey
from sqlalchemy.orm import relationship

from app.backend.database import Base


class Persona(Base):
    __tablename__ = 'tbl_persona'

    id = Column(Integer, primary_key=True, index=True)
    email = Column(String, unique=True, index=True)
    nom = Column(String)
    edat = Column(Integer)
    id_tipus_persona = Column(Integer, ForeignKey('tbl_tipus_persona.id'))

    # Relació amb la taula 'tbl_tipus_persona' aka TipusPersona
    tipus_persona = relationship("TipusPersona", back_populates="persones")
