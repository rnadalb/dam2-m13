import os
import urllib

from pydantic_settings import BaseSettings


class Settings(BaseSettings):
    # PostgreSQL
    DB_USER: str = os.environ.get('POSTGRES_USER')
    DB_PASSWORD: str = urllib.parse.quote_plus(os.environ.get('POSTGRES_PASSWORD'))
    DB_HOST: str = os.environ.get('POSTGRES_HOST')
    DB_PORT: int = os.environ.get('POSTGRES_PORT')
    DB_NAME: str = os.environ.get('POSTGRES_DBNAME')
    DB_SSL_MODE: str = 'require'

    # SQL Alchemy
    SQLALCHEMY_DATABASE_URI: str = f"postgresql+psycopg2://{DB_USER}:{DB_PASSWORD}@{DB_HOST}:{DB_PORT}/{DB_NAME}?sslmode={DB_SSL_MODE}"


settings = Settings()
