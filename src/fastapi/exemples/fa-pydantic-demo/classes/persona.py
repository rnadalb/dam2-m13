from pydantic import BaseModel, field_validator

_DEFAULT_DOMAIN = '@ies-eugeni.cat'


class Persona(BaseModel):
    id: int
    nom: str
    email: str
    edat: int | None = None

    # Validador personalitzat per al correu electrònic
    @field_validator('email')
    def email_de_domini_especific(cls, email: str):
        if not email.endswith(_DEFAULT_DOMAIN):
            raise ValueError(f'Has de proporcionar una adreça de correu de {_DEFAULT_DOMAIN}')
        return email

    @field_validator("edat")
    def validate_age(cls, edat: int):
        if edat <= 0:
            raise ValueError("El valor de l'edat ha de ser un enter positiu major que 0")
        return edat
