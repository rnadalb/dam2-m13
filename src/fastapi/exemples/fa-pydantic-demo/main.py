from typing import List

from fastapi import FastAPI, HTTPException, status

from classes.docent import Docent

app = FastAPI()

# Base de dades fictícia en memòria
db_docents = []

# Constants
_DOCENT_NOT_FOUND = "Docent no trobat"


def obtenir_index_docent(docent_id: int) -> int:
    """Obté l'índex d'un docent a la llista basant-se en l'ID.

    ### Paràmetres:
    **docent_id** (int): *L'identificador (PK) del docent a buscar.*

    ### Retorna:
    **índex** (int): *L'índex del docent a la llista.*

    ### Errors:
    **HTTPException**: *Si el docent amb l'ID donat no es troba.*
    """
    for index, docent in enumerate(db_docents):
        if docent.id == docent_id:
            return index
    raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail=_DOCENT_NOT_FOUND)


# CRUD per Docent
@app.post("/docents/", response_model=Docent, status_code=status.HTTP_201_CREATED)
def crear_docent(docent: Docent):
    """
    Crea un nou docent i l'afegeix a la base de dades fictícia.

    ### Paràmetres:
    **docent** (Docent) - *Un model de docent per crear amb informació personal i el centre educatiu on treballa.*

    ### Retorna:
    **Docent**: *El model del docent creat.*

    ### Errors:
    **HTTPException**: *Si el docent nou té l'identificador igual a un existent.*
    """
    # Comprovació si l'ID del docent ja existeix
    if any(d.id == docent.id for d in db_docents):
        raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail=f"L'identificador {docent.id} del docent ja existeix")
    db_docents.append(docent)
    return docent


@app.get("/docents/", response_model=List[Docent])
def llistar_docents():
    """
    Llista tots els docents en la base de dades fictícia.

    ### Retorna:
    **List[Docent]**: *Una llista de tots els docents.*
    """
    return db_docents


@app.get("/docents/{docent_id}", response_model=Docent)
def obtenir_docent(docent_id: int):
    """
    Obté un docent específic per ID de la base de dades fictícia.

    ### Paràmetres:
    **docent_id** (int): *L'identificador (PK) del docent a obtenir.*

    ### Retorna:
    **Docent**: *El model del docent si es troba.*
    """
    index = obtenir_index_docent(docent_id)  # En cas de no trobar el docent la funció de cerca llança una excepció
    return db_docents[index]


@app.put("/docents/{docent_id}", response_model=Docent)
def actualitzar_docent(docent_id: int, updated_docent: Docent):
    """
    Actualitza un docent existent a la base de dades fictícia segons el seu ID.

    ### Paràmetres:
    **docent_id** (int): - L'identificador (PK) del docent a actualitzar.
    **updated_docent** (Docent) - Un model de docent amb la informació actualitzada.

    ### Retorna:
    **Docent**: El model del docent actualitzat.
    """
    index = obtenir_index_docent(docent_id)
    db_docents[index] = updated_docent
    return db_docents[index]


@app.delete("/docents/{docent_id}", status_code=status.HTTP_204_NO_CONTENT)
def eliminar_docent(docent_id: int):
    """
    Elimina un docent de la base de dades fictícia segons el seu ID.

    ### Paràmetres:
    **docent_id** (int) - L'identificador (PK) del docent a eliminar.

    ### Retorna:
    **HTTP_204_NO_CONTENT**: Una resposta sense contingut si l'eliminació és exitosa.

    ### Errors:
    **HTTPException**: Si el docent no es troba.
    """
    index = obtenir_index_docent(docent_id)
    if index != -1:
        del db_docents[index]
        return "Docent eliminat"

    raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail=_DOCENT_NOT_FOUND)
