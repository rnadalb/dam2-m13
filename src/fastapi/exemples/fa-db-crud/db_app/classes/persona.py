# Models Pydantic
from datetime import date

from pydantic import BaseModel


class PersonaCreate(BaseModel):
    nom: str
    edat: int
    data_naixement: date
    es_admin: bool
    sou: float


class Persona(BaseModel):
    codi: int
    nom: str
    edat: int
    data_naixement: str
    es_admin: bool
    sou: float
