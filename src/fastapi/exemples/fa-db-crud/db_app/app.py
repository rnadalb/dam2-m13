from typing import List

import psycopg2
from fastapi import FastAPI, HTTPException
from starlette import status

from db_app.classes.connection import get_db_connection
from db_app.classes.persona import Persona, PersonaCreate

_PERSON_NOT_FOUND = "Persona no trobada"

app = FastAPI()


def prepare_sql(sql: str, values=None, single_result=True, delete=False):
    conn, cursor, persona = None, None, None
    try:
        conn = get_db_connection()
        if conn is None:
            raise HTTPException(status_code=status.HTTP_500_INTERNAL_SERVER_ERROR,
                                detail="Error al connectar a la base de dades")
        cursor = conn.cursor()
        if values:
            cursor.execute(sql, values)
        else:
            cursor.execute(sql)
        if single_result:
            if not delete:
                row = cursor.fetchone()
                conn.commit()
                persona = Persona(codi=row[0], nom=row[1], edat=row[2],
                                  data_naixement=str(row[3]),
                                  sou=row[4], es_admin=row[5])
            else:
                conn.commit()
        else:
            persona = [
                {
                    "codi": data[0],
                    "nom": data[1],
                    "edat": data[2],
                    "data_naixement": str(data[3]),
                    "sou": data[4],
                    "es_admin": data[5],

                }
                for data in cursor.fetchall()
            ]

    except psycopg2.OperationalError:
        raise HTTPException(status_code=status.HTTP_500_INTERNAL_SERVER_ERROR, detail="No hi ha connexió a la BBDD")
    except psycopg2.errors.UndefinedTable:
        raise HTTPException(status_code=status.HTTP_500_INTERNAL_SERVER_ERROR, detail="La taula no existeix")
    except Exception as e:
        raise HTTPException(status_code=status.HTTP_500_INTERNAL_SERVER_ERROR, detail=str(e))
    finally:
        if cursor:
            cursor.close()
        if conn:
            conn.close()

    return persona


@app.post("/persones/", response_model=Persona, status_code=status.HTTP_201_CREATED)
async def crear_persona(persona: PersonaCreate):
    return prepare_sql(
        "INSERT INTO tbl_persona_demo (nom, edat, data_naixement, es_admin, sou) VALUES (%s, %s, %s, %s, %s) RETURNING *;",
        values=(persona.nom, persona.edat, persona.data_naixement, persona.es_admin, persona.sou),
        single_result=True)


@app.get("/persones/", response_model=List[Persona], status_code=status.HTTP_200_OK)
async def llegir_persones():
    return prepare_sql("SELECT * FROM tbl_persona_demo ORDER BY codi;", single_result=False)


@app.get("/persones/{codi}", response_model=Persona)
async def llegir_persona(codi: int):
    return prepare_sql("SELECT * FROM tbl_persona_demo WHERE codi = %s;", values=(codi,), single_result=True)


@app.delete("/persones/{codi}", status_code=status.HTTP_204_NO_CONTENT)
async def eliminar_persona(codi: int):
    return prepare_sql("DELETE FROM tbl_persona_demo WHERE codi = %s;", values=(codi,), delete=True)


@app.put("/persones/{codi}", response_model=Persona)
async def actualitzar_persona(codi: int, persona: PersonaCreate):
    return prepare_sql(
        "UPDATE tbl_persona_demo SET nom = %s, edat = %s, data_naixement = %s, es_admin = %s, sou = %s WHERE codi = %s RETURNING *;",
        values=(persona.nom, persona.edat, persona.data_naixement, persona.es_admin, persona.sou, codi),
        single_result=True
    )


# @app.post("/persones/", response_model=Persona, status_code=status.HTTP_201_CREATED)
# async def crear_persona(persona: PersonaCreate):
#     conn = get_db_connection()
#     cursor = conn.cursor()
#     cursor.execute(
#         "INSERT INTO tbl_persona_demo (nom, edat, data_naixement, es_admin, sou) VALUES (%s, %s, %s, %s, %s) RETURNING *;",
#         (persona.nom, persona.edat, persona.data_naixement, persona.es_admin, persona.sou))
#     nova_persona = cursor.fetchone()
#     conn.commit()
#     cursor.close()
#     conn.close()
#     return Persona(codi=nova_persona[0], nom=persona.nom, edat=persona.edat,
#                    data_naixement=str(persona.data_naixement),
#                    sou=persona.sou, es_admin=persona.es_admin)
#
#
# @app.get("/persones/", response_model=List[Persona], status_code=status.HTTP_200_OK)
# async def llegir_persones():
#     persones, conn, cursor = None, None, None
#     try:
#         conn = get_db_connection()
#         if conn is None:
#             raise HTTPException(status_code=status.HTTP_500_INTERNAL_SERVER_ERROR,
#                                 detail="Error al connectar a la base de dades")
#         cursor = conn.cursor()
#         cursor.execute("SELECT * FROM tblpersona_demo ORDER BY codi;")
#         persones = [
#             {
#                 "codi": data[0],
#                 "nom": data[1],
#                 "edat": data[2],
#                 "data_naixement": str(data[3]),
#                 "sou": data[4],
#                 "es_admin": data[5],
#
#             }
#             for data in cursor.fetchall()
#         ]
#         if not persones:
#             raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail="No hi ha registres a la base de dades")
#     except psycopg2.OperationalError:
#         raise HTTPException(status_code=status.HTTP_500_INTERNAL_SERVER_ERROR, detail="No hi ha connexió a la BBDD")
#     except psycopg2.errors.UndefinedTable:
#         raise HTTPException(status_code=status.HTTP_500_INTERNAL_SERVER_ERROR, detail="La taula no existeix")
#     except Exception as e:
#         raise HTTPException(status_code=status.HTTP_500_INTERNAL_SERVER_ERROR, detail=str(e))
#     finally:
#         if cursor:
#             cursor.close()
#         if conn:
#             conn.close()
#
#     return persones
#
#
# @app.get("/persones/{codi}", response_model=Persona)
# async def llegir_persona(codi: int):
#     conn = get_db_connection()
#     cursor = conn.cursor()
#     cursor.execute("SELECT * FROM tbl_persona_demo WHERE codi = %s;", (codi,))
#     persona = cursor.fetchone()
#     if not persona:
#         raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail=_PERSON_NOT_FOUND)
#     cursor.close()
#     conn.close()
#     return Persona(codi=codi, nom=persona[1], edat=persona[2],
#                    data_naixement=str(persona[3]),
#                    sou=persona[4], es_admin=persona[5])
#
#
# @app.put("/persones/{codi}", response_model=Persona)
# async def actualitzar_persona(codi: int, persona: PersonaCreate):
#     conn = get_db_connection()
#     cursor = conn.cursor()
#     cursor.execute(
#         "UPDATE tbl_persona_demo SET nom = %s, edat = %s, data_naixement = %s, es_admin = %s, sou = %s WHERE codi = %s RETURNING *;",
#         (persona.nom, persona.edat, persona.data_naixement, persona.es_admin, persona.sou, codi))
#     persona_actualitzada = cursor.fetchone()
#     if not persona_actualitzada:
#         raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail=_PERSON_NOT_FOUND)
#     conn.commit()
#     cursor.close()
#     conn.close()
#     return Persona(codi=codi, nom=persona.nom, edat=persona.edat,
#                    data_naixement=str(persona.data_naixement),
#                    sou=persona.sou, es_admin=persona.es_admin)
#
#
# @app.delete("/persones/{codi}", status_code=status.HTTP_204_NO_CONTENT)
# async def eliminar_persona(codi: int):
#     conn = get_db_connection()
#     cursor = conn.cursor()
#     cursor.execute("DELETE FROM tbl_persona_demo WHERE codi = %s RETURNING *;", (codi,))
#     persona_eliminada = cursor.fetchone()
#     if not persona_eliminada:
#         raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail=_PERSON_NOT_FOUND)
#     conn.commit()
#     cursor.close()
#     conn.close()
#     return {"detall": "Persona eliminada"}
