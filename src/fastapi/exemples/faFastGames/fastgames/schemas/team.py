from pydantic import BaseModel


class TeamBase(BaseModel):
    name: str
    country: str
    rank: int


class TeamCreate(TeamBase):
    pass


class Team(TeamBase):
    id: int

    class Config:
        from_attributes = True
