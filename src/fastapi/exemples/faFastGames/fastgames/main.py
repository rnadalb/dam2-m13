from fastapi import FastAPI

from fastgames.database import Base
from fastgames.database import engine
from fastgames.routers import teams, auth

app = FastAPI()

Base.metadata.create_all(bind=engine)

app.include_router(teams.router)
app.include_router(auth.router)
