from fastapi import APIRouter, Depends, HTTPException
from sqlalchemy.orm import Session
from starlette import status

from fastgames.database import SessionLocal
from fastgames.models.team import Team as TeamModel
from fastgames.schemas.team import TeamCreate, Team

__prefix__='/teams'
__api_version__= '/api/v1'

router = APIRouter(prefix=f"{__api_version__}{__prefix__}", tags=["teams"])


def get_db():
    db = SessionLocal()
    try:
        yield db
    finally:
        db.close()


@router.post("/", response_model=Team)
def create_team(team: TeamCreate, db: Session = Depends(get_db)):
    db_team = TeamModel(name=team.name, country=team.country, rank=team.rank)
    db.add(db_team)
    db.commit()
    db.refresh(db_team)
    return db_team


@router.get("/", response_model=list[Team])
def read_teams(skip: int = 0, limit: int = 10, db: Session = Depends(get_db)):
    """
    Obtenir tots els equips.
    :param skip: element inicial
    :param limit: element final
    :param db: sessió de la base de dades
    :return: llistat d'equips
    """
    return db.query(TeamModel).offset(skip).limit(limit).all()


@router.get("/{team_id}", response_model=Team)
def get_team_by_id(team_id: int, db: Session = Depends(get_db)):
    """
    Obtenir un equip per identificador.
    :param team_id: identificador de l'equip
    :param db: sessió de la base de dades
    :return: equip
    """
    team = db.query(TeamModel).filter(TeamModel.id == team_id).first()
    if team is None:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail="Team not found")
    return team


@router.delete("/{team_id}", response_model=Team)
def delete_team(team_id: int, db: Session = Depends(get_db)):
    """
    Eliminar un equip.
    :param team_id: identificador de l'equip
    :param db: sessió de la base de dades
    :return: equip eliminat
    """
    team = db.query(TeamModel).filter(TeamModel.id == team_id).first()
    if team is None:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail="Team not found")
    db.delete(team)
    db.commit()
    return team








