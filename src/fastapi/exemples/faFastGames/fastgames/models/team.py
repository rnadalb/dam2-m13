from sqlalchemy import Column, Integer, String

from fastgames.database import Base


class Team(Base):
    __tablename__ = "tbl_teams"

    id = Column(Integer, primary_key=True, index=True, autoincrement=True)
    name = Column(String, index=True)
    country = Column(String)
    rank = Column(Integer, default=10)
