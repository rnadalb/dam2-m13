# Operacions CRUD per a Persona
from fastapi import HTTPException
from sqlalchemy.orm import Session
from starlette import status

from app.models.persona import Persona
from app.schemas.persona import PersonaCreateSchema


def get_persona(db: Session, persona_id: int):
    """
    Recupera una persona de la base de dades en funció de l'ID de la persona proporcionada.
    Paràmetres:
        db (Session): La sessió de la base de dades.
        persona_id (int): L'ID de la persona a recuperar.

    Retorna:
        Persona: L'objecte de persona amb l'ID coincident, o None si no es troba cap persona.
    """
    return db.query(Persona).filter(Persona.id == persona_id).first()


def get_persones(db: Session, skip: int = 0, limit: int = 100):
    """
    Recupera una llista de persones de la base de dades.

    Paràmetres:
        db (Session): La sessió de la base de dades.
        skip (int, opcional): El nombre de persones a saltar. Per defecte és 0.
        limit (int, opcional): El nombre màxim de persones a recuperar. Per defecte és 100.

    Retorna:
        List[Persona]: Una llista de persones recuperades de la base de dades.
    """
    return db.query(Persona).offset(skip).limit(limit).all()


def create_persona(db: Session, persona: PersonaCreateSchema):
    """
    Crea una nova persona a la base de dades.

    Paràmetres:
        db (Session): L'objecte sessió de la base de dades.
        persona (PersonaCreateSchema): L'esquema que representa la persona a crear.

    Retorna:
        Persona: L'objecte persona creat.

    Llança:
        Exception: Si es produeix un error durant la creació de la persona.
    """
    try:
        db_persona = Persona(**persona.model_dump())
        db.add(db_persona)
        db.commit()
        db.refresh(db_persona)
        return db_persona
    except Exception as e:
        db.rollback()
        raise e


def update_persona(db: Session, persona_id: int, persona: PersonaCreateSchema):
    """
    Actualitza una persona a la base de dades.

    Paràmetres:
    - db (Session): La sessió de la base de dades.
    - persona_id (int): L'ID de la persona a actualitzar.
    - persona (PersonaCreateSchema): Les dades actualitzades de la persona.

    Retorna:
    - db_persona (Persona): L'entitat de la persona actualitzada.
    """
    db_persona = get_persona(db, persona_id=persona_id)
    if db_persona is None:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail="Persona no trobada")
    if db_persona:
        for key, value in persona.model_dump().items():
            setattr(db_persona, key, value)
        db.commit()
        db.refresh(db_persona)
    return db_persona


def delete_persona(db: Session, persona_id: int):
    """
    Elimina una persona de la base de dades.

    Args:
        db (Session): La sessió de la base de dades.
        persona_id (int): L'ID de la persona a eliminar.

    Retorna:
        La persona eliminada.
    """
    db_persona = get_persona(db, persona_id=persona_id)
    if db_persona is None:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail="Persona no trobada")
    db.delete(db_persona)
    db.commit()
    return db_persona
