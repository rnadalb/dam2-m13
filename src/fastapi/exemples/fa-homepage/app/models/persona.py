from sqlalchemy import Column, Integer, String

from app.backend.database import Base


class Persona(Base):
    __tablename__ = 'tbl_persona'

    id = Column(Integer, primary_key=True, index=True)
    email = Column(String, unique=True, index=True)
    nom = Column(String)
    edat = Column(Integer)
