from typing import Optional

from pydantic import BaseModel


# Esquema base per a Persona
class PersonaBaseSchema(BaseModel):
    email: str
    nom: str
    edat: int


# Esquema per a Crear una nova Persona
class PersonaCreateSchema(PersonaBaseSchema):
    pass


# Esquema per a Llegir i Actualitzar una Persona
class PersonaUpdateSchema(PersonaBaseSchema):
    pass


# Esquema per a la Resposta de Persona
class PersonaResponseSchema(PersonaBaseSchema):
    id: int

    class Config:
        form_attributes = True
