from typing import List

from fastapi import Depends, APIRouter, Request, Form
from sqlalchemy.orm import Session
from starlette import status
from starlette.exceptions import HTTPException
from starlette.responses import RedirectResponse
from starlette.templating import Jinja2Templates

import app.schemas.persona as schemas
import app.services.persona as crud
from app.backend import database

router = APIRouter(prefix="/persones", tags=["persones"])

templates = Jinja2Templates(directory="templates")


def get_db():
    db = database.SessionLocal()
    try:
        yield db
    finally:
        db.close()


# Ruta GET per llistar totes les persones (pàgina principal)
@router.get("/", response_model=List[schemas.PersonaResponseSchema])
def read_persones(request: Request, db: Session = Depends(get_db)):
    persones_data = crud.get_persones(db)
    return templates.TemplateResponse("index.html", {"request": request, "persones_data": persones_data})


# Ruta POST per a crear una nova persona
# Mostra el formulari (pàgina web) de creació d'una nova persona
@router.get("/create_persona")
def create_persona_form(request: Request):
    return templates.TemplateResponse("create_persona.html", {"request": request})


# Mètode que emmagatzema el resultat de la petició POST a la BBDD
# Les dades de la petició POST ve donada pel formulari crea_persona.html
@router.post("/", response_model=schemas.PersonaResponseSchema)
def create_persona(email: str = Form(...),
                   nom: str = Form(...),
                   edat: int = Form(...),
                   db: Session = Depends(get_db)
                   ):
    persona = schemas.PersonaCreateSchema(email=email, nom=nom, edat=edat)
    crud.create_persona(db=db, persona=persona)
    return RedirectResponse(url="/persones", status_code=status.HTTP_303_SEE_OTHER)


# Ruta PUT per a actualitzar una persona
@router.get("/{persona_id}")
def edit_persona_form(request: Request, persona_id: int, db: Session = Depends(get_db)):
    persona = crud.get_persona(db, persona_id=persona_id)
    # En teoria no pot passar ja que el id de persona l'agafa de la taula a index.html
    # però si s'intenta possar des de la barra de navegació un valor que no existeix
    # aquest 'filtre' retornarà un HTTP_404 --> Prova-ho !
    if persona is None:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail="Persona no trobada")
    return templates.TemplateResponse("edit_persona.html", {"request": request, "persona": persona})


# Ruta POST per a actualitzar una persona
# Solament es permeten els mètodes GET i POST a un formulari web
# per tant modifiquem el mètode put i l'adaptem.
@router.post("/{persona_id}", response_model=schemas.PersonaResponseSchema)
def update_persona(
        persona_id: int,
        email: str = Form(...),
        nom: str = Form(...),
        edat: int = Form(...),
        db: Session = Depends(get_db)
):
    persona = schemas.PersonaCreateSchema(email=email, nom=nom, edat=edat)
    persona.nom = nom
    persona.email = email
    persona.edat = edat
    crud.update_persona(db=db, persona_id=persona_id, persona=persona)
    return RedirectResponse(url="/persones", status_code=status.HTTP_303_SEE_OTHER)


# Ruta DELETE per a eliminar una persona
# Hem utilitzat JavaScript per demanar la confirmació
# No caldria cercar si existeix o no la persona ja que sabem que l'identificador
# és vàlid perquè està sent llistat a la pàgina index.html
@router.get("/delete/{persona_id}")
def delete_persona_request(persona_id: int, db: Session = Depends(get_db)):
    crud.delete_persona(db=db, persona_id=persona_id)
    return RedirectResponse(url="/persones", status_code=status.HTTP_303_SEE_OTHER)
