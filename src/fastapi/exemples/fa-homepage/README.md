# _Simple people manager_

#### Descripció del projecte

Aquest projecte és una aplicació senzilla per la gestió de persones que permeti realitzar operacions CRUD (Crear, Llegir, Actualitzar, Eliminar) sobre una base de dades remota.

L'aplicació consta de diverses parts clau:

- **Models de la base de dades**: Utilitzem SQLAlchemy per a definir els models que representen les taules de la nostra base de dades.
- **Esquemes pydantic**: Per a la validació de dades, utilitzem esquemes Pydantic que ens ajuden a garantir que les dades entrants i sortints compleixen amb l'estructura desitjada.
- **Rutes FastAPI**: Definim rutes específiques per a cada operació CRUD, així com per a la visualització de les diferents pàgines web.
- **[Jinja](https://jinja.palletsprojects.com/en/3.1.x/), HTML i CSS**: Per a la interfície d'usuari, utilitzem templates HTML amb estils CSS, que complementarem amb estructures de control i altres elements de templating proveïts pel paquet [Jinja2](https://pypi.org/project/Jinja2/). Això ens permetrà interactuar amb els usuaris interactuar de manera fàcil i intuïtiva.

## Configuració del Projecte

#### Requisits

Per començar a treballar amb aquest projecte, necessitaràs tenir instal·lats els següents components en el teu sistema:

- Python 3.9 o superior: El llenguatge de programació utilitzat per a desenvolupar l'aplicació.
- Pip: El gestor de paquets de Python, que s'utilitza per a instal·lar les biblioteques necessàries.

#### Instal·lació i configuració amb PyCharm

1. **Crea un nou projecte**: Des del menú `File --> New Project --> FastAPI`.
   ![New FastAPI project](new_fastapi_project.png){width 25%}

2. **Instal·la les dependències**: Pots utilitzar l'ordre o afegir tu mateix els paquets des de la interfície de l'IDE.

   ```shell
   pip install -r requirements.txt
   ```
   
Els paquets a afegir són:
```text
fastapi==0.108.0
psycopg2-binary==2.9.9
pydantic-settings==2.1.0
python-multipart==0.0.6
SQLAlchemy==2.0.25
uvicorn==0.25.0
Jinja2==3.1.2
```
3. **Estructura del projecte**
La correcta estructuració d'un projecte és fonamental per mantenir el codi organitzat, fàcil de mantenir i escalable.
En aquest cas utitlizem la que vam veure anteriorment:
```text
── app
│   ├── backend               # Funcionalitat i configuració del backend 
│   │   ├── config.py           # Configuració de l'app
│   │   ├── database.py         # Gestió de la BBDD (sessions, connexió, ...)
│   ├── models                # Models de l'SQLAlchemy
│   │   └── persona.py          # Model Persona 
│   ├── routers               # Rutes/Paths de l'API
│   │   └── persona.py          # Paths per la part de persona
│   ├── schemas               # Models Pydantic
│   │   └── persona.py          # Tip: acabem amb Schema per identifdicar-los millor
│   ├── services              # Lògica de negoci
│   │   └── persona.py          # CRUD persona
│   ├── main.py              # Aplicació principal (runner)
│   └── version.py           # Versió de l'applicació
├── static                   # Directori de recursos del lloc web (imatges, css, icones, ...)
├── templates                # Directori on es trobaran les pàgines web del lloc
└── requirements.txt         # Dependències (paquets)
```

## Configura la base de dades
Aquest projecte utilitza SQLAlchemy com a [ORM](https://ca.wikipedia.org/wiki/Mapatge_d%27objectes_relacional). Aquesta aplicació utilitza PostgreSQL com a base de dades. 
A l'exemple utilitzarem 2 arxius per configurar la base de dades:
* [backend/config.py](app/backend/config.py): Aquest fitxer conté les variables necessàries per accedir de manera **segura** a la base dades així com de l'URL de la base de dades. Evita publicar les credencials a un lloc públic i/o accesible.
```python
import os
import urllib

from pydantic_settings import BaseSettings


class Settings(BaseSettings):
    # PostgreSQL
    DB_USER: str = os.environ.get('POSTGRES_USER')
    DB_PASSWORD: str = urllib.parse.quote_plus(os.environ.get('POSTGRES_PASSWORD'))  # Caràcters com @ són adaptats
    DB_HOST: str = os.environ.get('POSTGRES_HOST')
    DB_PORT: int = os.environ.get('POSTGRES_PORT')
    DB_NAME: str = os.environ.get('POSTGRES_DBNAME')
    DB_SSL_MODE: str = 'require'

    # SQL Alchemy
    SQLALCHEMY_DATABASE_URI: str = f"postgresql+psycopg2://{DB_USER}:{DB_PASSWORD}@{DB_HOST}:{DB_PORT}/{DB_NAME}?sslmode={DB_SSL_MODE}"


settings = Settings()
```
* [backend/database.py](app/backend/database.py): Aquí trobem la configuració de les parts necessàries perquè SQLAlchemy pugui treballar:
```python
from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker

from app.backend.config import settings

SQLALCHEMY_DATABASE_URL = settings.SQLALCHEMY_DATABASE_URI

engine = create_engine(SQLALCHEMY_DATABASE_URL)
SessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=engine)

Base = declarative_base()
```
Alguns aclariments ...
```python
SessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=engine)
```
👉 Crea una sessió a partir d'un patró _factory_. Les sessions són utilitzades per a gestionar les operacions amb la base de dades, com les queries i les transaccions.
```python
Base = declarative_base()
```
👉 Crea una classe base per a tots els models de la teva base de dades en SQLAlchemy. Els models que hereten d'aquesta classe base (Base) podran mapejar-se automàticament a taules en la base de dades.

## Model de la base de dades

Els models de base de dades són definicions de com les nostres entitats de dades (en aquest cas, la taula persones) estan estructurades. Utilitzem SQLAlchemy per a definir aquests models de manera que puguin ser mapejats a taules en una base de dades relacional. A continuació, es detalla com definim el model per a la taula `persona`.

`models/persona.py`:

```python
from sqlalchemy import Column, Integer, String

from app.backend.database import Base


class Persona(Base):
    __tablename__ = 'tbl_persona'

    id = Column(Integer, primary_key=True, index=True)
    email = Column(String, unique=True, index=True)
    nom = Column(String)
    edat = Column(Integer)
```

## Esquemes pydantic
Pydantic és una biblioteca de Python que s'utilitza per a la definició de dades basada en models de Python amb anotacions de tipus. En FastAPI, els esquemes Pydantic són utilitzats per a la validació de dades d'entrada i sortida. Els esquemes permeten assegurar que les dades enviades a l'API compleixen amb una estructura específica i que les dades retornades al client també ho fan.
Per facilitar la diferències entre esquemes i models podem fer que esls primers acabin, sempre, amb la paraula Schema

`schemas/persona.py`:

```python
from typing import Optional

from pydantic import BaseModel


# Esquema base per a Persona
class PersonaBaseSchema(BaseModel):
    email: str
    nom: str
    edat: int


# Esquema per a Crear una nova persona
class PersonaCreateSchema(PersonaBaseSchema):
    pass


# Esquema per a llegir i actualitzar una Persona
class PersonaUpdateSchema(PersonaBaseSchema):
    pass


# Esquema per a la Resposta de Persona (amb totes les dades)
class PersonaResponseSchema(PersonaBaseSchema):
    id: int

    class Config:
        form_attributes = True
```

## Operacions CRUD
Si consultem el mòdul [persona](app/services/persona.py) trobarem les funcions que ens permetran fer les operacions bàsiques contra una taula en una base de dades: crear nous registres, llistar tots els registres, obtenir les dades d'un registre, modificar un regitre existent i eliminar-lo.

```python
# Operacions CRUD per a Persona
from fastapi import HTTPException
from sqlalchemy.orm import Session
from starlette import status

from app.models.persona import Persona
from app.schemas.persona import PersonaCreateSchema


def get_persona(db: Session, persona_id: int):
    """
    Recupera una persona de la base de dades en funció de l'ID de la persona proporcionada.
    Paràmetres:
        db (Session): La sessió de la base de dades.
        persona_id (int): L'ID de la persona a recuperar.

    Retorna:
        Persona: L'objecte de persona amb l'ID coincident, o None si no es troba cap persona.
    """
    return db.query(Persona).filter(Persona.id == persona_id).first()


def get_persones(db: Session, skip: int = 0, limit: int = 100):
    """
    Recupera una llista de persones de la base de dades.

    Paràmetres:
        db (Session): La sessió de la base de dades.
        skip (int, opcional): El nombre de persones a saltar. Per defecte és 0.
        limit (int, opcional): El nombre màxim de persones a recuperar. Per defecte és 100.

    Retorna:
        List[Persona]: Una llista de persones recuperades de la base de dades.
    """
    return db.query(Persona).offset(skip).limit(limit).all()


def create_persona(db: Session, persona: PersonaCreateSchema):
    """
    Crea una nova persona a la base de dades.

    Paràmetres:
        db (Session): L'objecte sessió de la base de dades.
        persona (PersonaCreateSchema): L'esquema que representa la persona a crear.

    Retorna:
        Persona: L'objecte persona creat.

    Llança:
        Exception: Si es produeix un error durant la creació de la persona.
    """
    try:
        db_persona = Persona(**persona.model_dump())
        db.add(db_persona)
        db.commit()
        db.refresh(db_persona)
        return db_persona
    except Exception as e:
        db.rollback()
        raise e


def update_persona(db: Session, persona_id: int, persona: PersonaCreateSchema):
    """
    Actualitza una persona a la base de dades.

    Paràmetres:
    - db (Session): La sessió de la base de dades.
    - persona_id (int): L'ID de la persona a actualitzar.
    - persona (PersonaCreateSchema): Les dades actualitzades de la persona.

    Retorna:
    - db_persona (Persona): L'entitat de la persona actualitzada.
    """
    db_persona = get_persona(db, persona_id=persona_id)
    if db_persona is None:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail="Persona no trobada")
    if db_persona:
        for key, value in persona.model_dump().items():
            setattr(db_persona, key, value)
        db.commit()
        db.refresh(db_persona)
    return db_persona


def delete_persona(db: Session, persona_id: int):
    """
    Elimina una persona de la base de dades.

    Args:
        db (Session): La sessió de la base de dades.
        persona_id (int): L'ID de la persona a eliminar.

    Retorna:
        La persona eliminada.
    """
    db_persona = get_persona(db, persona_id=persona_id)
    if db_persona is None:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail="Persona no trobada")
    db.delete(db_persona)
    db.commit()
    return db_persona

```

## Rutes FastAPI

Al projecte, hem definit rutes per gestionar les persones. Aquestes rutes són implementades en el fitxer `routers\persones.py` i utilitzen els models de SQLAlchemy per a interactuar amb la base de dades i els esquemes Pydantic per a la validació de dades.
Com veuràs són una mica diferents a les habituals ja que les hem hagut d'adaptar a les diferents accions que llencem des del web.

Solament recordar que en un formulari web solkament s'accepten els mètodes `POST` i `GET`. Consulta [aquí](https://www.w3schools.com/tags/tag_form.asp) per obtenir més informació.

📌 [routers/persona.py](app/routers/persona.py)

```python
from typing import List

from fastapi import Depends, APIRouter, Request, Form
from sqlalchemy.orm import Session
from starlette import status
from starlette.exceptions import HTTPException
from starlette.responses import RedirectResponse
from starlette.templating import Jinja2Templates

import app.schemas.persona as schemas
import app.services.persona as crud
from app.backend import database

router = APIRouter(prefix="/persones", tags=["persones"])

templates = Jinja2Templates(directory="templates")


def get_db():
    db = database.SessionLocal()
    try:
        yield db
    finally:
        db.close()


# Ruta GET per llistar totes les persones (pàgina principal)
@router.get("/", response_model=List[schemas.PersonaResponseSchema])
def read_persones(request: Request, db: Session = Depends(get_db)):
    persones_data = crud.get_persones(db)
    return templates.TemplateResponse("index.html", {"request": request, "persones_data": persones_data})


# Ruta POST per a crear una nova persona
# Mostra el formulari (pàgina web) de creació d'una nova persona
@router.get("/create_persona")
def create_persona_form(request: Request):
    return templates.TemplateResponse("create_persona.html", {"request": request})


# Mètode que emmagatzema el resultat de la petició POST a la BBDD
# Les dades de la petició POST ve donada pel formulari crea_persona.html
@router.post("/", response_model=schemas.PersonaResponseSchema)
def create_persona(email: str = Form(...),
                   nom: str = Form(...),
                   edat: int = Form(...),
                   db: Session = Depends(get_db)
                   ):
    persona = schemas.PersonaCreateSchema(email=email, nom=nom, edat=edat)
    crud.create_persona(db=db, persona=persona)
    return RedirectResponse(url="/persones", status_code=status.HTTP_303_SEE_OTHER)


# Ruta PUT per a actualitzar una persona
@router.get("/{persona_id}")
def edit_persona_form(request: Request, persona_id: int, db: Session = Depends(get_db)):
    persona = crud.get_persona(db, persona_id=persona_id)
    # En teoria no pot passar ja que el id de persona l'agafa de la taula a index.html
    # però si s'intenta possar des de la barra de navegació un valor que no existeix
    # aquest 'filtre' retornarà un HTTP_404 --> Prova-ho !
    if persona is None:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail="Persona no trobada")
    return templates.TemplateResponse("edit_persona.html", {"request": request, "persona": persona})


# Ruta POST per a actualitzar una persona
# Solament es permeten els mètodes GET i POST a un formulari web
# per tant modifiquem el mètode put i l'adaptem.
@router.post("/{persona_id}", response_model=schemas.PersonaResponseSchema)
def update_persona(
        persona_id: int,
        email: str = Form(...),
        nom: str = Form(...),
        edat: int = Form(...),
        db: Session = Depends(get_db)
):
    persona = schemas.PersonaCreateSchema(email=email, nom=nom, edat=edat)
    persona.nom = nom
    persona.email = email
    persona.edat = edat
    crud.update_persona(db=db, persona_id=persona_id, persona=persona)
    return RedirectResponse(url="/persones", status_code=status.HTTP_303_SEE_OTHER)


# Ruta DELETE per a eliminar una persona
# Hem utilitzat JavaScript per demanar la confirmació
# No caldria cercar si existeix o no la persona ja que sabem que l'identificador
# és vàlid perquè està sent llistat a la pàgina index.html
@router.get("/delete/{persona_id}")
def delete_persona_request(persona_id: int, db: Session = Depends(get_db)):
    crud.delete_persona(db=db, persona_id=persona_id)
    return RedirectResponse(url="/persones", status_code=status.HTTP_303_SEE_OTHER)
```
## Jinja, HTML i CSS
Els templates HTML i els fitxers CSS són essencials per a crear una interfície d'usuari atractiva i fàcil d'utilitzar en aplicacions web. Al projecte utilitzarem aquests recursos per presentar les dades de forma estructurada i permetre als usuaris interactuar amb l'aplicació, realitzant les operacions més bàsiques com veure llistats de persones, crear, editar i eliminar registres.

Les templates Jinja són una part fonamental en moltes aplicacions web Python, incloent les desenvolupades amb FastAPI. Jinja és un motor de plantilles per a Python que permet la creació de pàgines HTML dinàmiques, combinant dades de Python amb codi HTML. Utilitza una sintaxi clara i expressiva que facilita la integració de lògica de programació dins de les pàgines web.

#### Estructures de control més comunes en Jinja

1. **Variables**: Pots utilitzar variables passades al template des de la teva aplicació FastAPI. Les variables són incloses dins de dobles claus `{{ }}`.

   ```html
   <p>Hola, {{ usuari.nom }}!</p>
   ```

2. **Estructures de control condicional**: Jinja permet utilitzar declaracions condicionals amb `if`, `elif`, i `else`.

   ```html
   {% if usuari.is_admin %}
   <p>Ets un administrador.</p>
   {% else %}
   <p>Ets un usuari normal.</p>
   {% endif %}
   ```

3. **Bucles**: Pots utilitzar bucles `for` per iterar sobre col·leccions.

   ```html
   <ul>
   {% for producte in llista_productes %}
       <li>{{ producte.nom }}</li>
   {% endfor %}
   </ul>
   ```

4. **Herència de templates**: Jinja suporta la herència de templates, permetent crear un "layout" base i estendre'l en altres templates. Això s'aconsegueix amb `block` i `extends`.

   - Template base (`base.html`):
     ```html
     <html>
     <head>
         <title>{% block title %}{% endblock %}</title>
     </head>
     <body>
         {% block content %}{% endblock %}
     </body>
     </html>
     ```

   - Template estesa:
     ```html
     {% extends 'base.html' %}

     {% block title %}Pàgina d'Inici{% endblock %}

     {% block content %}
     <h1>Benvingut a la Pàgina d'Inici</h1>
     {% endblock %}
     ```

5. **Incloure altres templates**: Pots incloure un template dins d'un altre amb la directriu `include`.

   ```html
   <div>
       {% include 'header.html' %}
   </div>
   ```
### [Templates](templates) al projecte

* `static/index.html`: Pàgina principal, accessible des de [http://localhost:8000/persones](http://localhost:8000/persones)
   ```html
   <!DOCTYPE html>
   <html lang="ca">
   <head>
       <title>Llista de Persones</title>
       <link href="/static/styles.css" rel="stylesheet">
       <link type="image/png" sizes="16x16" rel="icon" href="/static/icons8-favicon-16.png">
       <script>
           function confirma_delete(personaId) {
               if (confirm("Estàs segur que vols eliminar aquesta persona?")) {
                   window.location.href = `/persones/delete/${personaId}`;
               }
           }
       </script>
   </head>
   <body>
       <div class="container">
           <h1>Llista de Persones</h1>
           <a href="/persones/create_persona" class="button">Crear Nova Persona</a>
           <table>
               <thead>
                   <tr>
                       <th>ID</th>
                       <th>Email</th>
                       <th>Nom</th>
                       <th>Edat</th>
                       <th>Accions</th>
                   </tr>
               </thead>
               <tbody>
                   {% for persona in persones_data %}
                   <tr>
                       <td>{{ persona.id }}</td>
                       <td>{{ persona.email }}</td>
                       <td>{{ persona.nom }}</td>
                       <td>{{ persona.edat }}</td>
                       <td>
                           <a href="/persones/{{ persona.id }}" class="edit-button">Editar</a>
                           <button onclick="confirma_delete({{ persona.id }})" class="delete-button">Eliminar</button>
                       </td>
                   </tr>
                   {% endfor %}
               </tbody>
           </table>
       </div>
   </body>
   </html>

   ```
* `static/create_persona.html`: Formulari per crear una persona nova.
   ```html
   <!DOCTYPE html>
   <html lang="ca">
   <head>
       <title>Crea una nova persona</title>
       <link href="/static/styles.css" rel="stylesheet">
   </head>
   <body>
       <div class="container">
           <h1>Crea una nova persona</h1>
           <form action="/persones/" method="post">
               <div class="form-group">
                   <label for="email">Email:</label>
                   <input type="email" id="email" name="email" required>
               </div>
   
               <div class="form-group">
                   <label for="nom">Nom:</label>
                   <input type="text" id="nom" name="nom" required>
               </div>
   
               <div class="form-group">
                   <label for="edat">Edat:</label>
                   <input type="number" id="edat" name="edat" required>
               </div>
   
               <div class="form-group">
                   <button type="submit" class="btn">Crear</button>
               </div>
           </form>
       </div>
   </body>
   </html>
   ```
* `static/edit_persona.html`: Formulari per modificar una persona existent.
   ```html
   <!DOCTYPE html>
   <html lang="ca">
   <head>
       <title>Edita Persona</title>
       <link href="/static/styles.css" rel="stylesheet">
   </head>
   <body>
       <div class="container">
           <h2>Edita Persona</h2>
           <form action="/persones/{{ persona.id }}" method="POST">
               <input type="hidden" name="id" value="{{ persona.id }}">
               <!-- <input name="id" value="{{ persona.id }}"> -->
               <div class="form-group">
                   <label for="email">Email:</label>
                   <input type="email" id="email" name="email" value="{{ persona.email }}" required>
               </div>
   
               <div class="form-group">
                   <label for="nom">Nom:</label>
                   <input type="text" id="nom" name="nom" value="{{ persona.nom }}">
               </div>
   
               <div class="form-group">
                   <label for="edat">Edat:</label>
                   <input type="number" id="edat" name="edat" value="{{ persona.edat }}">
               </div>
   
               <div class="form-group">
                   <button type="submit" class="btn">Actualitza</button>
               </div>
           </form>
       </div>
   </body>
   </html>
   ```
* `static/404.html`: Pàgina 404. Pàgina no trobada.
   ```html
   <!DOCTYPE html>
   <html lang="ca">
   <head>
       <title>404 - Pàgina no trobada</title>
       <link href="/static/styles404.css" rel="stylesheet">
   </head>
   <body>
       <div class="error-container">
           <h1>Oops!</h1>
           <h2>404 - Pàgina no trobada</h2>
           <p>Sembla que aquesta pàgina ha fugit al espai sideral.</p>
           <img src="/static/404-astronaut.jpg" alt="Astronauta perdut">
           <br>
           <a href="/persones" class="button">Torna a casa</a>
       </div>
   </body>
   </html>
   ```
### Estils CSS

* `static/styles.css`: Estil del lloc web
   ```css
   body {
       font-family: Arial, sans-serif;
       margin: 0;
       padding: 0;
       background-color: #f4f4f4;
   }
   
   .form-group {
       margin-bottom: 15px;
   }
   
   .container {
       width: 80%;
       margin: 20px auto;
       padding: 20px;
       background-color: #fff;
       box-shadow: 0 0 10px rgba(0, 0, 0, 0.1);
   }
   
   input[type="email"], input[type="text"], input[type="number"] {
       width: 100%;
       padding: 8px;
       border: 1px solid #ddd;
       border-radius: 4px;
       box-sizing: border-box;
   }
   
   table {
       width: 100%;
       border-collapse: collapse;
       margin-top: 20px;
   }
   
   th, td {
       border: 1px solid #dddddd;
       text-align: left;
       padding: 8px;
   }
   
   th {
       background-color: #f2f2f2;
   }
   
   label {
       display: block;
       margin-bottom: 5px;
   }
   
   .button, .edit-button, .delete-button {
       padding: 6px 12px;
       color: white;
       background-color: #007bff;
       text-decoration: none;
       border-radius: 5px;
   }
   
   .edit-button {
       background-color: #28a745;
   }
   
   .delete-button {
       background-color: #dc3545;
   }
   
   .button:hover {
       background-color: #0056b3;
   }
   
   .edit-button:hover {
       background-color: #1ecb76;
   }
   
   .delete-button:hover {
       background-color: #ea5a69;
   }
   ```
* `static/styles404.css`: Estil de la pàgina 404.
   ```css
   body {
       font-family: 'Arial', sans-serif;
       background-color: #282c34;
       color: white;
       text-align: center;
       padding: 0;
       margin: 0;
   }
   
   .error-container {
       position: absolute;
       top: 50%;
       left: 50%;
       transform: translate(-50%, -50%);
   }
   
   .error-container h1 {
       font-size: 3rem;
   }
   
   .error-container h2 {
       font-size: 2rem;
   }
   
   .error-container p {
       font-size: 1.2rem;
   }
   
   .error-container img {
       max-width: 100%;
       height: auto;
       margin-top: 20px;
   }
   
   .button {
       display: inline-block;
       margin-top: 20px;
       padding: 10px 15px;
       background-color: #007bff;
       color: white;
       text-decoration: none;
       border-radius: 5px;
   }
   
   .button:hover {
       background-color: #0056b3;
   }
   ```
