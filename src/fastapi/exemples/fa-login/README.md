# Sistema d'autenticació amb FastAPI

## Descripció

L'objectiu d'aquest exemple és desenvolupar una aplicació web utilitzant FastAPI que incorpori funcionalitats d'autenticació d'usuaris. El projecte inclourà la creació d'una landing page amb opcions d'accés públic i seccions per a login i registre d'usuaris (sign up).

Cal desenvolupar una aplicació web amb FastAPI que permeti als usuaris registrar-se, iniciar sessió i accedir a contingut protegit. La pàgina principal de l'aplicació (landing page) ha de contenir les següents seccions:

- **Portada**: Una presentació breu de l'aplicació.
- **Serveis**: Informació sobre els serveis oferts per l'aplicació.
- **Login**: Una pàgina per a que els usuaris registrats puguin iniciar sessió.
- **Sign Up**: Un formulari per a que nous usuaris es puguin registrar.

### Requisits

1. **Landing page**: Desenvolupar una interfície d'usuari amigable que inclogui les seccions esmentades. Utilitzar templates HTML i CSS per a dissenyar la pàgina.

2. **Procés d'autencicació**:
   - Implementar la funcionalitat de registre d'usuaris (sign up). Els usuaris hauran de proporcionar un correu electrònic i una contrasenya.
   - Implementar la funcionalitat d'inici de sessió (login). Els usuaris podran iniciar sessió utilitzant les seves credencials.
   - Protegir les rutes d'accés a les seccions d'usuaris autenticats.

3. **Gestió de sessions**: Mantenir la sessió d'usuari activa després de l'inici de sessió i proporcionar una opció per a tancar-la (logout).

4. **Seguretat**: Assegurar-se que les contrasenyes dels usuaris són emmagatzemades de manera segura utilitzant algorismes hash i requisits de complexitat.

5. **Base de dades**: Utilitzar SQLAlchemy per a gestionar una base de dades que emmagatzemi la informació dels usuaris.

### Recursos

- FastAPI [Documentació oficial](https://fastapi.tiangolo.com/es/)
- SQLAlchemy [Tutorial](https://docs.sqlalchemy.org/en/20/)
- HTML [Guia de Referència](https://developer.mozilla.org/es/docs/Web/HTML)
- CSS [Guia de Referència](https://developer.mozilla.org/es/docs/Web/CSS)
- Templates Jinja [Documentació Oficial](https://jinja.palletsprojects.com/)


Images by [Unsplash](https://unsplash.com/)
[Black alarm clock at 10 : 10 on the white wooden table](https://unsplash.com/photos/X63FTIZFbZo)