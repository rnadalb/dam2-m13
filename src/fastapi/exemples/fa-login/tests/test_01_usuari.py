# tests/test_01_usuari.py
import unittest

from starlette import status
from werkzeug.security import generate_password_hash

from test_config import client, setup_test_database, teardown_test_database


class UserTests(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        # Configuració abans de tots els tests
        setup_test_database(init=True)

    @classmethod
    def tearDownClass(cls):
        # Neteja després de tots els tests
        teardown_test_database()

    def test_01_create_user(self):
        """
        Test per verificar la creació d'un nou usuari.
        """
        response = client.post("/usuari/", json={
            "correu_electronic": "jdoe@proves.cat",
            "contrasenya": generate_password_hash("password123"),
            "nom_complet": "John Doe"
        })
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        data = response.json()
        self.assertEqual(data["correu_electronic"], "jdoe@proves.cat")
        self.assertTrue(data["nom_complet"], "John Doe")


if __name__ == "__main__":
    unittest.main()
