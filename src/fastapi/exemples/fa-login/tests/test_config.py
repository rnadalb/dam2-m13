from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from fastapi.testclient import TestClient
from app.main import app
from app.database import Base
from app.dependencies import get_db

IN_MEMORY = False
DB_FILE = "./test.db"

# Variables
_space = None

# Configurar la base de dades de prova com SQLite
SQLALCHEMY_DATABASE_URL = "sqlite:///:memory:" if IN_MEMORY else f"sqlite:///./{DB_FILE}"

engine = create_engine(SQLALCHEMY_DATABASE_URL, connect_args={"check_same_thread": False})
TestingSessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=engine)


# Sobreescriure la dependència de la base de dades
def override_get_db():
    db = TestingSessionLocal()
    try:
        yield db
    finally:
        db.close()


# Configurar l'aplicació FastAPI per utilitzar la base de dades de prova
app.dependency_overrides[get_db] = override_get_db

# Client de FastAPI per a les proves
client = TestClient(app)


# Funció per inicialitzar la base de dades de prova amb les taules i dades necessàries
def setup_test_database(init: bool = False):
    if init:
        # Eliminar les taules de la base de dades de prova
        Base.metadata.drop_all(bind=engine)
        # Crear les taules a la base de dades de prova
        Base.metadata.create_all(bind=engine)


# Funció per netejar la base de dades després dels tests
def teardown_test_database(drop: bool = False):
    if drop:
        Base.metadata.drop_all(bind=engine)
