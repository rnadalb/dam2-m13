# Exemple de formulari d'introducció de dades


### Recursos

- FastAPI [Documentació oficial](https://fastapi.tiangolo.com/es/)
- SQLAlchemy [Tutorial](https://docs.sqlalchemy.org/en/20/)
- HTML [Guia de Referència](https://developer.mozilla.org/es/docs/Web/HTML)
- CSS [Guia de Referència](https://developer.mozilla.org/es/docs/Web/CSS)
- Templates Jinja [Documentació Oficial](https://jinja.palletsprojects.com/)


Images by [Unsplash](https://unsplash.com/)
[Black alarm clock at 10 : 10 on the white wooden table](https://unsplash.com/photos/X63FTIZFbZo)