from typing import List

from fastapi import APIRouter, Request, Depends
from sqlalchemy.orm import Session
from starlette import status
from starlette.exceptions import HTTPException
from starlette.responses import RedirectResponse
from starlette.templating import Jinja2Templates

import app.services.crud_dispositiu as crud
from app.backend.database import get_db
from app.schemas.dispositiu import DispositiuCreate, DispositiuUpdate, DispositiuInfo

router = APIRouter(prefix='/device', tags=['device'])

templates = Jinja2Templates(directory="templates")
_FOLDER = '/device'  # organització del directori de templates


@router.get('/new', response_model=List[DispositiuInfo])
async def create_new_device(request: Request):
    return templates.TemplateResponse(f'{_FOLDER}/device_form.html', {'request': request, 'device': None})


@router.get('/', response_model=List[DispositiuInfo])
async def get_all_devices(request: Request, db: Session = Depends(get_db)):
    devices = crud.get_all_devices(db)
    if devices:
        return templates.TemplateResponse(f'{_FOLDER}/devices.html', {"devices": devices, 'request': request})
    else:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail="Dispositiu no trobat")


@router.get('/{device_id}', response_model=DispositiuInfo)
async def read_device(device_id: int, request: Request, db: Session = Depends(get_db)):
    db_device = crud.get_device_by_id(device_id=device_id, db=db)
    if db_device:
        return templates.TemplateResponse(f'{_FOLDER}/device_form.html', {"device": db_device, 'request': request})
    else:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail="Dispositiu no trobat")


@router.post("/", response_model=DispositiuCreate, status_code=status.HTTP_201_CREATED)
async def create_device(device: DispositiuCreate):
    # Aquí aniria la lògica per inserir un nou dispositiu a la base de dades
    return '{"message": "Device created"}'


@router.put("/{device_id}", response_model=DispositiuUpdate)
async def update_device(device_id: int, device: DispositiuUpdate):
    return '{"message": "Device Modified"}'


@router.post("/delete/{device_id}", status_code=status.HTTP_204_NO_CONTENT)
async def delete_device(device_id: int, db: Session = Depends(get_db)):
    device = crud.delete_device_by_id(device_id, db)
    if device:
        return RedirectResponse("/device", status_code=status.HTTP_303_SEE_OTHER)

    else:  # No existeix el dispositiu
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail="Dispositiu no trobat")
