from fastapi import Depends
from sqlalchemy.orm import Session

from app.backend.database import get_db
from app.models.dispositiu import Dispositiu


def get_device_by_id(device_id: int, db: Session = Depends(get_db)):
    return db.query(Dispositiu).filter(Dispositiu.id == device_id).first()


def get_all_devices(db: Session = Depends(get_db)):
    return db.query(Dispositiu).all()


def delete_device_by_id(device_id: int, db: Session = Depends(get_db)):
    device = get_device_by_id(device_id, db)
    if device:
        db.delete(device)
        db.commit()
        return device
    return None
