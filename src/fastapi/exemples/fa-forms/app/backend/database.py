from sqlalchemy import create_engine
from sqlalchemy.orm import declarative_base, sessionmaker

from app.backend import config

SQLALCHEMY_DATABASE_URL = config.SQLALCHEMY_DATABASE_URI

engine = create_engine(SQLALCHEMY_DATABASE_URL)
SessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=engine)

Base = declarative_base()


def get_db():
    """
    Obté una connexió a la base de dades des del pool de sessions local.
    Retorna:
        Un objecte de connexió a la base de dades.
    """
    db = SessionLocal()
    try:
        yield db
    finally:
        db.close()
