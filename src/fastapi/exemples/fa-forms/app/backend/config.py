import urllib

from pydantic_settings import BaseSettings


class Settings(BaseSettings):
    # PostgreSQL
    POSTGRES_USER: str
    POSTGRES_PASSWORD: str
    POSTGRES_HOST: str
    POSTGRES_PORT: int
    POSTGRES_DBNAME: str
    POSTGRES_SSL_MODE: str
    SECRET_KEY: str
    ALGORITHM: str
    ACCESS_TOKEN_EXPIRE_SECONDS: int

    class Config:
        env_file = '.env'


settings = Settings()

SECRET_KEY = settings.SECRET_KEY
ALGORITHM = settings.ALGORITHM
ACCESS_TOKEN_EXPIRE_SECONDS = settings.ACCESS_TOKEN_EXPIRE_SECONDS
COOKIE_NAME = 'access_token'

# Adaptem la contrasenya per si té caràcters especials @#$%....
settings.POSTGRES_PASSWORD = urllib.parse.quote_plus(settings.POSTGRES_PASSWORD)

# SQL Alchemy
SQLALCHEMY_DATABASE_URI: str = f"postgresql+psycopg2://{settings.POSTGRES_USER}:{settings.POSTGRES_PASSWORD}@{settings.POSTGRES_HOST}:{settings.POSTGRES_PORT}/{settings.POSTGRES_DBNAME}?sslmode={settings.POSTGRES_SSL_MODE}"
