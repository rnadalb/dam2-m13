from pydantic import BaseModel


class DispositiuBase(BaseModel):
    alias: str
    deveui: str
    latitud: float
    longitud: float


class DispositiuCreate(DispositiuBase):
    pass


class DispositiuUpdate(DispositiuBase):
    pass


class DispositiuInfo(BaseModel):
    id: int
    alias: str
    deveui: str
    latitud: float
    longitud: float

    class ConfigDict:
        from_attributes = True
