from sqlalchemy import Column, Integer, String, Double

from app.backend.database import Base


class Dispositiu(Base):
    __tablename__ = 'tbl_dispositius'

    id = Column(Integer, primary_key=True, index=True, nullable=False)
    alias = Column(String, nullable=False)
    dev_eui = Column(String(64), index=True, unique=True, nullable=False)
    latitud = Column(Double, nullable=True)
    longitud = Column(Double, nullable=True)
