# **Índex de continguts: Introducció a FastAPI**

## **[1. Introducció a les API REST](doc/01_Introduccio_a_les_API_REST.md)**
- Què són les API?
- Diferència entre API i REST.
- Exemples pràctics d'ús d'API (amb serveis populars com OpenWeather o Spotify).

## **[2. Instal·lació i configuració de FastAPI](doc/02_installacio_configuracio.md)**
- Requisits previs:
    - Python >= 3.12
    - Entorn virtual amb `venv`
- Instal·lació de FastAPI i Uvicorn.
- Creació del primer projecte FastAPI.
- Executar el servidor i comprovació inicial.

## **[3. Arquitectura bàsica d'una aplicació FastAPI](doc/03_arquitectura_basica.md)**
- Estructura del projecte:
    - `main.py`
    - Folders modulars (`routes`, `models`, etc.).
- Primer endpoint: "Hello, World!".
- El concepte de "path operation".

## **[4. Maneig de dades amb Pydantic](doc/04_maneig_dades_pydantic.md)**
- Introducció a Pydantic i models de dades.
- Validació de dades d'entrada i sortida.
- Tipus de dades bàsics i avançats
- Errors comuns i com manejar-los.

## **[5. Herència en models Pydantic](doc/05_herencia_models_pydantic.md)**
- Herència bàsica.
- Sobreescriptura de camps.
- Validacions específiques.
- Jerarquies complexes.
- Validació automàtica.

## **[6. Rutes i mètodes HTTP](doc/06_rutes_metodes_http.md)**
- Definició de rutes (`GET`, `POST`, `PUT`, `DELETE`).
- Paràmetres en rutes i cadenes de consulta.
- Introducció a les respostes amb codi d'estat HTTP.

## **[7. Bases de dades amb FastAPI](doc/07_bases_dades_fastapi.md)**
- Què és SQLAlchemy?
- Configuració de l'app.
- CRUD bàsic (Create, Read, Update, Delete).
- Treballar amb Postgres.

## **[8. Autenticació i Autoria](doc_/08_autenticacio_autoria.md)**
- Introducció a JWT (JSON Web Tokens)
- Implementació d'autenticació d'usuaris.
- Rol d'usuari i permisos.

## **9. Documentació automàtica amb Swagger**
- Exploració de la documentació interactiva automàtica.
- Personalització bàsica de la documentació.

## **10. Testing d'una API**
- Configuració de proves amb `pytest`.
- Proves d'integració d'endpoints.
- Bones pràctiques en el testing d'APIs.

## **11. Deploy d'una aplicació FastAPI**
- Opcions de desplegament:
    - Docker.
    - Plataformes de desplegament com Heroku o Railway.
- Creació d'un contenidor Docker bàsic per FastAPI.
- Consideracions de seguretat.
