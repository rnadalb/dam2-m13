# FastAPI

D'acord amb la seva pàgina oficial **_[FastAPI](https://fastapi.tiangolo.com/es/)_** és un marc de treball (_framework_), alt rendiment, fàcil d'aprendre, ràpid de programar i llest per a producció.

Al llarg del curs aprendrem, a poc a poc i de menys a més complexitat les capacitats necessàries per desenvolupar un projecte complex.
Per a fer-ho l'equip docent del mòdul de projecte ha elaborat els següents materials.

### [1. Introducció a FastAPI](doc/01-fastapi.md)
- Què és FastAPI i per què utilitzar-ho?
- Avantatges sobre altres frameworks web
- Instal·lació i configuració de l'entorn de desenvolupament

### [2. Fonaments de FastAPI](doc/02-fastapi.md)
- Estructura bàsica d'una aplicació FastAPI
- Creació del teu primer endpoint
- Principis d'una API i API RESTful

### [3. Rutes i endpoints](doc/03-fastapi.md)
- Definició de rutes bàsiques
- Mètodes HTTP (GET, POST, PUT, DELETE)
- Gestió de paràmetres de ruta i query
- Activitat: Operacions CRUD bàsiques

### [4. Models i serialització](doc/04-fastapi.md)
- Introducció als Pydantic Models
- Validació de dades d'entrada
- Utilització de models per a la serialització de respostes

### [5. Dependències](doc/05-fastapi.md)
- Concepte de dependència
- Tipus de dependències

### 6. Seguretat i autenticació
- Autenticació bàsica
- Autenticació amb tokens (OAuth2)
- Dependències de seguretat

### 7. Bases de dades
- Integració amb bases de dades SQL i NoSQL
- ORM amb SQLAlchemy
- Asincronia amb bases de dades

### 8. Middleware, CORS i events
- Què és un Middleware?
- Gestió de CORS
- Events de startup i shutdown

### 9. Errors i maneig d'excepcions
- Maneig d'errors estàndard
- Creació de manejadors d'excepcions personalitzats
- Validacions i errors HTTP personalitzats

### 10. Tests
- Estratègies per a testear aplicacions FastAPI
- Creació de tests amb Pytest
- Mocking i fixtures

### 11. Desplegament
- Opcions de desplegament (Heroku, AWS, Azure, Docker)
- Configuració de serveis de producció
- Monitoreig i escalabilitat

### 12. Bones Pràctiques i Consells
- Organització del codi en aplicacions grans
- Millora del rendiment de l'API
- Documentació amb Swagger UI i ReDoc

### 13. Recursos Addicionals i Comunitat
- Llibreries útils per ampliar FastAPI
- Fòrums i grups de discussió
- Contribuir al projecte FastAPI

### [14. Apèndix](doc/14-fastapi.md)
- Glossari de termes
- Respostes als exercicis pràctics
- Referències i lectures addicionals


