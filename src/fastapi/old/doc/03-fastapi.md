# 3. Rutes i endpoints
[[_TOC_]]

En FastAPI, les **rutes** (*path*) són fonamentals per definir la lògica del negoci que es posarà a disposició dels usuaris a través d'una API. Un **endpoint** és bàsicament una ruta amb una funció associada que es crida quan es fa una petició HTTP a aquesta ruta.

## Definició de rutes bàsiques

Per definir una ruta en FastAPI, utilitzem decoradors de funció que associen un camí d'URL amb una funció de Python. El decorador més senzill és `@app.get()`, que associa una funció amb una petició `GET` a una URL específica.

### Exemple d'un endpoint GET

```python
@app.get("/items/")
async def read_items():
    return [{"name": "Item 1"}, {"name": "Item 2"}]
```

En aquest exemple, quan el client fa una petició `GET` a `/items/`, la funció `read_items` es cridada i retorna una llista d'elements.

## Rutes amb paràmetres

Pots definir rutes que capturin paràmetres des de l'URL usant la sintaxi de corxets `{ }` dins del camí de la ruta.

### Exemple

```python
@app.get("/items/{item_id}")
async def read_item(item_id: int):
    return {"item_id": item_id, "name": f"Item {item_id}"}
```

Aquesta ruta **captura** `item_id` com a paràmetre i el retorna juntament amb un nom d'element fictici.

### Tipus de paràmetres

FastAPI ofereix flexibilitat per definir diferents tipus de paràmetres per a les rutes de la teva API.

#### Paràmetres de camí

Els paràmetres de camí són parts variables de la URL que s'utilitzen per capturar valors específics enviats en la ruta.

##### Exemple

```python
@app.get("/users/{user_id}")
async def read_user(user_id: int):
    return {"user_id": user_id, "username": f"user{user_id}"}
```

`user_id`: és un paràmetre de camí que es captura i s'utilitza dins de la funció.

#### Paràmetres de consulta

Són paràmetres opcionals o obligatoris que s'envien en l'URL de la petició, normalment utilitzats per a filtrar o modificar la resposta.

##### Exemple

```python
@app.get("/items/")
async def read_items(skip: int = 0, limit: int = 10):
    return fake_items_db[skip : skip + limit]
```

`skip` i `limit`: són paràmetres de consulta que controlen quina part de la base de dades d'elements es retorna.

#### Paràmetres de cos

Els paràmetres de cos són dades enviades a través del cos de la petició, sovint en format **JSON**, i s'utilitzen principalment amb mètodes HTTP com POST, PUT i DELETE.

##### Exemple

```python
from pydantic import BaseModel

class Item(BaseModel):
    name: str
    description: str = None
    price: float
    tax: float = None

@app.post("/items/")
async def create_item(item: Item):
    fake_items_db.append(item.model_dump())
    return item
```

`item`: és un paràmetre de cos que representa un element nou per afegir a la base de dades.
Ja veurem més endavant que és això de **_pydantic_**.

#### Paràmetres de capçalera

Els paràmetres de capçalera són valors enviats a través de les capçaleres HTTP d'una petició i poden ser utilitzats per a passar informació addicional que no es vol enviar a través de l'URL.

##### Exemple

```python
@app.get("/items/")
async def read_items(user_agent: str = Header(None)):
    return {"User-Agent": user_agent}
```

`User-Agent`: de la capçalera HTTP d'una petició.

## Rutes avançades i operacions CRUD

En una API, normalment necessitaràs efectuar operacions CRUD. Això implica crear rutes per a cada una de les operacions: crear, llegir, actualitzar i eliminar.

### Exemple

```python
# Create
@app.post("/items/")
async def create_item(item: Item):
    fake_items_db.append(item)
    return item

# Read 
@app.get("/items/{item_id}")
async def read_item(item_id: int):
    item = fake_items_db[item_id]
    return item

# Update
@app.put("/items/{item_id}")
async def update_item(item_id: int, item: Item):
    fake_items_db[item_id] = item
    return item

#Delete
@app.delete("/items/{item_id}")
async def delete_item(item_id: int):
    del fake_items_db[item_id]
    return {"msg": "Item deleted"}
```

Com pots veure a l'exemple anterior definim rutes per a cada operació CRUD, utilitzant diferents mètodes HTTP i tipus de paràmetres.

### Respostes i estats HTTP

És important retornar el codi d'estat HTTP correcte en les respostes. FastAPI facilita això amb els paràmetres `status_code` dels decoradors de ruta i l'objecte `Response`.

#### Exemple

```python
from fastapi import Response, status

@app.post("/items/", status_code=status.HTTP_201_CREATED)
async def create_item(item: Item):
    fake_items_db.append(item)
    return item
```

En aquest exemple, si l'element és creat correctament, es retorna el codi d'estat 201, que indica que la petició ha tingut èxit i s'ha creat un nou recurs.

#### Codis d'estat HTTP més comuns

* `200 OK`: la petició ha tingut èxit. El significat exacte depèn del mètode HTTP utilitzat. En una petició `GET`, el recurs s'ha recuperat i s'envia en el cos del missatge.
* `201 Created`: la petició ha tingut èxit i com a resultat s'ha creat un nou recurs. La nova informació del recurs es troba normalment en el cos de la resposta i la URI del recurs creat en l'encapçalament `Location`.
* `202 Accepted`: la petició ha estat acceptada per a processament, però encara no s'ha completat. Aquesta resposta es reserva per a processos que triguen temps com les tasques asincròniques.
* `204 No Content`: la petició ha tingut èxit però el servidor no retorna cap contingut. Això és habitual en una petició DELETE, on el recurs es suprimeix amb èxit.
* `400 Bad Request`: el servidor no pot o no processarà la petició a causa d'un error aparent de client (p.ex., format de sol·licitud incorrecte).
* `401 Unauthorized`: semblant a 403 Forbidden, però específicament per a l'ús quan l'autenticació és requerida i ha fallat o encara no ha estat proporcionada.
* `403 Forbidden`: el client no té els drets d'accés al contingut; és a dir, és no autoritzat, així que el servidor està rebutjant donar la resposta adequada.
* `404 Not Found`: el servidor no ha trobat cap recurs que coincideixi amb l'URI de la sol·licitud. Sense indicar si la condició és temporal o permanent.
* `405 Method Not Allowed`: el mètode de sol·licitud està conegut pel servidor però ha estat deshabilitat i no pot ser utilitzat.
* `422 Unprocessable Entity`: la sol·licitud està ben formada però no es va poder seguir a causa d'errors semàntics.
* `500 Internal Server Error`: un missatge genèric d'error, donat quan una condició inesperada va ser trobada i cap missatge específic és adequat.
* `502 Bad Gateway`: el servidor estava actuant com a passarel·la o proxy i va rebre una resposta invàlida des del servidor amunt.
* `503 Service Unavailable`**: el servidor no està preparat per manejar la petició. Comúment causat per un servei desconnectat per manteniment o que està sobrecarregat.

# Activitat - Operacions CRUD Bàsiques

**Objectius:**
1. Crear endpoints d'API per manejar dades d'usuaris.
2. Entendre i implementar operacions CRUD.
3. Familiaritzar-se amb el testing d'APIs utilitzant eines com Postman o el navegador.

**Model de dades `Persona`:**
- `id`: Un identificador únic per a cada persona.
- `nom`: El nom de la persona.
- `email`: El correu electrònic de la persona, que ha de ser únic.
- `edat`: L'edat de la persona.
- `data_alta`: La data i hora en què la persona va ser afegida al sistema.
- `esAdmin`: Un booleà que indica si la persona té privilegis d'administrador.

**Endpoints d'API:**
- `POST /persones/`: Crea una nova entrada de persona.
- `GET /persones/`: Retorna una llista de totes les persones.
- `GET /persones/{persona_id}`: Obté una persona específica per ID.
- `PUT /persones/{persona_id}`: Actualitza les dades d'una persona existent.
- `DELETE /persones/{persona_id}`: Elimina una persona del sistema.

Consulta el codi de la [solució](../../exemples/fa-basic-crud/main.py).