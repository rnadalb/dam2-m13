# Apèndix

## Glossari de Termes
[[_TOC_]]

### 1. Introducció a FastAPI

#### FastAPI
Un framework modern i de ràpid rendiment per a construir APIs amb Python 3.7+, basat en tipus estàndard de Python. Està dissenyat per ser fàcil d'utilitzar i molt ràpid. [Més informació](https://fastapi.tiangolo.com/).

#### Framework
Un conjunt de components de programari preconstruïts que proporciona una base sobre la qual desenvolupar software, facilitant tasques comunes com la gestió de rutes o la validació de dades. [Més informació](https://en.wikipedia.org/wiki/Software_framework).

#### API (Application Programming Interface)
Una interfície que permet a diferents programes informàtics comunicar-se entre ells. En el context de FastAPI, normalment es refereix a una interfície web que permet a les aplicacions front-end comunicar-se amb una aplicació back-end. [Més informació](https://en.wikipedia.org/wiki/API).

#### RESTful API
Una API que segueix els principis de l'arquitectura REST (Representational State Transfer), utilitzant mètodes HTTP estàndard com GET, POST, PUT i DELETE. [Més informació](https://en.wikipedia.org/wiki/Representational_state_transfer).

#### Endpoint
Un punt final d'una API; una URL on es poden realitzar peticions per obtenir o enviar dades. [Més informació](https://en.wikipedia.org/wiki/Endpoint_interface).

#### HTTP (Hypertext Transfer Protocol)
El protocol utilitzat per a la transferència de dades a la web. Inclou mètodes com GET per a recuperar dades, POST per a enviar dades, entre altres. [Més informació](https://en.wikipedia.org/wiki/Hypertext_Transfer_Protocol).

#### JSON (JavaScript Object Notation)
Un format lleuger d'intercanvi de dades fàcil de llegir i escriure per a humans i fàcil de parsejar i generar per a màquines. [Més informació](https://www.json.org/json-en.html).

#### Query Parameters
Paràmetres que es poden incloure en l'URL d'una petició HTTP per especificar informació addicional per a la petició. [Més informació](https://en.wikipedia.org/wiki/Query_string).

#### Uvicorn
Un servidor ASGI lleuger i de ràpid rendiment per a Python 3, capaç de fer funcionar aplicacions FastAPI. [Més informació](https://www.uvicorn.org/).

#### ASGI (Asynchronous Server Gateway Interface)
Una especificació per a servidors web Python que serveix com a interfície entre aplicacions web Python asincròniques i els servidors. [Més informació](https://asgi.readthedocs.io/en/latest/).

### 2. Fonaments de FastAPI

#### REST (Representational State Transfer)
Un conjunt de restriccions arquitectòniques utilitzat per a la creació de serveis web. Les APIs que segueixen aquestes restriccions es diuen RESTful. [Més informació](https://en.wikipedia.org/wiki/Representational_state_transfer).

#### Stateless
En el context de les APIs RESTful, significa que cada petició del client al servidor ha de contenir tota la informació necessària per comprendre i respondre a la petició, sense necessitat de retenir estats de sessions entre peticions. [Més informació](https://en.wikipedia.org/wiki/Stateless_protocol).

#### Cacheable
Una propietat d'un sistema que permet emmagatzemar dades per reutilitzar-les en peticions futures, millorant així la velocitat i eficiència en el processament de les mateixes dades. [Més informació](https://en.wikipedia.org/wiki/Web_cache).

#### Uniform Interface
Un dels principis clau en l'arquitectura REST que indica que les interfícies per a la comunicació entre client i servidor han de ser uniformes i consistents. [Més informació](https://restfulapi.net/resource-naming/).

#### CRUD (Create, Read, Update, Delete)
Un acrònim per referir-se a les quatre operacions bàsiques realitzades en bases de dades persistents o que defineixen les operacions bàsiques en APIs RESTful. [Més informació](https://en.wikipedia.org/wiki/Create,_read,_update_and_delete).

#### Escalabilitat
La capacitat d'un sistema per manejar una quantitat creixent de treball o la seva capacitat d'augmentar recursos per acomodar aquest creixement. [Més informació](https://en.wikipedia.org/wiki/Scalability).

#### Portabilitat
En el context del desenvolupament de software, es refereix a la facilitat amb la qual el software pot ser traslladat d'un entorn de computació a un altre. [Més informació](https://en.wikipedia.org/wiki/Software_portability).

### Session
Una forma d'emmagatzemar informació (en variables) per ser utilitzada en múltiples pàgines. Contràriament a una cookie, la informació no es guarda en l'ordinador dels usuaris. [Més informació](https://en.wikipedia.org/wiki/Session_(computer_science)).

#### Token
En seguretat informàtica, és una peça d'informació que serveix per verificar l'identitat d'un usuari, dispositiu o qualsevol altra entitat en una transacció electrònica. [Més informació](https://en.wikipedia.org/wiki/Access_token).

### 3. Rutes i Endpoints

### Ruta
Una URL específica que pot ser invocada en una aplicació web. En FastAPI, es defineix mitjançant funcions de Python associades a operacions HTTP.
[Més informació](https://fastapi.tiangolo.com/tutorial/first-steps/#path-operation-decorator).

### Endpoint
La funció associada a una ruta que es crida amb una petició HTTP. Cada endpoint pot tenir la seva lògica per processar peticions i retornar respostes.
[Més informació](https://fastapi.tiangolo.com/tutorial/first-steps/#path-operation-function).

### Operacions CRUD
Les quatre operacions bàsiques de persistència de dades: Crear (Create), Llegir (Read), Actualitzar (Update), i Eliminar (Delete).
[Més informació](https://fastapi.tiangolo.com/tutorial/sql-databases/#crud).

### Paràmetre de camí
Un segment de la URL que s'utilitza per capturar valors específics passats a la ruta, com `/items/{item_id}`.
[Més informació](https://fastapi.tiangolo.com/tutorial/path-params/).

### Paràmetres de consulta
Opcions afegides a la URL per personalitzar la resposta, com ara filtrar resultats o paginació.
[Més informació](https://fastapi.tiangolo.com/tutorial/query-params/).

### Paràmetres de cos
Dades enviades en el cos d'una petició HTTP, normalment com un objecte JSON, especialment en peticions `POST` i `PUT`.
[Més informació](https://fastapi.tiangolo.com/tutorial/body/).

### Paràmetres de capçalera
Valors enviats en les capçaleres HTTP de la petició, que poden incloure metadades com l'autenticació o el tipus de contingut.
[Més informació](https://fastapi.tiangolo.com/tutorial/header-params/).

### Model pydantic
En FastAPI, els models de Pydantic defineixen l'estructura de dades per a paràmetres de cos i s'utilitzen per la validació i serialització de dades.
[Més informació](https://fastapi.tiangolo.com/tutorial/body/#pydantic-models).

### Validació de dades
Verificació que les dades d'entrada compleixen amb els criteris i restriccions establerts abans de ser processades.
[Més informació](https://fastapi.tiangolo.com/tutorial/body/#request-body).

### Serialització
Convertir objectes de dades complexos en formats més simples com JSON per a la seva transmissió a través de la xarxa.
[Més informació](https://fastapi.tiangolo.com/tutorial/encoder/).

### Deserialització
El procés de convertir dades estructurades en formats com JSON a objectes de dades complexos en Python.
[Més informació](https://fastapi.tiangolo.com/tutorial/body/#deserialize-the-data).

---