# 4. Models i serialització

[[_TOC_]]

## 4.1. Què són els models Pydantic?

Els models [Pydantic](https://docs.pydantic.dev/latest/) són classes de Python que s'utilitzen per definir l'estructura de les dades d'entrada i sortida en una aplicació FastAPI. Aquests models faciliten la validació automàtica de dades i la conversió entre formats complexos i JSON per a la transmissió a través de la xarxa.

```python
from pydantic import BaseModel

class Persona(BaseModel):
    nom: str
    email: str
    edat: int
    esAdmin: bool = False
```

## 4.2. Validació automàtica

Quan s'utilitza un model Pydantic en un endpoint, FastAPI automàticament valida les dades d'entrada contra el model. Si les dades no s'ajusten al model, FastAPI retorna un error de validació sense cridar l'endpoint.

```python
from fastapi import FastAPI, HTTPException
from models import Persona

app = FastAPI()

@app.post("/persona/")
def create_persona(persona: Persona):
    # Si `persona` no és vàlid segons el model Persona, aquest codi no s'executarà.
    return {"nom": persona.nom, "email": persona.email}
```

## 4.3. Serialització i deserialització

Els models Pydantic també s'encarreguen de la serialització i deserialització de dades. Quan es retorna un model Pydantic en un endpoint, es converteix automàticament a JSON. Inversament, quan s'envien dades JSON a un endpoint que espera un model Pydantic, es deserialitzen en una instància del model.

```python
@app.get("/persona/{persona_id}")
def get_persona(persona_id: int):
    # Simulació de cerca en base de dades.
    fake_db_response = {"nom": "Anna", "email": "anna@example.com", "edat": 25, "esAdmin": False}
    persona = Persona(**fake_db_response)
    return persona
```

## 4.4. Avançat: models anidats i llistes

Els models Pydantic poden incloure altres models o llistes de models, permetent estructurar dades relacionades de manera complexa.

```python
from typing import List

class Item(BaseModel):
    nom: str
    descripcio: str = None

class Inventari(BaseModel):
    propietari: str
    items: List[Item]

@app.post("/inventari/")
def create_inventari(inventari: Inventari):
    # Validació i serialització automàtiques per inventari i els seus items.
    return inventari
```

## 4.5. Tipus de dades personalitzats

Pydantic permet definir tipus de dades personalitzats, útils per validar formats específics com adreces d'email, URLs, etc.

```python
from pydantic import EmailStr

class Persona(BaseModel):
    nom: str
    email: EmailStr
```

## 4.6. Creació de validacions pròpies

Mentre que Pydantic ofereix una àmplia gamma de validacions automàtiques, pot ser necessari definir validacions personalitzades per a certs casos d'ús. Això es pot aconseguir a través de decoradors i mètodes especials dins dels models Pydantic.

### Exemple

Suposem que volem assegurar que l'`email` d'una `Persona` sempre sigui d'un domini específic. Podem crear un mètode de validació personalitzat dins de la nostra classe `Persona`:

```python
from pydantic import BaseModel, field_validator

class Persona(BaseModel):
    nom: str
    email: str
    @field_validator('email')
    def email_de_domini_especific(cls, v):
        if not v.endswith('@exemple.com'):
            raise ValueError('Has de proporcionar una adreça de correu de @exemple.com')
        return v
    edat: int
    esAdmin: bool = False    
```

El decorador `@field_validator` s'utilitza per especificar un mètode que s'executarà durant la validació del camp `email`. Si l'`email` no finalitza amb `@exemple.com`, es llança una `ValueError`, la qual cosa fa que la validació fracassi i es retorni un missatge d'error al client.

### Avantatges de validacions personalitzades

- **Control**: Ofereixen control més granular sobre la lògica de validació.
- **Negocis específics**: Permeten implementar regles de negoci específiques que no estan cobertes per les validacions estàndard.
- **Reutilització**: Una vegada definides, es poden reutilitzar a través de diversos models o projectes.

### Consideracions

- **Complexitat**: Les validacions personalitzades poden incrementar la complexitat del model.
- **Rendiment**: Com més complexa sigui la validació, més pot afectar al rendiment.
- **Manteniment**: Canvis en les regles de negoci poden requerir actualitzacions en les validacions personalitzades.

Crear validacions personalitzades pot ser extremadament útil quan es tracta d'assegurar que les dades compleixen amb requisits específics que no són coberts per les validacions per defecte de Pydantic.

# Activitat: implementació d'un CRUD bàsic amb validacions

### Objectius

1. Definir models de dades amb Pydantic.
2. Implementar operacions **CRUD** (_Create_, _Read_, _Update_, _Delete_).
3. Realitzar validacions.
4. Documentar les vostres rutes API amb docstrings.

### Descripció
Heu de construir una API utilitzant FastAPI que gestioni docents. Cada **docent** és una **persona**, però amb l'atribut addicional `centre_educatiu` que indica on treballen.

### Criteris d'acceptació
- Utilitzar la llibreria FastAPI.
- Definir una classe `Persona` amb els següents camps:
    - `id`: Un identificador únic per a la persona.
    - `nom`: El nom de la persona.
    - `email`: El correu electrònic de la persona.
    - `edat`: L'edat de la persona (pot ser opcional).
- Definir una classe `Docent` que hereti de `Persona` i afegeixi el camp `centre_educatiu` per indicar on treballa.
- Cal **assegurar** que:
  - `id` és únic.
  - `email` pertany **exclusivament** al domini **@ies-eugeni.cat**.
  - `edat` es major que 0.
- Implementar rutes API per a les operacions CRUD.
- Documentar totes les funcions amb docstrings.

### Diagrama de classes

```mermaid
classDiagram
    class Persona {
      +int id
      +str nom
      +str email
      +int edat?
    }

    class Docent {
      +str centre_educatiu
    }

    Persona <|-- Docent: Inherits
```

Enllaç a la proposta de solució fent clic [aquí](../../exemples/fa-pydantic-demo)