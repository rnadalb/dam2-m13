# 5. Dependències en FastAPI
[[_TOC_]]

Les [dependències en FastAPI](https://fastapi.tiangolo.com/es/features/?h=dependencias#dependency-injection) són una manera potent per a reutilitzar codi, injectar lògica predefinida i compartir dades entre diferents rutes i operacions de l'API. FastAPI permet definir dependències a nivell de funcions i injeccions de dependències per a reutilitzar codi de manera eficient i modular.

## 5.1 Concepte de dependència

Una dependència en FastAPI és essencialment una funció que s'executa abans de la funció principal d'una ruta. Pot ser utilitzada per a realitzar tasques com:

- Autenticació i autorització
- Connexió a bases de dades
- Càrrega i validació de dades de sol·licitud
- Configuració de contextos de petició

La manera d'incloure aquestes dependències és a través del mètode `Depends` en els teus **_endpoints_**:

```python
app = FastAPI()

def get_hello() -> str:
    return 'hello'

@app.route('/hello_world')
def hello_world(d: str = Depends(get_hello)):
    return d + ' world'
```

És important saber que aquestes dependències ens serveixen per a executar codi **abans** que la petició arribi als nostres **_endpoints_**.

## 5.2 Tipus de dependències

#### Dependències funció

Les dependències més comunes són utilitzades a través de funcions. El sistema d'injecció de dependències permet que puguem fer servir els resultats d'aquestes funcions en els nostres **_endpoints_**. Aquestes dependències són útils per aplicar el principi "_**Don’t Repeat Yourself**_" (**_DRY_**) ja que faciliten la reutilització de manera senzilla entre els diferents punts d'entrada.

**Exemple:**

***1. Definim la dependència***

```python
from fastapi import Depends, HTTPException, status

def get_current_user(token: str):
    # Aquí hauríem de validar el token i obtenir l'usuari    
    if token != "token_molt_pero_molt_secret":
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="No autoritzat"
        )
    return "Usuari_autenticat"  # En un cas real, retornaríem un objecte d'usuari
```
***2. Utilitzem la dependència***

```python
from fastapi import FastAPI, Depends

app = FastAPI()

@app.get("/items-privats/")
def read_items_privats(current_user: str = Depends(get_current_user)):
    # La lògica per obtenir els elements només si l'usuari està autenticat
    return {"items": ["item1", "item2"], "usuari": current_user}

```

A l'exemple anterior, quan un client fa una petició a `/items-privats/`, FastAPI executa automàticament la funció `get_current_user` abans de la funció de la ruta `read_items_privats`. Si el token no és vàlid, la petició fallarà amb una excepció HTTP 401.

#### Classes com dependències

Podem fer que tot allò que sigui "cridable" (**_callable_**) sigui una dependència. Aquest sistema no només permet utilitzar funcions, sinó que també permet injectar instàncies de classes gestionades pel mateix sistema.

**Exemple:**

Suposem que volem crear una dependència que gestiona l'accés a una base de dades. Podem encapsular la lògica de connexió i tancament de la base de dades dins d'una classe.

```python
class DBManager:
    def __init__(self):
        self.db = None

    def connect(self):
        self.db = connect_to_db()  # Funció fictícia per connectar-se a la base de dades
        return self.db

    def close(self):
        if self.db:
            self.db.close()

    def __call__(self):
        db = self.connect()
        try:
            yield db
        finally:
            self.close()

```
Aquesta classe `DBManager` gestiona la connexió i tancament de la base de dades. El mètode `__call__` la converteix en una dependència que es pot utilitzar en rutes FastAPI.

Per utilitzar-la podríem codificar: 

```python
@app.get("/items/")
def read_items(db = Depends(DBManager())):
    # Utilitza 'db' per a realitzar operacions de base de dades
    pass
```
#### Dependències anidades

Les dependències anidades a FastAPI permeten construir una cadena de dependències, on una dependència pot depèn d'una altra. Això és particularment útil quan una sèrie de passos o comprovacions han de ser realitzades en un cert ordre.

**Exemple:**

Imaginem que tenim una aplicació on volem realitzar dues tasques abans d'executar una funció de ruta: primer, validar un token d'usuari, i després, basant-nos en l'usuari autenticat, obtenir les seves preferències de l'aplicació.


***1. Creem la primera dependència (validació de l'usuari)***
```python
from fastapi import Depends, HTTPException, status

def get_current_user(token: str):
    if token != "token_valid":
        raise HTTPException(status_code=status.HTTP_401_UNAUTHORIZED)
    return {"username": "usuari_valid"}

```
***2. Creem la primera dependència (obtenció de les preferències de l'usuari)***
```python
def get_user_preferences(current_user: dict = Depends(get_current_user)):
    username = current_user.get("username")
    # Aquí es podria obtenir les preferències de l'usuari de la base de dades
    return {"theme": "fosc", "notifications": "enabled"}
```

***3. Utilitzem les dependències anidades***
```python
@app.get("/user/preferences/")
def user_preferences(preferences: dict = Depends(get_user_preferences)):
    return preferences
```

🚨 **ALERTA**: FastAPI permet reutilitzar el valor de les diferents dependències (com un Singleton) utilitzant el paràmetre **_cache_**, `Depends(cache=True)`, del mètode `Depends`. Aquest mètode evita que es cridi cada vegada que es necessita la dependència.

#### Dependències a les rutes

Les dependències en les rutes de FastAPI són una manera eficient de reutilitzar codi i lògica comuna, especialment útil per a tasques com l'autenticació, la connexió a bases de dades, i la gestió de configuracions.

**Exemple**

A l'exemple podreu observar una dependència que comprova l'autenticació d'un usuari abans de permetre accés a una ruta específica.

***1. Creació de la funció de dependència***

La funció validarà un token fictici per simplificar l'exemple:

```python
from fastapi import Depends, HTTPException, status

def get_current_user(token: str):
    # Aquí es podria validar el token real
    if token != "token_secret":
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Token no vàlid o absent"
        )
    return {"usuari_id": "123", "nom": "Usuari Test"}
```

**2. Utilització de la dependència**

```python
from fastapi import FastAPI, Depends

app = FastAPI()

@app.get("/perfil-usuari/")
def perfil_usuari(current_user: dict = Depends(get_current_user)):
    # La lògica de la ruta aquí, com ara recuperar la informació de l'usuari
    return {"nom": current_user["nom"], "usuari_id": current_user["usuari_id"]}
```

Les dependències globals en FastAPI són una forma d'aplicar una dependència a múltiples rutes o a tota l'aplicació, sense necessitat de repetir la dependència en cada funció de ruta. Això és útil per a funcionalitats comunes com la autenticació, la comprovació de permisos, la connexió a bases de dades, o la configuració global. Implementar una dependència global assegura que certa lògica s'executa de manera consistent en totes les rutes on s'aplica.

#### Dependències globals

Una dependència global es pot definir a través de l'ús de `dependencies` en la creació de l'objecte `FastAPI` o en un `APIRouter`. Aquesta dependència s'executarà per a cada sol·licitud a les rutes associades.

***Exemple***

Suposem que volem que totes les nostres rutes verifiquin un token d'usuari.

**1. Creació de la dependència**

Aquesta funció verificarà un token d'usuari:

```python
def verify_token(token: str = Header(...)):
    if token != "token_secret":
        raise HTTPException(status_code=401, detail="Token invàlid")    
    return token
```

**2. Configuració de la dependència global**

```python
app = FastAPI(dependencies=[Depends(verify_token)])
```

**3. Utilització de la dependència global**

**Totes** les rutes definides en aquesta instància de `FastAPI` utilitzaran la dependència `verify_token`:

```python
@app.get("/item/")
def read_item():
    return {"item": "Algún contingut"}

@app.get("/altra-ruta/")
def another_route():
    return {"ruta": "Un altre contingut"}
```

#### Dependències amb generadors
Les dependències amb generadors ([yield](https://fastapi.tiangolo.com/tutorial/dependencies/dependencies-with-yield/)) a FastAPI són una tècnica avançada que permet gestionar recursos que necessiten una inicialització i una finalització, com ara connexions a bases de dades o fitxers. Utilitzant un generador, pots assegurar-te que es realitzen accions tant abans com després d'una petició HTTP.

Aquesta funcionalitat és especialment útil per gestionar recursos de manera eficient, assegurant que s'obrin i tanquin correctament, independentment de si la petició es compleix amb èxit o no.

**Exemple**

Suposem que tenim una aplicació que necessita connectar-se a una base de dades en cada petició i tancar la connexió un cop finalitzada la petició, siguin quines siguin les circumstàncies.

**1. Definició de la funció de dependència**

Aquesta funció de dependència gestiona la connexió a la base de dades:

```python
from contextlib import contextmanager
from fastapi import Depends, HTTPException

@contextmanager
def get_db_connection():
    db = connect_to_db()  # Aquesta funció és fictícia
    try:
        yield db
    except Exception:
        db.rollback()
    finally:
        db.close()

def get_db():
    with get_db_connection() as db:
        yield db
```

**2.: Utilització de la funció**

```python
from fastapi import FastAPI, Depends

app = FastAPI()

@app.get("/items/")
def read_items(db = Depends(get_db)):
    # Aquí es realitzen operacions amb la base de dades
    pass
```

