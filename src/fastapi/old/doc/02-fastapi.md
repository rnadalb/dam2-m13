# 2. Fonaments de FastAPI

[[_TOC_]]

## Estructura bàsica d'una aplicació FastAPI

Una aplicació FastAPI comença amb la creació d'una instància de l'objecte `FastAPI`, que serveix com a punt central per a totes les rutes i configuracions de l'API.

```python
from fastapi import FastAPI

app = FastAPI()
```

Aquesta instància pot ser utilitzada per a definir rutes d'API utilitzant decoradors de Python, que vinculen funcions de Python a rutes HTTP específiques.

## Creació del teu primer endpoint

Un **endpoint** és un punt final de l'API que respon a una petició HTTP específica. A continuació es mostra com es pot crear un endpoint senzill que respon a peticions GET a l'arrel (`/`) de l'API.

```python
@app.get("/")
async def read_root():
    return {"message": "Benvingut a FastAPI"}
```

## Principis d'una API i API RESTful

Les APIs (Application Programming Interfaces) són conjunts de definicions i protocols que permeten la comunicació i interacció entre diferents programes de software. En el context web, una API permet que els clients (com navegadors o aplicacions mòbils) interactuin amb un servidor per sol·licitar dades o accions.

Les APIs RESTful segueixen un conjunt de principis de disseny que s'adhereixen a l'estil d'arquitectura REST (Representational State Transfer). Aquest estil s'orienta al recurs i fa ús dels mètodes HTTP estàndard de manera coherent.

### Exemple d'un endpoint RESTful

```python
@app.get("/users/{user_id}")
async def read_user(user_id: int):
    return {"user_id": user_id, "name": "Nom de l'usuari"}
```

Aquesta funció crea un **endpoint** que respon a peticions `GET`. Utilitza una ruta parametritzada on `user_id` és una variable que representa l'identificador d'un usuari fictici.

### Què fa que una API sigui RESTful?

- **Client-Servidor**: Una separació clara entre el client i el servidor.
- **Stateless**: Cada petició ha de contenir tota la informació necessària per a ser entesa.
- **Cacheable**: Les respostes han de ser explícitament marcades com a cacheables o no.
- **Interface uniforme**: Utilització consistent de rutes i representacions de recursos.

### Avantatges de les API RESTful

- **Escala i rendiment**: La naturalesa estatal·less i la possibilitat de fer cache de les respostes millora l'escalabilitat i el rendiment.
- **Flexibilitat i portabilitat**: Al separar el client i el servidor, i comunicar-se a través de representacions estàndard de dades (com JSON), es fa més fàcil el desenvolupament de clients en diferents plataformes.
- **Independència**: Les modificacions en el backend no afecten directament al client sempre que les interfícies es mantinguin consistents.
- **Facilitat d'enteniment i ús**: Utilitzar mètodes HTTP estàndard fa que els desenvolupadors puguin entendre i utilitzar l'API més fàcilment.

### Desavantatges de les API RESTful

- **Estatal·less pot ser un repte**: Mantenir l'estat entre peticions pot ser difícil sense utilitzar sessions o tokens, que poden complicar l'arquitectura.
- **Menys flexibilitat en el maneig de peticions**: L'ús de mètodes HTTP estàndard pot limitar les operacions a les funcions bàsiques com `CREATE`, `READ`, `UPDATE` i `DELETE` ([CRUD](https://en.wikipedia.org/wiki/Create,_read,_update_and_delete)), que poden no ser suficients per aplicacions més complexes.
- **Necessitat de documentació**: Tot i que REST és intuïtiu, sense una documentació adequada, els usuaris poden no entendre completament les funcions o els requisits de les dades.