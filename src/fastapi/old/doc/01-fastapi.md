# 1. Introducció a FastAPI

[[_TOC_]]

FastAPI és un framework web modern i de ràpid rendiment per a construir APIs amb Python 3.7+, basat en anotacions de
tipus estàndard de Python. La seva ràpida adopció es deu a la seva facilitat d'ús, rendiment i capacitat d'escalar per a
aplicacions empresarials.

## Què és FastAPI i per què utilitzar-ho?

FastAPI és dissenyat per ser fàcil d'utilitzar, mentre que permet als desenvolupadors crear aplicacions robustes i
eficients. El framework fomenta la creació de codi amb anotacions de tipus, això no només millora la velocitat de
desenvolupament sinó que també ofereix una millor experiència al programador mitjançant autocompletat i menys errors en
temps d'execució.

### Característiques Clau:

- **Ràpid**: És un dels frameworks de Python més ràpids disponibles.
- **Ràpid per codificar**: Augmenta la velocitat de desenvolupament de funcionalitats gràcies a l'autocompletat.
- **Menys errors**: Redueix els errors amb anotacions de tipus.
- **Intuïtiu**: Té una gran intuïció i és fàcil d'aprendre, sobretot per a desenvolupadors que estan familiaritzats amb
  Python.
- **Standards-based**: Construït sobre i per a standards com JSON Schema, OAuth 2.0, entre d'altres.

## Avantatges sobre altres frameworks web

Comparat amb altres frameworks com Flask o Django, FastAPI ofereix una major velocitat de processament, suport natu per
a la programació asincrònica i una millora en l'experiència de desenvolupament gràcies a l'ús extensiu de les anotacions
de tipus de Python.

### Comparació Ràpida:

- **Flask**: Mentre Flask és lleuger i fàcil d'utilitzar, FastAPI proporciona moltes funcionalitats addicionals "
  out-of-the-box" com la validació de dades i la serialització sense necessitat de plugins addicionals.
- **Django**: Django és un framework "full-stack" que proporciona una estructura més rígida. FastAPI, per contra, és més
  flexible i, per tant, pot ser més adequat per a aplicacions que requereixen un rendiment elevat en les operacions
  d'API.

## Instal·lació i configuració de l'entorn de desenvolupament

Per començar a treballar amb FastAPI, necessitaràs configurar el teu entorn de desenvolupament. Això inclou la
instal·lació del framework i altres eines auxiliars com Uvicorn, un servidor ASGI.

Si utilitzem un IDE com [PyCharm](https://www.jetbrains.com/pycharm/) simplement ens cal crear un nou projecte de tipus
FastAPI i seguir les passes.

Si volem entendre més sobre FastAPI ens vindrà molt bé seguir i entendre molt bé aquestes passes.

1. Preparem una nova aplicació amb Python. Recorda situar-te al directori on vols tenir la teva aplicació 😉

```bash
python -m venv venv
```

2. Una vegada finaltizi el procés activarem l'entorn Python executant:

```bash
source venv/bin/activate
```

3. Comprovaràs que el prompt del teu terminal favorit ha canviat indicant _venv_, normalment, a l'esquerra

```text
(venv) ~:
```

```bash
pip install fastapi uvicorn
```

Una vegada instal·lat, pots crear el teu primer fitxer `main.py` amb el següent codi bàsic:

```python
from fastapi import FastAPI

app = FastAPI()


@app.get("/")
async def read_root():
    return {"Hello": "World"}
```

El fragment anterior crea un *endpoint* FastAPI bàsic:

* `from fastapi import FastAPI`: La funcionalitat de la teva API la proporciona la classe FastAPI.

* `app = FastAPI()`: Això crea una instància de FastAPI.

* `@app.get("/")`: Es tracta d'un decorador que especifica a FastAPI que la funció que hi ha sota és l'encarregada de gestionar les peticions.  Aquest és un decorador que especifica la ruta. Això crea un mètode `GET` en la ruta del lloc. El resultat és retornat per la funció..

* Altres possibles operacions que s'utilitzen per a comunicar-se són `@app.post()`, `@app.put()`, `@app.delete()`, `@app.options()`, `@app.head()`, `@app.patch()` i `@app.trace()`.

Per executar l'aplicació, utilitza la següent ordre:

```bash
uvicorn main:app --reload
```

L'opció `--reload` fa que el servidor es reiniciï automàticament quan realitzes canvis en el codi. Això és útil durant
el desenvolupament, però s'hauria de treure en un entorn de producció.

Visita http://127.0.0.1:8000 en el teu navegador per veure la teva nova API en acció.
