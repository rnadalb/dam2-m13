# **1. Introducció a les API REST: Benvingudes a "_FastGames_"!**


Benvingudes a **FastGames**, la plataforma que revolucionarà el món dels eSports! Us imagineu formar part d’un equip que crea una aplicació per gestionar un torneig internacional de videojocs? Aquesta aplicació serà el punt central per organitzar equips, registrar jugadores i jugadors, gestionar resultats i establir classificacions.

Com a desenvolupadores i desenvolupadors, tindreu el repte de construir la base d’aquest sistema: una API que farà possible que altres aplicacions i serveis col·laborin amb **FastGames**. Però, què són exactament les API REST i per què són tan importants?

## **Què és una API?**
Una **API (Application Programming Interface)** és com un menú en un restaurant: especifica les opcions disponibles (operacions que es poden fer) i què cal proporcionar per obtenir el resultat esperat (paràmetres o dades necessàries). Tot i això, no revela com es preparen els resultats; simplement en retorna el producte final.

A **FastGames**, la nostra API serà aquest menú que permetrà:
- Consultar quins equips participen al torneig.
- Registrar noves jugadores i jugadors.
- Actualitzar els resultats dels partits.
- Obtenir la classificació actualitzada.

![API](01_Introduccio_a_les_API_REST.png "Title Text"){width=70%}

## **Què és REST i per què és útil?**
REST (**REpresentational State Transfer**) és un estil d’arquitectura per dissenyar APIs que permet una comunicació clara i estructurada entre sistemes diferents. Gràcies a REST, les APIs són més fàcils de construir, mantenir i utilitzar.

Les característiques principals de les API REST són:
1. **Ús de mètodes HTTP:** S’utilitzen verbs com `GET`, `POST`, `PUT` i `DELETE` per definir les accions.
2. **Format senzill de dades:** Les respostes solen utilitzar formats com JSON, que són fàcils de llegir i processar.
3. **Escalabilitat i flexibilitat:** Les API REST són ideals per construir sistemes escalables i adaptar-los a les necessitats canviants.

Imagineu que REST és com les normes d’un torneig: si tothom segueix les mateixes regles, el procés és més eficient i comprensible per a totes les parts implicades.

## **Exemples pràctics d’APIs en el món real**
Per entendre millor el concepte, explorem alguns exemples d’ús d’APIs que segurament ja heu experimentat:

1. **Spotify:** Quan cerqueu una cançó, l’aplicació envia una petició a la seva API, que retorna informació sobre la cançó i en permet la reproducció.
2. **Instagram:** Cada cop que feu scroll, Instagram utilitza la seva API per carregar més publicacions.
3. **Videojocs multijugador:** Joc com Fortnite utilitzen API per actualitzar les classificacions globals o gestionar les estadístiques de les jugadores i jugadors.

De manera similar, amb **FastGames**, dissenyareu una API que organitzarà i gestionarà el vostre torneig d’eSports. 🏆
