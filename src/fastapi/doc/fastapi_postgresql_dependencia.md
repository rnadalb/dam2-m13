
# **Treballar amb PostgreSQL a FastAPI**

## **Introducció**

PostgreSQL és un sistema de bases de dades relacional de codi obert conegut per la seva robustesa, extensibilitat i compatibilitat amb SQL estàndard. Quan es combina amb **FastAPI** i **SQLAlchemy**, ofereix una solució eficient per desenvolupar aplicacions web escalables i segures.

En aquest apartat, configurarem PostgreSQL per treballar amb **FastAPI** i implementarem operacions bàsiques amb models i sessions.

---

## **1. Configuració de PostgreSQL**

### **Instal·lació del controlador**
Per connectar-nos a una base de dades PostgreSQL des de Python, utilitzarem **`psycopg2`** com a controlador.

**Instal·lació amb `pip`:**
```bash
pip install psycopg2-binary sqlalchemy
```

---

## **2. Configuració de la connexió**

### **Fitxer `/fastgames/app/database.py`**
Creem un motor per a PostgreSQL utilitzant SQLAlchemy:
```python
from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker

# URL de la base de dades PostgreSQL
SQLALCHEMY_DATABASE_URL = "postgresql://username:password@localhost/fastgames"

# Crear el motor
engine = create_engine(SQLALCHEMY_DATABASE_URL)
SessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=engine)
Base = declarative_base()
```

- **`postgresql://username:password@localhost/fastgames`**:
  - `username`: Nom d'usuari del sistema PostgreSQL.
  - `password`: Contrasenya de l'usuari.
  - `localhost`: Host de la base de dades (pot ser un IP o un domini).
  - `fastgames`: Nom de la base de dades.

---

## **3. Crear models de bases de dades**

### **Fitxer `/fastgames/app/models/team.py`**
Definim un model per a l’entitat `Team`:
```python
from sqlalchemy import Column, Integer, String
from app.database import Base

class Team(Base):
    __tablename__ = "teams"

    id = Column(Integer, primary_key=True, index=True)
    name = Column(String, index=True)
    country = Column(String)
    rank = Column(Integer)
```

---

## **4. Inicialització de la base de dades**

Abans de començar, creem la base de dades `fastgames` a PostgreSQL utilitzant la consola:
```sql
CREATE DATABASE fastgames;
```

I inicialitzem les taules utilitzant el codi següent a `/fastgames/app/main.py`:
```python
from app.database import engine
from app.models.team import Base

Base.metadata.create_all(bind=engine)
```

---

## **5. Implementació d'operacions CRUD**

### **Fitxer `/fastgames/app/schemas/team.py`**
Esquemes per a la validació i estructuració de dades:
```python
from pydantic import BaseModel

class TeamBase(BaseModel):
    name: str
    country: str
    rank: int

class TeamCreate(TeamBase):
    pass

class Team(TeamBase):
    id: int

    class Config:
        orm_mode = True
```

---

### **CREATE: Afegir un equip**

#### **Codi a `/fastgames/app/routes/teams.py`**
```python
from fastapi import APIRouter, Depends
from sqlalchemy.orm import Session
from app.models.team import Team
from app.schemas.team import TeamCreate
from app.database import SessionLocal

router = APIRouter()

def get_db():
    db = SessionLocal()
    try:
        yield db
    finally:
        db.close()

@router.post("/teams", response_model=Team)
def create_team(team: TeamCreate, db: Session = Depends(get_db)):
    db_team = Team(name=team.name, country=team.country, rank=team.rank)
    db.add(db_team)
    db.commit()
    db.refresh(db_team)
    return db_team
```

---

### **Explicació del concepte de dependència**

En **FastAPI**, una **dependència** és un mecanisme per injectar funcionalitats o dades addicionals a les rutes de manera estructurada i reutilitzable. En el codi següent:

```python
db: Session = Depends(get_db)
```

- **`Depends(get_db)`**: És una dependència que injecta una instància de sessió de la base de dades al paràmetre `db`.
- **`get_db()`**: Aquesta funció gestiona la creació i tancament de la sessió de la base de dades de forma segura.

#### **Avantatges de les dependències**
1. **Reutilització del codi:** Podem definir una funció comuna (com `get_db`) i reutilitzar-la a diferents rutes.
2. **Gestió automàtica:** FastAPI s'encarrega de cridar i gestionar la dependència.
3. **Escalabilitat:** Facilita l’extensió del projecte, ja que podem injectar altres serveis com autenticació, configuració o altres recursos.
4. **Simplicitat:** Evitem la necessitat de gestionar manualment instàncies o recursos dins de les funcions de les rutes.

#### **Funció `get_db`**
Aquesta funció defineix la dependència i gestiona la sessió:
```python
def get_db():
    db = SessionLocal()
    try:
        yield db
    finally:
        db.close()
```

- **`SessionLocal()`**: Crea una nova sessió de la base de dades.
- **`yield db`**: Retorna la sessió per ser utilitzada a la ruta.
- **`finally: db.close()`**: Garanteix que la sessió es tanqui correctament, evitant fuites de recursos.

---

### **Provar amb `curl`**
**Crida:**
```bash
curl -X POST "http://localhost:8000/teams" -H "Content-Type: application/json" -d '{"name": "Dragons", "country": "Spain", "rank": 1}'
```

**Resposta:**
```json
{
    "id": 1,
    "name": "Dragons",
    "country": "Spain",
    "rank": 1
}
```

---

Amb aquesta explicació integrada, el concepte de dependència i la seva utilitat en FastAPI queden clarament definits per a l’alumnat. 😊
