# **7. Bases de dades amb FastAPI**

Les bases de dades permeten emmagatzemar i gestionar informació de manera persistent. A **FastAPI**, podem integrar bases de dades utilitzant biblioteques com **[SQLAlchemy](https://www.sqlalchemy.org/)** per gestionar models i sessions de manera eficient. Aquest apartat cobreix les operacions **CRUD** (_**C**reate, **R**ead, **U**pdate, **D**elete_) per interactuar amb una base de dades SQLite.

Abans de continuar ens caldrà conèixer que ès això de SQLAlchemy.

## **7.1. Què és SQLAlchemy?**

SQLAlchemy és una biblioteca popular de Python per interactuar amb bases de dades relacionals com **SQLite**, **PostgreSQL**, **MySQL** i altres. És un **ORM (Object Relational Mapper)**, és a dir, una eina que permet treballar amb bases de dades utilitzant objectes Python en lloc de sentències SQL tradicionals.


### **Per a què serveix?**

L'objectiu principal de SQLAlchemy és simplificar i abstraure la interacció amb bases de dades relacionals. Proporciona eines per:
- Definir l'estructura de les taules com classes Python.
- Realitzar operacions de consulta, inserció, actualització i eliminació sense necessitat d'escriure SQL explícitament (tot i que permet fer-ho si cal).
- Gestionar connexions, sessions i transaccions de manera eficient.

### **Què és un ORM?**

Un **ORM (Object Relational Mapper)** és una tècnica de programació que converteix dades d'una base de dades relacional en objectes del llenguatge de programació i viceversa. Amb un ORM:
1. Les taules de la base de dades es representen com classes.
2. Les files (registres) es representen com instàncies d'aquestes classes.
3. Es poden manipular dades utilitzant codi Python en lloc de SQL directe.

### **Avantatges de SQLAlchemy**

1. **Abstracció del SQL**
    - Permet treballar amb bases de dades utilitzant objectes Python i mètodes clars.
    - Ofereix suport per escriure SQL personalitzat si cal.

2. **Compatibilitat amb múltiples bases de dades**
    - És compatible amb diversos motors de bases de dades (SQLite, PostgreSQL, MySQL, Oracle, etc.).
    - És fàcil migrar d'una base de dades a una altra sense modificar massa codi.

3. **Flexibilitat**
    - Ofereix un nivell alt d'abstracció amb el seu ORM, però també permet treballar al nivell inferior amb SQL brut utilitzant **SQLAlchemy Core**.

4. **Gestió de sessions i transaccions**
    - Simplifica la gestió de connexions a la base de dades, garantint la integritat i eficiència.

5. **Comunitat i documentació**
    - Té una comunitat activa i una documentació extensa que facilita l'aprenentatge i la resolució de problemes.

### **Desavantatges de SQLAlchemy**

1. **Corba d'aprenentatge inicial**
    - És una eina molt potent, però pot resultar complexa per a principiants, especialment per a aquells que són nous en el concepte d’ORM o bases de dades.

2. **Rendiment amb operacions massives**
    - Les operacions massives poden ser menys eficients que SQL directe, ja que el codi ORM afegeix una capa addicional.

3. **Dependència de Python**
    - Està estretament vinculat a Python, la qual cosa pot ser una limitació si el projecte ha de ser multiplataforma en termes de llenguatges.

4. **Sobrecàrrega per a projectes senzills**
    - En aplicacions molt senzilles, l'ús de SQLAlchemy pot ser excessiu, ja que implementar directament SQL seria més fàcil i ràpid.



## **7.2. Configuració inicial**
### **Fitxer `/fastgames/app/database.py`**
```python
from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker

SQLALCHEMY_DATABASE_URL = "sqlite:///./fastapi.db"

engine = create_engine(SQLALCHEMY_DATABASE_URL, connect_args={"check_same_thread": False})
SessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=engine)
Base = declarative_base()
```
Una mica de literatura 🤣

El codi anteior configura la connexió entre **FastAPI** i una base de dades SQLite utilitzant SQLAlchemy, una biblioteca de Python per interactuar amb bases de dades relacionals.


### **1. `SQLALCHEMY_DATABASE_URL`**
Aquesta variable defineix l'URL de la base de dades.
- **Valor:** `"sqlite:///./fastapi.db"` indica que utilitzem SQLite i que la base de dades es troba al fitxer `fastapi.db` en el directori actual.
- Podeu canviar l'URL per utilitzar altres bases de dades, com PostgreSQL (`postgresql://`) o MySQL (`mysql://`).

### **2. `create_engine`**
Aquesta funció crea un motor de connexió amb la base de dades.
- **Paràmetre `connect_args={"check_same_thread": False}`:** És específic per SQLite i desactiva la verificació d’execució en el mateix fil.

### **3. `sessionmaker`**
Aquesta classe configura una fàbrica de sessions per interactuar amb la base de dades.
- **Opcions:**
    - `autocommit=False`: Desactiva l'autocommit, cal cridar `commit()` manualment per guardar canvis.
    - `autoflush=False`: Evita actualitzacions automàtiques dels objectes abans d’executar consultes.
    - `bind=engine`: Vincula les sessions amb el motor creat amb `create_engine`.

### **4. `Base`**
La variable `Base` és la classe base a partir de la qual es crearan els models de la base de dades.
- **Funció `declarative_base()`:** Proporciona les eines per definir models d'objectes relacionals que es tradueixen a taules de bases de dades.

### **Relació amb l'API de FastAPI**
Aquest codi no és específic de FastAPI, però s’integra perfectament amb ell per gestionar bases de dades. En un projecte de **FastAPI**, aquesta configuració permet utilitzar sessions de base de dades en rutes i dependències.

- Per a més informació, consulteu la [documentació oficial de FastAPI: SQL (Relational) Databases](https://fastapi.tiangolo.com/tutorial/sql-databases/). Aquesta secció inclou exemples i detalls sobre com treballar amb SQLAlchemy i bases de dades relacionals a FastAPI.


## Preparan't la nosta app

### **Fitxer `/fastgames/app/models/team.py`**
Els models defineixen l’estructura de les dades a la base de dades.

```python
from sqlalchemy import Column, Integer, String
from app.database import Base

class Team(Base):
    __tablename__ = "teams"

    id = Column(Integer, primary_key=True, index=True)
    name = Column(String, index=True)
    country = Column(String)
    rank = Column(Integer)
```

### **Fitxer `/fastgames/app/schemas/team.py`**
Els esquemes defineixen les dades d’entrada i sortida, utilitzant **Pydantic** per validar i estructurar la informació.

```python
from pydantic import BaseModel

class TeamBase(BaseModel):
    name: str
    country: str
    rank: int

class TeamCreate(TeamBase):
    pass

class Team(TeamBase):
    id: int

    class Config:
        from_attributes = True
```


### **Inicialització de la base de dades**
A `/fastgames/app/main.py`, inicialitzem la base de dades:
```python
from app.database import engine
from app.models.team import Base

Base.metadata.create_all(bind=engine)
```

## **7.3. Operacions CRUD**

### **CREATE: crear un equip**
Ruta definida a `/fastgames/app/routes/teams.py` per permetre la creació d’equips:
```python
from fastapi import APIRouter, Depends
from sqlalchemy.orm import Session
from app.models.team import Team
from app.schemas.team import TeamCreate
from app.database import SessionLocal

router = APIRouter()

def get_db():
    db = SessionLocal()
    try:
        yield db
    finally:
        db.close()

@router.post("/teams", response_model=Team)
def create_team(team: TeamCreate, db: Session = Depends(get_db)):
    db_team = Team(name=team.name, country=team.country, rank=team.rank)
    db.add(db_team)
    db.commit()
    db.refresh(db_team)
    return db_team
```

**Crida amb `curl`**
```bash
curl -X POST "http://localhost:8000/teams" -H "Content-Type: application/json" -d '{"name": "Dragons", "country": "Spain", "rank": 1}'
```

**Resposta**
```json
{
    "id": 1,
    "name": "Dragons",
    "country": "Spain",
    "rank": 1
}
```

### **READ: llistar tots els equips**
Ruta definida a `/fastgames/app/routes/teams.py`:
```python
@router.get("/teams", response_model=list[Team])
def read_teams(skip: int = 0, limit: int = 10, db: Session = Depends(get_db)):
    return db.query(Team).offset(skip).limit(limit).all()
```

**Crida al navegador**
```
http://localhost:8000/teams
```

**Resposta**
```json
[
    {
        "id": 1,
        "name": "Dragons",
        "country": "Spain",
        "rank": 1
    }
]
```

### **READ: obtenir un equip per ID**
Ruta definida a `/fastgames/app/routes/teams.py`:
```python
from fastapi import HTTPException

@router.get("/teams/{team_id}", response_model=Team)
def read_team(team_id: int, db: Session = Depends(get_db)):
    team = db.query(Team).filter(Team.id == team_id).first()
    if not team:
        raise HTTPException(status_code=404, detail="Equip no trobat")
    return team
```

**Crida al navegador**
```
http://localhost:8000/teams/1
```

**Resposta**
```json
{
    "id": 1,
    "name": "Dragons",
    "country": "Spain",
    "rank": 1
}
```

### **UPDATE: actualitzar un equip**
Ruta definida a `/fastgames/app/routes/teams.py`:
```python
@router.put("/teams/{team_id}", response_model=Team)
def update_team(team_id: int, team: TeamCreate, db: Session = Depends(get_db)):
    db_team = db.query(Team).filter(Team.id == team_id).first()
    if not db_team:
        raise HTTPException(status_code=404, detail="Equip no trobat")
    db_team.name = team.name
    db_team.country = team.country
    db_team.rank = team.rank
    db.commit()
    db.refresh(db_team)
    return db_team
```

**Crida amb `curl`**
```bash
curl -X PUT "http://localhost:8000/teams/1" -H "Content-Type: application/json" -d '{"name": "Lions", "country": "UK", "rank": 2}'
```

**Resposta**
```json
{
    "id": 1,
    "name": "Lions",
    "country": "UK",
    "rank": 2
}
```

### **DELETE: eliminar un equip**
Ruta definida a `/fastgames/app/routes/teams.py`:
```python
@router.delete("/teams/{team_id}")
def delete_team(team_id: int, db: Session = Depends(get_db)):
    db_team = db.query(Team).filter(Team.id == team_id).first()
    if not db_team:
        raise HTTPException(status_code=404, detail="Equip no trobat")
    db.delete(db_team)
    db.commit()
    return {"message": f"Equip {team_id} eliminat correctament"}
```

**Crida amb `curl`**
```bash
curl -X DELETE "http://localhost:8000/teams/1"
```

**Resposta**
```json
{
    "message": "Equip 1 eliminat correctament"
}
```

## **7.4. Treballar amb Postgres**

PostgreSQL és un sistema de bases de dades relacional de codi obert conegut per la seva robustesa, extensibilitat i compatibilitat amb SQL estàndard. Quan es combina amb **FastAPI** i **SQLAlchemy**, ofereix una solució eficient per desenvolupar aplicacions web escalables i segures.

### **Configuració de PostgreSQL**

#### **Instal·lació del controlador**
Per connectar-nos a una base de dades PostgreSQL des de Python, utilitzarem **[psycopg2-binary](https://pypi.org/project/psycopg2-binary/)** com a controlador.

```bash
pip install psycopg2-binary
```

#### **Configuració de la connexió**

#### **Fitxer `/fastgames/app/database.py`**

Creem un motor per a PostgreSQL utilitzant SQLAlchemy:

```python
from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker

# URL de la base de dades PostgreSQL
SQLALCHEMY_DATABASE_URL = "postgresql://username:password@localhost/fastgames"

# Crear el motor
engine = create_engine(SQLALCHEMY_DATABASE_URL)
SessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=engine)
Base = declarative_base()
```

- **`postgresql://username:password@localhost/fastgames`**:
    - `username`: nom d'usuari del sistema PostgreSQL.
    - `password`: contrasenya de l'usuari.
    - `localhost`: host de la base de dades (pot ser una adreça IP o un domini).
    - `fastgames`: nom de la base de dades.

### **Crear models de bases de dades**

#### **Fitxer `/fastgames/app/models/team.py`**
Definim un model per a l’entitat `Team`:

```python
from sqlalchemy import Column, Integer, String
from app.database import Base

class Team(Base):
    __tablename__ = "teams"

    id = Column(Integer, primary_key=True, index=True)
    name = Column(String, index=True)
    country = Column(String)
    rank = Column(Integer)
```

Com hauràs endevinat el model no canvia!

### **Inicialització de la base de dades**

Abans de començar, creem la base de dades `fastgames` a PostgreSQL utilitzant la consola:
```sql
CREATE DATABASE fastgames;
```

Recordeu que teniu una base de dades personal o que podeu utilitzar [docker](https://hub.docker.com/_/postgres).

Inicialitzarem les taules utilitzant el codi següent a `/fastgames/app/main.py`:
```python
from app.database import engine
from app.models.team import Base

Base.metadata.create_all(bind=engine)
```

### **Implementació d'operacions CRUD**

#### **Fitxer `/fastgames/app/schemas/team.py`**
Esquemes per a la validació i estructuració de dades:
```python
from pydantic import BaseModel

class TeamBase(BaseModel):
    name: str
    country: str
    rank: int

class TeamCreate(TeamBase):
    pass

class Team(TeamBase):
    id: int

    class Config:
        from_attributes = True
```

### **CRUD sobre el model team**

#### **Codi a `/fastgames/app/routes/teams.py`**
```python
from fastapi import APIRouter, Depends
from sqlalchemy.orm import Session
from app.models.team import Team
from app.schemas.team import TeamCreate
from app.database import SessionLocal

router = APIRouter()

def get_db():
    db = SessionLocal()
    try:
        yield db
    finally:
        db.close()


@router.post("/teams", response_model=Team)
def create_team(team: TeamCreate, db: Session = Depends(get_db)):
    db_team = Team(name=team.name, country=team.country, rank=team.rank)
    db.add(db_team)
    db.commit()
    db.refresh(db_team)
    return db_team


@router.get("/teams", response_model=list[Team])
def read_teams(skip: int = 0, limit: int = 10, db: Session = Depends(get_db)):
    return db.query(Team).offset(skip).limit(limit).all()


@router.put("/teams/{team_id}", response_model=Team)
def update_team(team_id: int, team: TeamCreate, db: Session = Depends(get_db)):
    db_team = db.query(Team).filter(Team.id == team_id).first()
    if not db_team:
        raise HTTPException(status_code=404, detail="Equip no trobat")
    db_team.name = team.name
    db_team.country = team.country
    db_team.rank = team.rank
    db.commit()
    db.refresh(db_team)
    return db_team


@router.delete("/teams/{team_id}")
def delete_team(team_id: int, db: Session = Depends(get_db)):
    db_team = db.query(Team).filter(Team.id == team_id).first()
    if not db_team:
        raise HTTPException(status_code=404, detail="Equip no trobat")
    db.delete(db_team)
    db.commit()
    return {"message": f"Equip {team_id} eliminat correctament"}
```

#### **Què significa _db: Session = Depends(get_db)_**

En **FastAPI**, una **dependència** és un mecanisme per injectar funcionalitats o dades addicionals a les rutes de manera estructurada i reutilitzable. En el codi següent:

```python
db: Session = Depends(get_db)
```

- **`Depends(get_db)`**: És una dependència que injecta una instància de sessió de la base de dades al paràmetre `db`.
- **`get_db()`**: Aquesta funció gestiona la creació i tancament de la sessió de la base de dades de forma segura.

#### **Avantatges de les dependències**
1. **Reutilització del codi:** podem definir una funció comuna (com `get_db`) i reutilitzar-la a diferents rutes.
2. **Gestió automàtica:** FastAPI s'encarrega de cridar i gestionar la dependència.
3. **Escalabilitat:** facilita l’extensió del projecte, ja que podem injectar altres serveis com autenticació, configuració o altres recursos.
4. **Simplicitat:** evitem la necessitat de gestionar manualment instàncies o recursos dins de les funcions de les rutes.

#### **Funció `get_db`**
Aquesta funció defineix la dependència i gestiona la sessió:
```python
def get_db():
    db = SessionLocal()
    try:
        yield db
    finally:
        db.close()
```

- **`SessionLocal()`**: Crea una nova sessió de la base de dades.
- **`yield db`**: Retorna la sessió per ser utilitzada a la ruta.
- **`finally: db.close()`**: Garanteix que la sessió es tanqui correctament, evitant fuites de recursos.

## Provar les operacions amb PostgreSQL

**1. Crear un equip**:
```bash
curl -X POST "http://localhost:8000/teams" -H "Content-Type: application/json" -d '{"name": "Dragons", "country": "Spain", "rank": 1}'
```
**2. Llistar els equips**:
```bash
curl -X GET "http://localhost:8000/teams"
```
**3. Actualitzar un equip**:
```bash
curl -X PUT "http://localhost:8000/teams/1" -H "Content-Type: application/json" -d '{"name": "Lions", "country": "UK", "rank": 2}'
```
**4. Eliminar un equip**:
```bash
curl -X DELETE "http://localhost:8000/teams/1"
```
