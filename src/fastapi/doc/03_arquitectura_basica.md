
# **3. Arquitectura bàsica d'una aplicació FastAPI**

Amb FastAPI instal·lat i el primer **_endpoint_** funcionant, és moment d'organitzar el nostre projecte de manera professional. Una estructura clara i modular és essencial per mantenir el codi net i fàcil de gestionar, especialment en projectes més grans com **FastGames**.

Un **_endpoint_** és un punt d’accés definit en una API (Application Programming Interface) que permet la comunicació entre diferents sistemes. Cada endpoint està associat a una adreça única (URL o URI) i defineix una operació específica que el sistema pot realitzar, com obtenir dades, enviar informació o actualitzar recursos.

Aquest apartat cobreix:
1. Una estructura recomanada per a projectes FastAPI.
2. Com crear rutes senzilles i ben organitzades.
3. Introducció al concepte de "path operations" (operacions de ruta).

![arquitectura](03_arquitectura_basica.png "Title Text"){width=70%}

## **Estructura del projecte**
Per mantenir el codi modular i escalable, utilitzarem una estructura de projectes que inclou **paquets Python**. Això ens permet dividir el projecte en components més petits i independents.

### **Estructura recomanada**
Aquí teniu l'estructura del projecte amb els fitxers necessaris:

```
fastgames/
├── app/
│   ├── __init__.py       # Fa que la carpeta app sigui un paquet Python.
│   ├── main.py           # Punt d'entrada de l'aplicació.
│   ├── routes/           # Conté les rutes de l'API.
│   │   ├── __init__.py   # Permet importar rutes des de routes.
│   │   ├── teams.py      # Rutes relacionades amb equips.
│   │   ├── players.py    # Rutes relacionades amb jugadores i jugadors.
│   └── models/           # Models de dades per validar i estructurar la informació.
│       ├── __init__.py   # Permet importar models des de models.
│       ├── team.py       # Model per als equips.
│       ├── player.py     # Model per a les jugadores i jugadors.
└── venv/                 # Entorn virtual.
```

### **Funció dels fitxers `__init__.py`**
- **`app/__init__.py`**: Marca la carpeta `app` com a paquet Python. És útil per importar submòduls com `routes` o `models`.
- **`app/routes/__init__.py`**: Permet agrupar i importar rutes des de la carpeta `routes`.
- **`app/models/__init__.py`**: Fa el mateix amb els models, agrupant totes les definicions en un sol paquet.

## **Primeres rutes: exemple pràctic**
Comencem definint rutes bàsiques per gestionar equips i jugadores o jugadors.

### **1. Crear rutes al fitxer `teams.py`**
Creeu el fitxer `teams.py` dins la carpeta `app/routes/` amb el següent contingut:

```python
from fastapi import APIRouter

router = APIRouter()

teams = []

@router.get("/teams")
def get_teams():
    return {"teams": teams}

@router.post("/teams")
def add_team(team_name: str):
    teams.append(team_name)
    return {"message": f"Equip '{team_name}' afegit correctament!"}
```

### **2. Registrar les rutes a `main.py`**
Editeu el fitxer `main.py` per incloure les rutes definides a `teams.py`:

```python
from fastapi import FastAPI
from app.routes import teams

app = FastAPI()

app.include_router(teams.router)
```

Amb això, ja tenim una API que gestiona equips de manera bàsica.

---

## **Path operations**
Les **path operations** són les operacions associades a les rutes de l’API. Cada operació defineix:
1. **El mètode HTTP** (per exemple, `GET`, `POST`, `PUT`, `DELETE`).
2. **El camí de la ruta** (per exemple, `/teams`).
3. **La funció associada**, que processa la petició i retorna una resposta.

### **Tipus de path operations**
- **`GET`**: Per obtenir dades. Exemple: obtenir la llista d'equips.
- **`POST`**: Per enviar dades noves. Exemple: afegir un nou equip.
- **`PUT`**: Per actualitzar dades existents. Exemple: modificar el nom d'un equip.
- **`DELETE`**: Per eliminar dades. Exemple: eliminar un equip.

---

## **Prova pràctica**
1. **Obtenir la llista d'equips**:
   - Ruta: `GET /teams`
   - Resposta esperada (si no hi ha equips):
     ```json
     {
         "teams": []
     }
     ```

2. **Afegir un nou equip**:
   - Ruta: `POST /teams`
   - Paràmetre: `team_name="Dragons"`
   - Resposta esperada:
     ```json
     {
         "message": "Equip 'Dragons' afegit correctament!"
     }
     ```

3. **Comprovar que l'equip s'ha afegit**:
   - Ruta: `GET /teams`
   - Resposta esperada:
     ```json
     {
         "teams": ["Dragons"]
     }
     ```