
# **5. Herència en models Pydantic**

Els models de **Pydantic** permeten utilitzar **herència**, cosa que facilita la reutilització de models i la creació de jerarquies. Aquesta funcionalitat és especialment útil quan tenim models amb camps comuns que volem compartir entre diferents subclasses.

A continuació, veurem com treballar amb herència, classificant els casos més comuns.


![API](05_herencia_models_pydantic.png "Title Text"){width=70%}

### **5.1. Herència bàsica**
L'**herència bàsica** permet que un model fill hereti tots els camps del model pare, afegint o modificant camps específics.

#### **Exemple**
```python
from pydantic import BaseModel

class BaseTeam(BaseModel):
    name: str
    country: str

class RegionalTeam(BaseTeam):
    region: str  # Camp addicional específic.
```

- **`BaseTeam`**: Conté els camps comuns.
- **`RegionalTeam`**: Hereta els camps de `BaseTeam` i afegeix un camp específic `region`.


### **5.2. Sobreescriptura de camps**
Els models fills poden sobreescriure camps del model pare per canviar-ne els tipus o les restriccions.

#### **Exemple**
```python
class BaseTeam(BaseModel):
    name: str
    country: str

class ModifiedTeam(BaseTeam):
    name: int  # Sobreescriu el tipus del camp 'name'.
```

#### **Nota**
Tot i que és possible sobreescriure camps, cal fer-ho amb precaució per evitar inconsistències.


### **5.3. Afegir validacions específiques**
Els models fills poden incloure validacions addicionals sense afectar el model pare.

#### **Exemple**
```python
from pydantic import BaseModel, validator

class BaseTeam(BaseModel):
    name: str
    country: str

class RankedTeam(BaseTeam):
    rank: int

    @validator("rank")
    def validate_rank(cls, value):
        if value < 1 or value > 100:
            raise ValueError("Rank must be between 1 and 100")
        return value
```

- **`BaseTeam`**: Defineix els camps comuns.
- **`RankedTeam`**: Afegeix un camp `rank` amb validacions específiques.


### **5.4. Jerarquies complexes**
Els models fills poden heretar de models que, al seu torn, hereten d'altres models, creant jerarquies complexes.

#### **Exemple**
```python
class BaseTeam(BaseModel):
    name: str
    country: str

class RegionalTeam(BaseTeam):
    region: str

class InternationalTeam(RegionalTeam):
    players: list[str]
```

- **`BaseTeam`**: Model base.
- **`RegionalTeam`**: Hereta de `BaseTeam` i afegeix `region`.
- **`InternationalTeam`**: Hereta de `RegionalTeam` i afegeix `players`.

---

### **5.5. Herència i validacions automàtiques**
Els models Pydantic mantenen la validació automàtica de tots els camps, independentment del nivell de la jerarquia.

#### **Exemple**
Model complet:
```python
class BaseTeam(BaseModel):
    name: str
    country: str

class RegionalTeam(BaseTeam):
    region: str

class InternationalTeam(RegionalTeam):
    players: list[str]
```

Petició:
```json
{
    "name": "Dragons",
    "country": "Spain",
    "region": "Europe",
    "players": ["Player1", "Player2"]
}
```

Resposta:
```json
{
    "message": "Equip internacional 'Dragons' afegit correctament!"
}
```
