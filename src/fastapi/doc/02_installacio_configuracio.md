
# **2. Instal·lació i configuració de FastAPI**

Ara que ja coneixeu la importància de les API REST, és hora de posar mans a l’obra i començar a construir la vostra pròpia API amb **FastAPI**. A partir d’aquí, convertirem les idees sobre **FastGames** en una aplicació funcional.

En aquest apartat, aprendrem a:
1. Configurar el nostre entorn de treball.
2. Instal·lar **FastAPI** i Uvicorn.
3. Crear el nostre primer projecte.
4. Executar el servidor per veure els primers resultats.

![API](02_installacio_configuracio.png "Title Text"){width=70%}

## **Requisits previs**
Abans d’iniciar-nos en el desenvolupament, assegureu-vos que teniu les eines necessàries al vostre ordinador. Aquí teniu una llista dels requisits:

### **1. Python**
FastAPI requereix Python. Podeu comprovar la vostra versió de Python amb la següent ordre al terminal:

```bash
python --version
```

Si no teniu Python instal·lat o la versió no és compatible, descarregueu la versió més recent des de [python.org](https://www.python.org/).


### **2. Editor de codi**
Recomanem utilitzar un editor de codi modern com **PyCharm Professional** del que haurieu de tenir llicència educativa o **Visual Studio Code (VS Code)**, tots dos ofereien
moltes funcionalitats per a Python, com l’autocompleció i la integració amb eines de depuració. Podeu descarregar-lo aquí:
* [PyCharm Professional](https://www.jetbrains.com/pycharm/download/)
* [Visual Studio Code](https://code.visualstudio.com/)


## **Creació de l’entorn de treball**
Per entendre millor que fans molts IDE's per nosaltres i aquestes passes les farem 'a mà'.
Per organitzar correctament el projecte, és recomanable utilitzar un **entorn virtual**. Això ens permetrà mantenir les dependències del projecte separades de les del sistema.

### **1. Creeu una carpeta per al projecte**
Creeu una carpeta on guardarem els arxius del projecte. Podeu fer-ho des del terminal:

```bash
mkdir fastgames
cd fastgames
```

### **2. Creeu un entorn virtual**
Un entorn virtual ens ajuda a instal·lar paquets només per a aquest projecte. Utilitzeu la comanda següent per crear-lo:

```bash
python -m venv venv
```

Activeu l’entorn virtual:
- **Windows**:
  ```bash
  .env\scripts\activate
  ```
- **MacOS/Linux**:
  ```bash
  source venv/bin/activate
  ```

Un cop activat, veureu que el nom de l’entorn (per exemple, `venv`) apareix al principi del terminal. Això indica que esteu treballant dins de l’entorn virtual.


## **Instal·lació de FastAPI i Uvicorn**
FastAPI és el framework que utilitzarem per crear la nostra API, mentre que **Uvicorn** és un servidor lleuger i ràpid per executar-la.

Instal·leu-los utilitzant `pip`:

```bash
pip install fastapi uvicorn
```

Per comprovar que s’ha instal·lat correctament, executeu:

```bash
pip list
```

Haureu de veure FastAPI i Uvicorn a la llista.

---

## **Creació del primer projecte FastAPI**
Crearem un fitxer senzill per assegurar-nos que tot funciona correctament.

1. Creeu un fitxer anomenat `main.py` dins la carpeta del projecte:

```bash
touch main.py
```

2. Afegiu el següent codi al fitxer `main.py`:

```python
from fastapi import FastAPI

app = FastAPI()

@app.get("/")
def read_root():
    return {"message": "Benvingudes a FastGames!"}
```

Aquest codi fa dues coses:
- Crea una instància de l’aplicació FastAPI.
- Defineix una ruta (`/`) que respon amb un missatge.

---

## **Executar el servidor**
Ara que tenim el nostre primer fitxer, executem-lo amb **Uvicorn** per veure l’API en acció:

```bash
uvicorn main:app --reload
```

### **Què fa aquesta ordre?**
- **`main`**: Indica el nom del fitxer (`main.py`).
- **`app`**: És el nom de la instància FastAPI que hem creat.
- **`--reload`**: Permet que el servidor es recarregui automàticament quan fem canvis al codi.

Quan executeu la comanda, veureu un missatge similar a aquest al terminal:

```
INFO:     Uvicorn running on http://127.0.0.1:8000 (Press CTRL+C to quit)
INFO:     Application startup complete.
```

---

## **Comprovació inicial**
1. Obriu un navegador i accediu a `http://127.0.0.1:8000`. Veureu una resposta JSON amb el missatge:  
   ```json
   {
       "message": "Benvingudes a FastGames!"
   }
   ```

2. Accediu a la documentació automàtica de FastAPI a `http://127.0.0.1:8000/docs`. Aquí podreu veure i provar les rutes definides.
