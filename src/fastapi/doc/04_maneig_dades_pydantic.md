
# **4. Maneig de dades amb Pydantic**

Un dels punts forts de **FastAPI** és l’ús de **Pydantic** per a la validació i estructuració de dades. Gràcies a Pydantic, podem definir models que s’utilitzen per validar dades d’entrada i sortida de manera automàtica, assegurant-nos que la informació que rep i envia la nostra API és sempre coherent i conforme a les especificacions.

En aquest apartat, aprendrem a:
1. Crear models de dades amb **Pydantic**.
2. Validar dades d'entrada.
3. Validar dades de sortida.
4. Tipus de dades que accepta **Pydantic**.
5. Gestió d'errors

![maneig dades](04_maneig_dades_pydantic.png "Title Text"){width=70%}


## **1. Introducció als models de Pydantic**
Els models de **Pydantic** es defineixen com classes Python que hereten de `BaseModel`. Cada atribut d’un model representa un camp de dades, i podem especificar el tipus de dades esperat.

### **Exemple**
Creeu un fitxer nou anomenat `team.py` dins de `app/models/` i definiu-hi un model per representar un equip:

```python
from pydantic import BaseModel

class Team(BaseModel):
    name: str
    country: str
    rank: int
```

## **2. Validar dades d'entrada**
Afegim validació de dades utilitzant el model `Team` a les rutes de l’API. Editeu el fitxer `teams.py` i modifiqueu el codi per utilitzar el model:

```python
from fastapi import APIRouter
from app.models.team import Team

router = APIRouter()

teams = []

@router.post("/teams")
def add_team(team: Team):
    teams.append(team)
    return {"message": f"Equip '{team.name}' afegit correctament!"}
```

## **3. Validar dades de sortida**
També podem utilitzar models de Pydantic per assegurar que les dades que envia la nostra API segueixin una estructura definida. Editeu la ruta `GET /teams` per retornar un model:

```python
from typing import List

@router.get("/teams", response_model=List[Team])
def get_teams():
    return teams
```

## **4. Tipus de dades acceptats per Pydantic**

Pydantic suporta una àmplia varietat de tipus de dades que podem utilitzar per definir els camps dels nostres models. Això ens permet representar informació de manera precisa i garantir la validació automàtica.

### Tipus de dades bàsics

1. **str**: Cadena de text.
2. **int**: Enter.
3. **float**: Número decimal.
4. **bool**: Valor booleà (True o False).

### Tipus avançats

1. **List**: Una llista d’elements del mateix tipus.
    • Exemple: List[str] (llista de cadenes de text).
2. **Dict**: Un diccionari amb claus i valors.
    • Exemple: Dict[str, int] (diccionari amb claus de tipus cadena i valors enters).
3. **Optional**: Camp opcional (pot ser del tipus definit o None).
    • Exemple: Optional[str] (cadena de text o valor None).
4. **Union**: Permet definir múltiples tipus possibles per a un camp.
    • Exemple: Union[int, str] (pot ser un enter o una cadena).
5. **datetime**: Representa dates i hores.
    • Exemple: datetime.datetime per gestionar dates i hores completes.
6. **date**: Només la data.
    • Exemple: datetime.date

### Tipus de dades específics per treballar amb correus electrònics
1. **`EmailStr`**: Valida que el camp conté un correu electrònic vàlid.
2. **`NameEmail`**: Valida noms i correus electrònics combinats, com `"Nom <correu@example.com>"`.

#### Exemple
```python
from pydantic import BaseModel, EmailStr, NameEmail
from typing import List, Optional
from datetime import datetime

class Team(BaseModel):
    name: str
    country: str
    rank: int
    players: Optional[List[str]] = None  # Opcional: Llista de noms de jugadores i jugadors.
    created_at: datetime                  # Data i hora de creació de l’equip.
    contact_email: EmailStr               # Correu electrònic de contacte de l’equip.
    manager: Optional[NameEmail] = None   # Opcional: Nom i correu del representant.
```

### Petició d’exemple

Ruta: POST /teams

Còs de la petició:
```json
{
    "name": "Dragons",
    "country": "Spain",
    "rank": 1,
    "players": ["Player1", "Player2"],
    "created_at": "2024-11-26T10:00:00",
    "contact_email": "dragons@example.com",
    "manager": "Alice Manager <alice.manager@example.com>"
}
```
Resposta:
```json
{
  "message": "Equip 'Dragons' afegit correctament!"
}
```

## **5. Gestió d’errors comuns**

Quan es treballa amb models de **Pydantic**, els errors de validació són habituals quan les dades d’entrada no compleixen els requisits especificats. **FastAPI** s’encarrega de gestionar i retornar errors detallats de manera automàtica.

A continuació, es mostren diferents tipus d’errors comuns agrupats per categoria, amb exemples i com es gestionen.

---

### **5.1. Errors per camps obligatoris**
Els camps obligatoris definits en un model són imprescindibles en les peticions. Si falta algun camp, es genera un error de validació.

#### **Exemple**
Model:
```python
class Team(BaseModel):
    name: str
    country: str
    rank: int
```

Petició:
```json
{
    "name": "Dragons",
    "rank": 1
}
```

Resposta:
```json
{
    "detail": [
        {
            "loc": ["body", "country"],
            "msg": "field required",
            "type": "value_error.missing"
        }
    ]
}
```

---

### **5.2. Errors de tipus de dades**
Si un camp conté un valor amb un tipus incorrecte, es genera un error automàticament.

#### **Exemple**
Petició:
```json
{
    "name": "Dragons",
    "country": "Spain",
    "rank": "first"
}
```

Resposta:
```json
{
    "detail": [
        {
            "loc": ["body", "rank"],
            "msg": "value is not a valid integer",
            "type": "type_error.integer"
        }
    ]
}
```

---

### **5.3. Errors de format**
Apareixen quan es treballa amb tipus de dades avançats (com correus electrònics) i el valor no compleix el format esperat.

#### **Exemple**
Model:
```python
class Team(BaseModel):
    name: str
    contact_email: EmailStr
```

Petició:
```json
{
    "name": "Dragons",
    "contact_email": "not_an_email"
}
```

Resposta:
```json
{
    "detail": [
        {
            "loc": ["body", "contact_email"],
            "msg": "value is not a valid email address",
            "type": "value_error.email"
        }
    ]
}
```

---

### **5.4. Errors en valors fora de rang o longitud**
Podem definir restriccions com longitud mínima/màxima o rangs numèrics. Si no es compleixen, es genera un error de validació.

#### **Exemple**
Model:
```python
class Team(BaseModel):
    name: str
    rank: int

    @validator("rank")
    def check_rank(cls, value):
        if value < 1 or value > 100:
            raise ValueError("rank must be between 1 and 100")
        return value
```

Petició:
```json
{
    "name": "Dragons",
    "rank": 101
}
```

Resposta:
```json
{
    "detail": [
        {
            "loc": ["body", "rank"],
            "msg": "rank must be between 1 and 100",
            "type": "value_error"
        }
    ]
}
```

---

### **5.5. Errors en camps opcionals**
Els camps opcionals poden contenir valors o ser `None`. Si el tipus del valor no coincideix amb el tipus esperat, es genera un error.

#### **Exemple**
Model:
```python
class Team(BaseModel):
    name: str
    manager: Optional[NameEmail] = None
```

Petició:
```json
{
    "name": "Dragons",
    "manager": "not_an_email"
}
```

Resposta:
```json
{
    "detail": [
        {
            "loc": ["body", "manager"],
            "msg": "value is not a valid email address",
            "type": "value_error.email"
        }
    ]
}
```

---

### **5.6. Errors en col·leccions**
Quan treballem amb tipus com `List` o `Dict`, els errors es generen si els elements individuals no compleixen els requisits.

#### **Exemple**
Model:
```python
class Team(BaseModel):
    players: List[str]
```

Petició:
```json
{
    "players": ["Player1", 123, "Player3"]
}
```

Resposta:
```json
{
    "detail": [
        {
            "loc": ["body", "players", 1],
            "msg": "str type expected",
            "type": "type_error.str"
        }
    ]
}
```
