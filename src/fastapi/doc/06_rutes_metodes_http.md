
# **6. Rutes i mètodes HTTP**

Les rutes són la base de qualsevol API. A **FastAPI**, cada ruta defineix un punt d'accés per interactuar amb l’aplicació, ja sigui per obtenir dades, crear recursos, actualitzar informació o eliminar-la. Aquest apartat classifica les operacions amb rutes en funció de la seva complexitat per facilitar-ne l’aprenentatge.


## **6.1. Rutes bàsiques: operacions senzilles**
Aquestes operacions són simples i només requereixen mètodes HTTP com `GET` o `POST` per interactuar amb els recursos.

### **Exemple: llistar recursos**
Definim una ruta que retorna una llista d’equips:
```python
@app.get("/teams")
def list_teams():
    return {"teams": ["Dragons", "Lions", "Sharks"]}
```

**Crida al navegador**
```
http://localhost:8000/teams
```

**Resposta**
```json
{
    "teams": ["Dragons", "Lions", "Sharks"]
}
```

---

### **Exemple: crear un recurs**
Definim una ruta per crear un equip:
```python
@app.post("/teams")
def create_team(name: str):
    return {"message": f"Equip '{name}' creat correctament"}
```

**Crida al navegador** (simulat com a query string per entendre millor):

Per a fer-ho correctament i no pots utilitzar l'eina [curl](https://curl.se/) o [Postman](https://www.postman.com/).
```
http://localhost:8000/teams?name=Dragons
```

**Crida amb `curl`**
```bash
curl -X POST "http://localhost:8000/teams" -d "name=Dragons"
```

**Resposta**
```json
{
    "message": "Equip 'Dragons' creat correctament"
}
```

## **6.2. Rutes intermèdies: Paràmetres i query strings**
Aquestes operacions afegeixen flexibilitat amb **paràmetres de ruta** o **query strings**, permetent filtrar o personalitzar els resultats.

### **Paràmetres de ruta**
S’utilitzen per identificar un recurs específic.

#### **Exemple: Consultar un recurs per ID**
```python
@app.get("/teams/{team_id}")
def get_team(team_id: int):
    return {"team_id": team_id, "message": f"Informació de l'equip {team_id}"}
```

**Crida al navegador**
```
http://localhost:8000/teams/42
```

**Resposta**
```json
{
    "team_id": 42,
    "message": "Informació de l'equip 42"
}
```

### **Query strings**
S’utilitzen per filtrar o afegir paràmetres opcionals.

#### **Exemple: Filtrar equips**
```python
@app.get("/teams")
def filter_teams(country: str = None, min_rank: int = 1, max_rank: int = 100):
    return {
        "filters": {
            "country": country,
            "min_rank": min_rank,
            "max_rank": max_rank,
        }
    }
```

**Crida al navegador**
```
http://localhost:8000/teams?country=Spain&min_rank=1&max_rank=10
```

**Resposta**
```json
{
    "filters": {
        "country": "Spain",
        "min_rank": 1,
        "max_rank": 10
    }
}
```

## **6.3. Rutes avançades: validacions i respostes personalitzades**
Aquestes operacions impliquen validació automàtica, configuració de codis d'estat i modificació de respostes.

### **Validació de dades**
Podem utilitzar **Pydantic** per validar dades.

#### **Exemple**
```python
from pydantic import BaseModel

class Team(BaseModel):
    name: str
    rank: int

@app.post("/teams")
def create_team(team: Team):
    return {"message": f"Equip '{team.name}' amb rànquing {team.rank} creat"}
```

**Crida amb `curl`**
```bash
curl -X POST "http://localhost:8000/teams" -H "Content-Type: application/json" -d '{"name": "Dragons", "rank": 1}'
```

**Resposta**
```json
{
    "message": "Equip 'Dragons' amb rànquing 1 creat"
}
```

### **Respostes personalitzades**
Podem personalitzar els codis d'estat o afegir capçaleres.

#### **Exemple**
```python
from fastapi import Response, status

@app.post("/teams", status_code=status.HTTP_201_CREATED)
def create_team(name: str):
    return Response(content=f"Equip '{name}' creat correctament", media_type="text/plain")
```

**Crida amb `curl`**
```bash
curl -X POST "http://localhost:8000/teams" -d "name=Dragons"
```

**Resposta**
```
Equip 'Dragons' creat correctament
```
