# 📁 Recursos per al mòdul M13

Aquest directori conté diferents materials per a la programació i anàlisi de dades.

## 📂 Contingut

### 📄 **docs/** - Documents teòrics i PDFs
Aquesta carpeta conté materials teòrics i guies en format PDF per complementar l'aprenentatge.

### 🖼️ **images/** - Imatges i gràfics
Recursos visuals com diagrames, icones i captures de pantalla utilitzats en la documentació.

### 📂 **datasets/** - Datasets per a l'anàlisi de dades
Fitxers CSV i altres formats amb dades reals per a l'anàlisi en exercicis i projectes.

### 📚 **texts/** - Fitxers de text
Articles, exemples i altres materials en format de text.


---

👉 **Nota:** Si fas servir aquests materials en exercicis, segueix les indicacions de cada apartat per entendre el seu contingut i utilitat.

